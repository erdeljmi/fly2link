package main

// DataClient : code executed by each node/drone.
// communicates information to the robot server.

import (
	"log"
	"os/exec"
	"time"

	flatbuffers "github.com/google/flatbuffers/go"
	zmq "github.com/pebbe/zmq4"
	//message "gitlab.utc.fr/bobo-dataflatbuffers/TXData"
	viper "github.com/spf13/viper"
	message "gitlab.utc.fr/ukborey/tx_streaming/data-streaming/bobo-dataflatbuffers/TXData"
)

// TODO : - add a function to build stream information message
//	  - add a configuration flag or file

func main() {
	// read configuration from file
	viper.SetConfigName("config")
	viper.AddConfigPath("./")

	err := viper.ReadInConfig()

	if err != nil {
		log.Fatalf("Config file not found. Create a 'config' config file")
	}

	streamServer := viper.GetString("stream.server")
	droneId := viper.GetString("drone.id")
	camDevice := viper.GetString("drone.cam-path")
	url := viper.GetString("stream.url")
	robotServer := viper.GetString("robot-server.server")

	droneScreenshotServer := viper.GetString("screenshot.folder")

	// mkdir(droneScreenshotServer+"/"+droneId)

	streamInfo := streamServer + url + "/" + droneId

	cmd := exec.Command("ffmpeg", "-re", "-i", camDevice, "-bsf:v", "h264_mp4toannexb", "-c", "copy", "-f", "mpegts", streamInfo)
	go cmd.Run()

	log.Println("launched stream on " + streamInfo)

	// create the context
	context, _ := zmq.NewContext()

	// a drone node is a PUSH client
	// connect to the sink
	sender, _ := context.NewSocket(zmq.PUSH)
	defer sender.Close()
	sender.Connect("tcp://" + robotServer)
	i := 0
	j := 0

	// endless streaming
	for {
		data := retrieveTelemetryData()
		sender.Send("Telemetry", zmq.SNDMORE)
		sender.SendBytes(data, 0)
		log.Println("sent telemetry")
		i++
		j++
		if j == 5 {
			sender.Send("StreamData", zmq.SNDMORE)
			sender.Send(streamInfo, 0)
			log.Println("sent stream info")
			j = 0
		}
		if i == 3 {
			sender.Send("StreamData", zmq.SNDMORE)
			// screenshot := read(droneScreenshotFolder+"/screenshot.png")
			// base64screenshot := strings.encode(screenshot, 64)
			// sender.Send(base64screenshot, 0)
			log.Println("sent screenshot")
			i = 0
		}
		time.Sleep(time.Second)
	}
}

// This function will get the telemetry data and encode it into a flatbuffer
func retrieveTelemetryData() []byte {
	// TODO : instructions to get real telemetry data...

	// initialize our flatbuffer builder with 1024 bytes of initial size
	builder := flatbuffers.NewBuilder(1024)

	// message -> telemetry -> strings
	componentString := builder.CreateString("component-1")
	messageString := builder.CreateString("slt from bobo")

	// build the telemetry
	message.TelemetryStart(builder)
	// localisation is a struct, containing latitude:float and longitude:float
	message.TelemetryAddLocalisation(builder, message.CreateLocalisation(builder, 1.0, 2.0))
	message.TelemetryAddTelemetryId(builder, 1)
	message.TelemetryAddPriority(builder, 1)
	message.TelemetryAddComponentId(builder, componentString)
	message.TelemetryAddMessage(builder, messageString)
	telemetryMessage := message.TelemetryEnd(builder)

	// build the message containing the telemetry
	message.MessageStart(builder)
	message.MessageAddMessageId(builder, 1337)
	// we need to precise what type of message is contained in the message
	message.MessageAddMessageType(builder, message.MessageContentTelemetry)
	// we add the telemetry message
	message.MessageAddMessage(builder, telemetryMessage)

	// everything is built, return our encoded message
	messageToSend := message.MessageEnd(builder)
	builder.Finish(messageToSend)
	data := builder.FinishedBytes()

	return data
}
