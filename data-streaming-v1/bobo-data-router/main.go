package main

// DataRouter : code executed by robot server.
// will gather every data sent by the local nodes, and send it to the central webserver

import (
	"log"

	flatbuffers "github.com/google/flatbuffers/go"
	zmq "github.com/pebbe/zmq4"
	//	message "gitlab.utc.fr/bobo-dataflatbuffers/TXData"
	viper "github.com/spf13/viper"
	message "gitlab.utc.fr/ukborey/tx_streaming/data-streaming/bobo-dataflatbuffers/TXData"
)

func main() {
	viper.SetConfigName("config")
	viper.AddConfigPath("./")

	err := viper.ReadInConfig()

	if err != nil {
		log.Fatalf("Config file not found. Create a config file and relaunch")
	}

	context, _ := zmq.NewContext()

	serverAddress := viper.GetString("server.address")
	webserverAddress := viper.GetString("webserver.address")

	log.Println(serverAddress)
	log.Println(webserverAddress)

	// we want to pull everything that is sent by the drones
	receiver, _ := context.NewSocket(zmq.PULL)
	defer receiver.Close()
	receiver.Bind("tcp://" + serverAddress)

	// we will push every data sent by the drones to the webserver
	sender, _ := context.NewSocket(zmq.PUSH)
	defer sender.Close()
	sender.Connect("tcp://" + webserverAddress)

	for {
		// get the envoloppe
		envl, _ := receiver.Recv(0)
		// get the data
		data, _ := receiver.RecvBytes(0)
		log.Println("got message")
		log.Println(envl)
		log.Println(data)
		//resend everything
		sender.Send(envl, zmq.SNDMORE)
		sender.SendBytes(data, 0)
	}
}




// test function to read from a flatbuffer

func readData(data []byte) {
	receivedMessageContent := message.GetRootAsMessage(data, 0)
	unionTable := new(flatbuffers.Table)
	if receivedMessageContent.Message(unionTable) {
		unionType := receivedMessageContent.MessageType()
		if unionType == message.MessageContentTelemetry {
			telemetryRead := new(message.Telemetry)
			telemetryRead.Init(unionTable.Bytes, unionTable.Pos)
			receivedMessageString := telemetryRead.Message()
			log.Println(string(receivedMessageString))
		}
	}
}
