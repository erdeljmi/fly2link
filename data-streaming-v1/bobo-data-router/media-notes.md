# RTMP/MPEG-TS Server

nginx and module is under permissive BSD license.

## RTMP server setup
get utilities:
```
sudo apt-get install build-essential libpcre3 libpcre3-dev libssl-dev
```

get nginx-module:
```
git clone https://github.com/arut/nginx-rtmp-module.git
```

download nginx :
``̀ 
wget http://nginx.org/download/nginx-1.12.0.tar.gz
tar xzf nginx-1.12.0.tar.gz
cd nginx-1.12.0
```

build nginx with rtmp-module:
```
sudo ./configure --with-http_ssl_module --add-module=../nginx-rtmp-module
sudo make
sudo make install
```

start nginx:
```
sudo /usr/local/nginx/sbin/nginx
```

copy the provided config from this repo into the nginx folder:
```
cd [directory where you cloned this repo]/tx_streaming/data-stream/bobo-data-router
sudo cp ./nginx.conf.defaults /usr/local/nginx/conf/nginx.conf
sudo /usr/local/nginx/sbin/nginx -s stop
sudo /usr/local/nginx/sbin/nginx
```

you can now publish on [server-ip]:8080/drone-cam/[what you want] with ffmpeg


## MPEG-TS server setup
clone the right module:
```
git clone https://github.com/arut/nginx-ts-module
```

follow the same steps for downloading nginx

build nginx with the ts-module:
```
sudo ./configure --with-http_ssl_module --add-modyle=../nginx-ts-module
sudo make
sudo make install
```

follow the same steps, but copy this config instead of the other
```
sudo cp ./nginx.conf.ts.defaults /usr/local/nginx/conf/nginx.conf
```
