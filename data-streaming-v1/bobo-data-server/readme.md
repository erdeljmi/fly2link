# data-webserver
central point of the system.
gather every data sent by the (multiple) robot-servers and dispatch the processing to sub programs.

we have 3 handlers for each type of message received :
- telemetry -> TelemetryProcesser (telemetry.go)
- screenshots -> ScreenshotProcesser (screenshot.go + call to opencv)
- stream (info) -> StreamProcesser (stream.go)
