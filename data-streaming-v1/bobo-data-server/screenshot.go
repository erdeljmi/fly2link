package main

import (
	"log"

	zmq "github.com/pebbe/zmq4"
	//	message "gitlab.utc.fr/bobo-dataflatbuffers/TXData"
)

func NewScreenshotProcesser(socket *zmq.Socket) *GenericProcesser {
	return &GenericProcesser{"Screenshot", socket}
}

// A screenshot is a base64 string

func (sp *GenericProcesser) ProcessScreenshots() {
	log.Println("ScreenshotProcesser launched!")
	// TODO
	sp.ProcesserSocket.Connect("tcp://" + internalAddress)
	sp.ProcesserSocket.SetSubscribe(sp.SubscribeLine)

	for {
		_, _ = sp.ProcesserSocket.Recv(0)
		data, _ := sp.ProcesserSocket.Recv(0)
		log.Println("got a screenshot to process !")
		log.Println(data)

	}
}
