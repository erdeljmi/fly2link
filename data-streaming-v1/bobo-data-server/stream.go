package main

import (
	"log"

	zmq "github.com/pebbe/zmq4"
	//	message "gitlab.utc.fr/bobo-dataflatbuffers/TXData"
)

func NewStreamProcesser(socket *zmq.Socket) *GenericProcesser {
	return &GenericProcesser{"StreamInfo", socket}
}

func (sp *GenericProcesser) ProcessStreams() {
	log.Println("StreamProcesser launched!")

	sp.ProcesserSocket.Connect("tcp://" + internalAddress)
	sp.ProcesserSocket.SetSubscribe(sp.SubscribeLine)

	// A stream info is just a string
	for {
		envl, _ := sp.ProcesserSocket.Recv(0)
		data, _ := sp.ProcesserSocket.Recv(0)
		log.Println(envl)
		log.Println(data)
	}
}
