package main

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"strconv"
	"strings"
	"time"

	"github.com/olivere/elastic"
	zmq "github.com/pebbe/zmq4"
	message "gitlab.utc.fr/ukborey/tx_streaming/data-streaming/bobo-dataflatbuffers/TXData"
)

type JsonTelemetry struct {
	SystemID         uint32
	CompID           uint32
	Type             uint32
	Autopilot        uint32
	BaseMode         uint32
	CustomMode       uint32
	SystemStatus     uint32
	VoltageBattery   uint32
	BatteryRemaining int32
	DropRateComm     uint32
	ErrorsComm       uint32
	TimeBootMs       uint32
	X                float32
	Y                float32
	Z                float32
	Vx               float32
	Vy               float32
	Vz               float32
	SensorsPresent   uint32
	SensorsEnabled   uint32
	SensorsHealth    uint32
	MainloopLoad     uint32
	CurrentBattery   uint32
	Latitude         int32
	Longitude        int32
	Altitude         int32
	RelativeAlt      int32
	Heading          uint32
	LastUpdate       uint32
	SystemIP         string
}

/*
const (
	indexMapping = `
	{
		"mappings" : {
			"telemetry": {
				"properties" : {
					"systemID" : {"type":"integer"},
					"compID" : {"type":"integer"},
					"type" : {"type":"integer"},
					"autopilot" : {"type":"integer"},
					"basemode" : {"type":"integer"},
					"custommode" : {"type":"integer"},
					"systemstatus" : {"type":"integer"},
					"voltagebattery" : {"type":"integer"},
					"batteryremaining" : {"type":"integer"},
					"dropratecomm" : {"type":"integer"},
					"errorscomm" : {"type":"integer"},
					"timebootms" : {"type":"integer"},
					"x" : {"type":"float"},
					"y" : {"type":"float"},
					"z" : {"type":"float"},
					"vx": {"type":"float"},
					"vy": {"type":"float"},
					"vz": {"type":"float"},
					"sensorspresent" : {"type":"integer"},
					"sensorsenabled" : {"type":"integer"},
					"sensorshealth" : {"type":"integer"},
					"mainloopload" : {"type":"integer"},
					"currentbattery" : {"type":"integer"},
					"latitude": {"type":"integer"},
					"longitude": {"type":"integer"},
					"altitude": {"type":"integer"},
					"relativealt": {"type":"integer"},
					"heading": {"type":"integer"},
					"lastupdate": {"type":"integer"}
				}
			}
		}
	}`
)
*/
func NewTelemetryProcesser(socket *zmq.Socket) *GenericProcesser {
	return &GenericProcesser{"Telemetry", socket}
}

func (sp *GenericProcesser) ProcessTelemetries() {
	sp.ProcesserSocket.Connect("tcp://" + internalAddress)
	sp.ProcesserSocket.SetSubscribe(sp.SubscribeLine)
	for {

		_, _ = sp.ProcesserSocket.Recv(0)
		data, _ := sp.ProcesserSocket.RecvBytes(0)

		//go readData(data)
		processData(data)

		//log.Print("Telemetry processer got " + envl + "/")
		//log.Println(data)
		//jsonData, _ := json.Marshal(data)
		//log.Println(jsonData)
	}

	/*
		receiver, _ := zmq.NewSocket(zmq.SUB)
		receiver.Connect("tcp://localhost:1339")
		receiver.SetSubscribe("Telemetry")
		for {
			evl, _ := receiver.Recv(0)
			_, _ = receiver.RecvBytes(0)
			log.Println(evl)
		}*/
}

func getCurrentTime() string {
	t := time.Now()
	return strings.ToLower(t.Format(time.RFC3339))
}

func processData(data []byte) {
	ctx := context.Background()

	// TODO: let the user configure the elastic client
	client, err := elastic.NewClient(
		elastic.SetURL("http://127.0.0.1:9200"),
		elastic.SetSniff(false),
		elastic.SetBasicAuth("elastic", "_MEl7P=v9_TzBqeXJwpl"),
	)
	if err != nil {
		panic(err)
	}

	telemetry, systemid := convertToJson(data)
	currentTime := getCurrentTime()
	currentIndex := "drones-" + currentTime
	// Check if index exists for the current second
	exists, err := client.IndexExists(currentIndex).Do(ctx)
	if err != nil {
		// Handle error
		log.Println(err)
	}
	if !exists {
		// Create a new index.
		createIndex, err := client.CreateIndex(currentIndex).BodyString("").Do(ctx)
		if err != nil {
			// Handle error
			log.Println(err)
		}
		if !createIndex.Acknowledged {
			// Not acknowledged
		}
	}

	// insert and use the systemid as key
	put2, err := client.Index().
		Index(currentIndex).
		Type("telemetry").
		Id(strconv.Itoa(int(systemid))).
		BodyString(string(telemetry)).
		Do(ctx)
	if err != nil {
		log.Println(err)
	}
	fmt.Printf("Indexed drone %s to index %s, type %s\n", put2.Id, put2.Index, put2.Type)
}

func convertToJson(fbTelemetry []byte) ([]byte, uint32) {
	log.Println("converting")
	telemetry := message.GetRootAsNeighUAV(fbTelemetry, 0)
	jsonTelemetry := JsonTelemetry{
		telemetry.SystemId(),
		telemetry.CompId(),
		telemetry.Type(),
		telemetry.Autopilot(),
		telemetry.BaseMode(),
		telemetry.CustomMode(),
		telemetry.SystemStatus(),
		telemetry.VoltageBattery(),
		telemetry.BatteryRemaining(),
		telemetry.DropRateComm(),
		telemetry.ErrorsComm(),
		telemetry.TimeBootMs(),
		telemetry.X(),
		telemetry.Y(),
		telemetry.Z(),
		telemetry.Vx(),
		telemetry.Vy(),
		telemetry.Vz(),
		telemetry.SensorsPresent(),
		telemetry.SensorsEnabled(),
		telemetry.SensorsHealth(),
		telemetry.MainloopLoad(),
		telemetry.CurrentBattery(),
		telemetry.Latitude(),
		telemetry.Longitude(),
		telemetry.Altitude(),
		telemetry.RelativeAlt(),
		telemetry.Heading(),
		telemetry.LastUpdate(),
		string(telemetry.SystemIp()),
	}
	marshalledJson, err := json.Marshal(jsonTelemetry)
	if err != nil {
		log.Println(err)
	}
	return marshalledJson, telemetry.SystemId()
}

func readData(data []byte) {
	telemetry := message.GetRootAsNeighUAV(data, 0)
	log.Println(telemetry.SystemId())
	log.Println(telemetry.LastUpdate())
}
