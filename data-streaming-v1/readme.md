# Architecture pour données
Première implémentation de la proposition d'architecture afin de communiquer des données, que ce soit télémétrie, screenshots, ou informations générales (ex: infos de stream).
Est associé au document suivant : https://docs.google.com/document/d/1SwlTs-4IyxVt76cgh_QVFrY5WXUO3IVgRgYjY8uVLbQ/
Utilise la meme architecture que celle définie par David K. et Clément P., le streaming multimédia reposant sur un flux ffmpeg relayée par `konam-relay`.
4 blocs :
- data-node : correspond au drone, envoie son flux video (voir archi de David et Clément)
- fleetgcs : logiciel de controle des flottes; envoie les données de télémétrie au `data-router`, propose(ra) une api pour controler les drones
- data-router : centralise les flots de données, fonctionne en parallèle avec `konam-relay`, route toutes les données reçues pour une flotte au webserver.
- data-server : constitue le webserver, distribue les messages aux "workers" stream et telemetry, insère les données en base de données


!!! nécessite `go build -ldflags -s` sur mac os !!!

## Installation préalable de logiciels
### installer zeromq
testé sur ubuntu 16.04
```
sudo apt-get install libtool pkg-config build-essential autoconf automake
git clone https://github.com/zeromq/libzmq
./autogen.sh && ./configure && make -j 4
make check && make install && sudo ldconfig
```

bindings go (faculatitf)
```
go get github.com/pebbe/zmq4
```
### installer flatbuffers
testé sur ubuntu 16.04

```
sudo apt install cmake
git clone https://github.com/google/flatbuffers.git
cd flatbuffers
cmake -G "Unix Makefiles"
make
./flattests
go get github.com/google/flatbuffers/go
```

### ElasticSearch & Kibana
suivre les instructions : `https://www.elastic.co/fr/start`
changer le mot de passe dans `data-server/telemetry.go`

## drone-node
programme exécuté sur le drone.
récupère les infos utiles, les encode puis les publie sur la file de datarouter.
envoie des screenshots ainsi que des infos de stream.

De la même manière que dans l'architecutre multimédia existante, envoie un flux vidéo avec `ffmpeg`.

techs: go, zeromq, flatbuf (?), ffmpeg

### TODO
- ajouter un vrai screenshot
- formatter les messages envoyés au data-router


## data-router
programme exécuté sur le robot-server.
récupère tous les messages envoyés par les drones et fleetgcs et les route vers le data server.
permet de centraliser tous les messages de drone.

techs: go, zeromq

## DataServer
Centralise les messages et les distribue à des sous-fonctions exécutés dans des threads.
est composé de 4 sous programmes :
- screenshot-processer : va correspondre au script js existant, récupère les screenshots
- telemetry-processer : traite les messages de télémétrie reçus
- stream-processer : récupère les messages contenant des infos de streaming
- data-api : expose une api REST/JSON au client web (pas encore fait)

techs: go, zeromq, flatbuf, opencv, [une base de données orienté doc], go-restful
