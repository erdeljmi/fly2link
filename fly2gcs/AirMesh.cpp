/*********************************************************************/
//  created:    04/04/2018
//
//  author:     Milan Erdelj, <erdeljmi@gmail.com>
//
//  version:    $Id: $
//
/*********************************************************************/

#include "AirMesh.h"
#include <sstream>
#include <stdlib.h>
#include <iostream>
#include <netdb.h>
#include <boost/bind.hpp>
#include "AirMessage.h"
#include <tuple>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>

#define THREAD_MS_IN 3
#define THREAD_MS_OUT 3

#define MSG_TIME_TO_LIVE 1500000
#define MSG_RETRY_TIMEOUT 400000
#define PERIOD_SECOND 1000000
#define UAV_INFO_TIMEOUT 2500000
#define INFO_TIMER_DURATION 1000000 //1.0
#define UAV_TABLE_TIMER_DURATION 500000 //0.5


using namespace std;

const std::string red("\033[1;31m");
const std::string green("\033[1;32m");
const std::string yellow("\033[1;33m");
const std::string reset("\033[0m");

AirMesh::AirMesh(uint8_t sysid, int cmdPort, int infoPort, bool debug_info, string config_file) :
    cmdPort(cmdPort), bcPort(infoPort), my_system_id(sysid), debug_info(debug_info), config_file(config_file),
    cmd_server(io_service, cmdPort),
    info_server(io_service, infoPort),
    service_thread(std::bind(&AirMesh::run_service, this))
{
    // endpoint address initialization
    for(int i=0; i<MAX_NEIGH; i++) {
        infoMesh[i].endpoint_address.clear();
    }

    // get the parameters from the configuration file
    getParams();

    // starting the timers
    timerInfoUAV.start(info_timer_duration, std::bind(&AirMesh::callbackDiscovery,this));
    timerUpdateNeighTable.start(uav_table_timer_duration, std::bind(&AirMesh::callbackUpdateNeighTable,this));

    stop_recv = false;
    stop_send = false;
    startThreads(); // start both sending and receiving thread

    std::cout << green << "[AirMesh] OK" << reset << std::endl;
}

void AirMesh::getParams() {
    //const std::string filename = "airmesh_config.xml";
    using boost::property_tree::ptree;
    ptree pt;

    // using hard-coded values in case if parameters are not provided
    time_to_live = MSG_TIME_TO_LIVE;
    retry_timeout = MSG_RETRY_TIMEOUT;
    uav_info_timeout = UAV_INFO_TIMEOUT;
    info_timer_duration = INFO_TIMER_DURATION;
    uav_table_timer_duration = UAV_TABLE_TIMER_DURATION;

    read_xml(config_file, pt);

    for(const auto& i : pt.get_child("airmesh.config"))
    {
        std::string name;
        std::string param_name;
        std::string param_value;
        ptree sub_pt;
        std::tie(name, sub_pt) = i;

        if (name != "param")
            continue;
        param_name = sub_pt.get<std::string>("<xmlattr>.name");
        param_value = sub_pt.get<std::string>("<xmlattr>.value");
        //std::cout << name << " " << sub_pt.get<std::string>("<xmlattr>.name") << " " << sub_pt.get<std::string>("<xmlattr>.value") << std::endl;

        if(!param_name.compare("uav_sys_id")) {
            my_system_id = stoi(param_value);
            std::cout << "Configuration parameter retrieved: " << param_name << " = " << param_value << std::endl;
        }
        //            if(!param_name.compare("time_to_live")) {
        //                time_to_live = atof(param_value.c_str());
        //                std::cout << "Configuration parameter retrieved: " << param_name << " = " << param_value << std::endl;
        //            }
        //            if(!param_name.compare("retry_timeout")) {
        //                retry_timeout = atof(param_value.c_str());
        //                std::cout << "Configuration parameter retrieved: " << param_name << " = " << param_value << std::endl;
        //            }
        //            if(!param_name.compare("uav_info_timeout")) {
        //                uav_info_timeout = atof(param_value.c_str());
        //                std::cout << "Configuration parameter retrieved: " << param_name << " = " << param_value << std::endl;
        //            }
        //            if(!param_name.compare("info_timer_duration")) {
        //                info_timer_duration = atof(param_value.c_str());
        //                std::cout << "Configuration parameter retrieved: " << param_name << " = " << param_value << std::endl;
        //            }
        //            if(!param_name.compare("uav_table_timer_duration")) {
        //                std::string::size_type sz;     // alias of size_t
        //                uav_table_timer_duration = stod(param_value,&sz);
        //                std::cout << "Configuration parameter retrieved: " << param_name << " = " << param_value << std::endl;
        //            }
    }
}

AirMesh::~AirMesh() {
    io_service.stop();
    service_thread.join();
    stopThreads();
}

void AirMesh::run_service()
{
    usleep(1000);
    info_server.start_receive();
    while (!io_service.stopped()){
        try {
            io_service.run();
        } catch( const std::exception& e ) {
            std::cout << "Server network exception: " << e.what() << std::endl;
        }
        catch(...) {
            std::cout << "Unknown exception in server network thread" << std::endl;
        }
    }
}

void AirMesh::broadcast(std::string msg_body){
    //info_server.broadcast(msg_body, my_system_id, msg_seq++, AirmeshInfo);
    // not affecting the msg_sequence:
    info_server.broadcast(msg_body, my_system_id, 0, AirmeshInfo);
}

int AirMesh::send(std::string msg_body, uint8_t destId, bool with_ack)
{
    if(activeNodes.find(destId) != activeNodes.end())
    {
        MsgListElem temp;
        temp.dest = destId;
        temp.source = my_system_id;
        temp.msg = msg_body;
        temp.replied = false;
        temp.needs_ack = with_ack;
        temp.sent = false;
        temp.sequence = msg_seq++;
        temp.type = AirmeshCommand;
        commands_to_send.push_back(temp);
    } else {
        // element is not there
        printf("[AIRMESH] Cannot find node id: %d\n", destId);
        return -1;
    }
    return 0;
}

void AirMesh::sendToAll(std::string msg_body, bool with_ack){
    std::set<uint8_t>::iterator it;
    for (it = activeNodes.begin(); it != activeNodes.end(); ++it)
    {
        // send to each of the active nodes
        send(msg_body, *it, with_ack);
    }
}

// starting send and receive threads
void AirMesh::startThreads() {
    recv_th = std::thread(&AirMesh::handle_recv, this);
    send_th = std::thread(&AirMesh::handle_send, this);
}

void AirMesh::stopThreads() {
    stop_recv = true;
    stop_send = true;
    usleep(100);
    if(recv_th.joinable()) recv_th.join();
    if(send_th.joinable()) send_th.join();
}

void AirMesh::handle_recv() {
    while(!stop_recv) {
        if(cmd_server.HasMessages()) {
            AirMessage recv_cmd = cmd_server.PopMessage();
            string recv_str(recv_cmd.body.begin(), recv_cmd.body.end());
            // depending on what is received, there are different actions:
            switch(recv_cmd.header.type){
            case AirmeshCommand: {
                // check if the message have already been received
                recv_seq_pair.first = recv_cmd.header.source;
                recv_seq_pair.second = recv_cmd.header.sequence;
                if(std::find(recv_seq_array.begin(), recv_seq_array.end(), recv_seq_pair) == recv_seq_array.end()) {
                    // new message, this seq number not received before, add it to recv_seq
                        recv_msg_count++;
                        // put in the sequence array, using the circular buffer method
                        recv_seq_array[recv_msg_count%SEQ_SET_FLUSH] = recv_seq_pair;
                    // put the message in the recv vector (string)
                    recv_messages.push(recv_str);
                }
                // send the reply to command with sequence number as payload
                std::string reply_msg = to_string(recv_cmd.header.sequence);
                cmd_server.send(reply_msg, my_system_id, recv_cmd.header.source, recv_cmd.header.sequence, AirmeshReply, recv_cmd.recv_endpoint);
                if(debug_info){
                    printf("[SENT] source %d dest %d type %d len %ld seq %d\n", my_system_id, recv_cmd.header.source, AirmeshReply, reply_msg.size(), recv_cmd.header.sequence);
                }
                break;
            }
            case AirmeshInfo: {
                // TODO relay the message, CIDv3
                break;
            }
            case AirmeshReply: {
                // insert the reply into the command list
                for(vector<MsgListElem>::size_type i = 0; i != commands_to_send.size(); i++) {
                    if(commands_to_send[i].sequence == recv_cmd.header.sequence) {
                        commands_to_send[i].replied = true;
                        break; // terminating the for loop after the element has been found
                    }
                }
                break;
            }
            default:
                cout << "[WARN] Unknown message!" << endl;
                break;
            }
        }
        if(info_server.HasMessages()) {
            // info received - manage neigh table
            AirMessage temp = info_server.PopMessage();
            string recv_msg(temp.body.begin(), temp.body.end());
            // add the message body to the airmesh queue
            recv_messages.push(recv_msg);
            if(debug_info) {
                printf("[INFO] source %d dest %d type %d len %d seq %d\n", temp.header.source, temp.header.dest, temp.header.type, temp.header.msg_len, temp.header.sequence);
                //cout << "[INFO] msg: " << recv_msg << endl;
            }
            activeNodes.insert(temp.header.source);
            infoMesh[temp.header.source].data = recv_msg;
            infoMesh[temp.header.source].last_update = get_time_usec();
            infoMesh[temp.header.source].system_id = temp.header.source;
            infoMesh[temp.header.source].endpoint_address = temp.endpoint_address;
        }
        std::this_thread::sleep_for(std::chrono::milliseconds(THREAD_MS_IN));
    }
}

void AirMesh::handle_send() {
    while(!stop_send) {
        // sending commands and manage acknowledgements
        vector<uint16_t> delete_list;
        for(std::vector<MsgListElem>::size_type i = 0; i != commands_to_send.size(); i++) {
            // send or check if retransmission needed
            MsgListElem temp_msg = commands_to_send[i];
            if(temp_msg.needs_ack){
                // this command needs an acknowledgement
                if(temp_msg.sent){
                    // message already sent, waiting for reply
                    if(!temp_msg.replied) {
                        // still no reply
                        if((temp_msg.time_sent + time_to_live)<get_time_usec()) {
                            // sending failed!
                            cout << "[" << red << "ERRR" << reset;
                            printf("] source %d dest %d type %d seq %d ", temp_msg.source, temp_msg.dest, temp_msg.type, temp_msg.sequence);
                            delete_list.push_back(i);
                        } else {
                            // message is still valid
                            if((temp_msg.last_time_sent + retry_timeout)<get_time_usec()){
                                cmd_server.send(temp_msg.msg, temp_msg.source, temp_msg.dest, temp_msg.sequence, temp_msg.type, udp::endpoint(boost::asio::ip::address::from_string(infoMesh[temp_msg.dest].endpoint_address), cmdPort));
                                if(debug_info) printf("[SENT] source %d dest %d type %d len %ld seq %d\n", temp_msg.source, temp_msg.dest, temp_msg.type, temp_msg.msg.size(), temp_msg.sequence);
                                commands_to_send[i].sent = true;
                                commands_to_send[i].replied = false;
                                commands_to_send[i].last_time_sent = get_time_usec();
                            }
                        }
                    } else {
                        // ok, reply received
                        //cout << "[" << green << "SENT" << reset << "]" << endl;
                        printf("[SENT] source %d dest %d type %d seq %d\n", temp_msg.source, temp_msg.dest, temp_msg.type, temp_msg.sequence);
                        delete_list.push_back(i);
                    }
                } else {
                    // not sent before, send the message
                    cmd_server.send(temp_msg.msg, temp_msg.source, temp_msg.dest, temp_msg.sequence, temp_msg.type, udp::endpoint(boost::asio::ip::address::from_string(infoMesh[temp_msg.dest].endpoint_address), cmdPort));
                    commands_to_send[i].sent = true;
                    commands_to_send[i].replied = false;
                    //commands_to_send[i].needs_ack = true;
                    commands_to_send[i].time_sent = get_time_usec();
                    commands_to_send[i].last_time_sent = get_time_usec();
                }
            }
        }
        // deleting the replied and/or expired commands
        for(std::vector<uint16_t>::size_type i = 0; i != delete_list.size(); i++) {
            commands_to_send.erase(commands_to_send.begin()+i);
        }
        std::this_thread::sleep_for(std::chrono::milliseconds(THREAD_MS_OUT));
    }
}

void AirMesh::callbackDiscovery()
{
    info_server.discovery(my_system_id, AirmeshDiscovery);
}

void AirMesh::callbackUpdateNeighTable()
{
    uint64_t latency;
    for(auto f : activeNodes) {
        //printf("active: %d %d", f, infoMesh[f].system_id);
        latency = get_time_usec() - infoMesh[f].last_update;
        if(latency > uav_info_timeout) {
            // exclude the drone
            infoMesh[f].endpoint_address.clear();
            activeNodes.erase(f);
        }
    }
}

uint64_t AirMesh::get_time_usec() {
    struct timeval tv;
    gettimeofday(&tv, NULL);
    return ((uint64_t)tv.tv_sec) * 1000000 + tv.tv_usec;
}
