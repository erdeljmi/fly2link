/*********************************************************************/
//  created:    07/05/2015
//
//  author:     Milan Erdelj, <erdeljmi@gmail.com>
//
//  version:    $Id: $
//
/*********************************************************************/

#ifndef AIRMESH_H
#define AIRMESH_H

#include <thread>
#include <vector>
#include <array>
#include <set>
//#include <pair>
#include <boost/thread.hpp>
#include "LockedQueue.h"
#include "InfoServer.h"
#include "CommandServer.h"
#include "AirMessage.h"
#include "CallbackTimer.h"
#include <boost/asio.hpp>
#include <climits>

using namespace std;

#define MAX_NEIGH 255
#define SEQ_SET_FLUSH 1000

struct InfoMesh
{
    uint8_t system_id;
    uint8_t system_status;
    uint64_t last_update;
    std::string data;
    std::string endpoint_address;
};

struct MsgListElem
{
    bool sent;
    bool needs_ack;
    bool replied;
    uint64_t time_sent;
    uint64_t last_time_sent;
    uint8_t dest;
    uint8_t source;
    AirmeshMessageType type;
    uint16_t sequence;
    string msg;
    string endpoint_address;
};


class AirMesh {
public:
    AirMesh(uint8_t sysid, int cmdPort, int infoPort, bool debug_info, string config_file);
    ~AirMesh();

    uint8_t my_system_id;

    bool debug_info = false;

    // threads start and stop
    void startThreads();
    void stopThreads();

    vector<MsgListElem> commands_to_send;

    uint16_t message_interval;

    InfoMesh infoMesh[MAX_NEIGH];
    std::set<uint8_t> activeNodes;

    uint64_t get_time_usec();

    void broadcast(string msg);
    int send(string msg, uint8_t dest_id, bool with_ack = false);
    void sendToAll(string msg, bool with_ack = false);

    bool HasMessages() {return !recv_messages.empty();}
    std::string PopMessage() { return recv_messages.pop();}

    void getParams();

private:

    // network send receive
    boost::asio::io_service io_service;
    boost::thread service_thread;

    CommandServer cmd_server;
    InfoServer info_server;

    LockedQueue<std::string> recv_messages;

    std::pair<uint8_t, uint16_t> recv_seq_pair;
    std::array<std::pair<uint8_t,uint16_t>, SEQ_SET_FLUSH> recv_seq_array;
    uint16_t msg_seq = 0;
    uint32_t recv_msg_count = 0;

    string config_file;

    int cmdPort;
    int bcPort;

    void run_service();

    AirMesh(AirMesh&); // block default copy constructor

    std::thread send_th;
    std::thread recv_th;
    void handle_recv(); // receiver thread
    void handle_send();// sender thread
    bool stop_recv;
    bool stop_send;

    CallbackTimer timerInfoUAV;
    void callbackDiscovery();

    CallbackTimer timerUpdateNeighTable;
    void callbackUpdateNeighTable();

    // parameters read from the launch file
    double time_to_live;
    double retry_timeout;
    double uav_info_timeout;
    double info_timer_duration;
    double uav_table_timer_duration;
};

#endif // AIRMESH_H
