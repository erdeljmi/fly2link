/*********************************************************************/
//  created:    04/04/2018
//
//  author:     Milan Erdelj, <erdeljmi@gmail.com>
//
//  version:    $Id: $
//
/*********************************************************************/

#include "AirMessage.h"

AirMessage::AirMessage() {}

// Code from : https://stackoverflow.com/questions/27939882/fast-crc-algorithm
uint32_t AirMessage::ComputeCrc32c(uint32_t crc, const uint8_t *buf, size_t len)
{
    int k;
    crc = ~crc;
    while (len--) {
        crc ^= *buf++;
        for (k = 0; k < 8; k++)
            crc = crc & 1 ? (crc >> 1) ^ POLY : crc >> 1;
    }
    return ~crc;
}

//int AirMessage::Pack(unsigned char *msg_body, uint8_t sourceId, uint8_t destId, uint16_t msg_len, uint16_t sequence, uint8_t type) {
//    // create the message header and body from the provided parameters
//    uint32_t crc_calc;
//    header.source = sourceId;
//    header.dest = destId;
//    header.type = type;
//    header.msg_len = (uint16_t)msg_len;
//    header.sequence = sequence;
//    // append header
//    memcpy(headerBuffer, &header, sizeof(AirHeader));
//    for(unsigned int i = 0; i<sizeof(AirHeader); i++) {
//        messageBuffer.push_back(headerBuffer[i]);
//    }
//    // append body
//    for(int i = 0; i<msg_len; i++) {
//        messageBuffer.push_back(msg_body[i]);
//    }
//    // append crc
//    std::vector<uint8_t> v;
//    uint8_t* msgbuff = &messageBuffer[0];
//    crc_calc = ComputeCrc32c(0, msgbuff, msg_len+sizeof(AirHeader));
//    memcpy(crcBuffer, &crc_calc, sizeof(uint32_t));
//    for(unsigned int i = 0; i<sizeof(uint32_t); i++) {
//        messageBuffer.push_back(crcBuffer[i]);
//    }
//    //printf("[PACK] source %d dest %d type %d len %d seq %d msg_buff_size %d crc %d\n", sourceId, destId, type, msg_len, sequence, messageBuffer.size(), crc_calc);
//    return 0;
//}

int AirMessage::Pack(string msg_body, uint8_t sourceId, uint8_t destId, uint16_t msg_len, uint16_t sequence, uint8_t type) {
    // create the message header and body from the provided parameters
    uint32_t crc_calc;
    header.source = sourceId;
    header.dest = destId;
    header.type = type;
    header.msg_len = (uint16_t)msg_len;
    header.sequence = sequence;
    // append header
    memcpy(headerBuffer, &header, sizeof(AirHeader));
    for(unsigned int i = 0; i<sizeof(AirHeader); i++) {
        messageBuffer.push_back(headerBuffer[i]);
    }
    // append body
    for(int i = 0; i<msg_len; i++) {
        messageBuffer.push_back(msg_body[i]);
    }
    // append crc
    std::vector<uint8_t> v;
    uint8_t* msgbuff = &messageBuffer[0];
    crc_calc = ComputeCrc32c(0, msgbuff, msg_len+sizeof(AirHeader));
    memcpy(crcBuffer, &crc_calc, sizeof(uint32_t));
    for(unsigned int i = 0; i<sizeof(uint32_t); i++) {
        messageBuffer.push_back(crcBuffer[i]);
    }
    //printf("[PACK] source %d dest %d type %d len %d seq %d msg_buff_size %d crc %d\n", sourceId, destId, type, msg_len, sequence, messageBuffer.size(), crc_calc);
    return 0;
}

int AirMessage::Unpack(uint8_t *buff){
    // extract the header
    AirHeader recv_header;
    memcpy(&recv_header, buff, sizeof(AirHeader));
    // extract the body
    //uint8_t recv_body[MESSAGE_BUFFER_SIZE];
    //vector<uint8_t> recv_body(buff+sizeof(CidHeader), recv_header.msg_len);
    //memcpy(&recv_body, buff+sizeof(CidHeader), recv_header.msg_len);
    // extract the CRC and check it
    uint32_t recv_crc;
    memcpy(&recv_crc, buff + sizeof(AirHeader) + recv_header.msg_len, 4);
    uint32_t computed_crc = ComputeCrc32c(0, (const uint8_t *)buff, recv_header.msg_len+sizeof(AirHeader));
    //printf("[CRC] received %d calculated %d\n", recv_crc, computed_crc);
    //printf("[UNPACK] source %d dest %d type %d len %d seq %d crc %d\n", recv_header.sourceId, recv_header.destId, recv_header.type, recv_header.msg_len, recv_header.sequence, recv_crc);
    if(recv_crc != computed_crc) {
        //printf("Error: CRC incorrect.\n");
        return -1;
    } else {
        header = recv_header;
        for(int i = 0; i<recv_header.msg_len; i++) {
            body.push_back(buff[i+sizeof(AirHeader)]);
        }
        return 0;
    }

}
