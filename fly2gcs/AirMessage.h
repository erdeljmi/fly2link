/*********************************************************************/
//  created:    04/04/2018
//
//  author:     Milan Erdelj, <erdeljmi@gmail.com>
//
//  version:    $Id: $
//
/*********************************************************************/

#ifndef MESSAGE_H
#define MESSAGE_H

#include <iostream>
#include <vector>
#include <cstring>
#include <boost/asio.hpp>

// CRC-32C (iSCSI) polynomial in reversed bit order
#define POLY 0x82f63b78

using namespace std;

enum AirmeshMessageType
{
    AirmeshInfo = 0,
    AirmeshMinInfo = 1,
    AirmeshCommand = 2,
    AirmeshReply = 3,
    AirmeshDiscovery = 4
};

struct AirHeader {
    uint8_t source;
    uint8_t dest;
    uint16_t msg_len;
    uint16_t sequence;
    uint8_t type;
}__attribute__ ((packed));

class AirMessage
{

private:
    // something

public:
    AirHeader header;
    vector<uint8_t> body;

    uint8_t headerBuffer[sizeof(AirHeader)];
    uint8_t crcBuffer[4];
    vector<uint8_t> messageBuffer;

    string endpoint_address;
    boost::asio::ip::udp::endpoint recv_endpoint;

    // Code from : https://stackoverflow.com/questions/27939882/fast-crc-algorithm
    uint32_t ComputeCrc32c(uint32_t crc, const uint8_t *buf, size_t len);

    int Pack(string msg_body, uint8_t sourceId, uint8_t destId, uint16_t msg_len, uint16_t sequence, uint8_t type);
    int Unpack(uint8_t *buff);

    AirMessage();
};

#endif // MESSAGE_H
