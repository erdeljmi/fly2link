/*********************************************************************/
//  created:    17/10/2017
//
//  author:     Milan Erdelj, <erdeljmi@gmail.com>
//
//  version:    $Id: $
//
/*********************************************************************/

#include "CommandServer.h"

CommandServer::CommandServer(boost::asio::io_service &io_service, int port) :
    socket_(io_service, udp::endpoint(udp::v4(), port))
{
    // allow reusing address (to run multiple nodes on the same machine
    socket_.set_option(boost::asio::ip::udp::socket::reuse_address(true));
    start_receive();
}

void CommandServer::start_receive()
{
    socket_.async_receive_from(boost::asio::buffer(recv_buffer_), remote_endpoint_, boost::bind(&CommandServer::handle_receive, this, boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred));
    //std::cout << "CMD async receive remote endpoint: " << remote_endpoint_ << std::endl;
}

void CommandServer::handle_receive(const boost::system::error_code& error, std::size_t bytes_transferred)
{
    if (!error || error == boost::asio::error::message_size)
    {
        //std::cout << "[CMD] Received " << bytes_transferred << " bytes from " << remote_endpoint_ << std::endl;
        AirMessage msg;
        unsigned char buff[MAX_BUFF_SIZE];
        for(unsigned int i = 0; i < bytes_transferred; i++) {
            buff[i] = recv_buffer_[i];
        }
        if(!msg.Unpack(buff)){
            //msg.endpoint_address = remote_endpoint_.address().to_string();
            msg.recv_endpoint = remote_endpoint_;
            recv_messages.push(msg);
        }
        receivedBytes += bytes_transferred;
        receivedMessages++;
        start_receive();
    }
}

void CommandServer::handle_send(boost::shared_ptr<std::string> message)
{
    sentBytes += message->size();
    sentMessages++;
}

CommandServer::~CommandServer()
{
    socket_.close();
}

void CommandServer::send(std::string message, udp::endpoint target_endpoint)
{
    socket_.send_to(boost::asio::buffer(message), target_endpoint);
}

void CommandServer::send(string msg, uint8_t sourceId, uint8_t destId, uint16_t sequence, uint8_t type, udp::endpoint target_endpoint)
{
    AirMessage message;
    // unsigned char *a = new unsigned char[msg_body.size()+1];
    // a[msg_body.size()] = 0;
    // memcpy(a,msg_body.c_str(),msg_body.size());
    message.Pack(msg, sourceId, destId, msg.length(), sequence, type);
    socket_.send_to(boost::asio::buffer(message.messageBuffer), target_endpoint);
    // delete a;
}

uint64_t CommandServer::get_time_usec() {
    struct timeval tv;
    gettimeofday(&tv, NULL);
    return ((uint64_t)tv.tv_sec) * 1000000 + tv.tv_usec;
}
