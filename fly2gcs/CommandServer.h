/*********************************************************************/
//  created:    17/10/2017
//
//  author:     Milan Erdelj, <erdeljmi@gmail.com>
//
//  version:    $Id: $
//
/*********************************************************************/

#ifndef CMD_UDP_H
#define CMD_UDP_H

#include <boost/array.hpp>
#include <boost/bind.hpp>
#include <boost/asio.hpp>
#include "AirMessage.h"
#include "LockedQueue.h"

#define MAX_BUFF_SIZE 400

using boost::asio::ip::udp;

class CommandServer
{
public:
    CommandServer(boost::asio::io_service& io_service, int port);
    ~CommandServer();

    void send(std::string message, udp::endpoint target_endpoint);
    void send(std::string msg, uint8_t sourceId, uint8_t destId, uint16_t sequence, uint8_t type, udp::endpoint target_endpoint);

    bool HasMessages() {return !recv_messages.empty();}
    AirMessage PopMessage() { return recv_messages.pop();}

    uint64_t get_time_usec();

    inline uint64_t GetStatReceivedMessages() {return receivedMessages; }
    inline uint64_t GetStatReceivedBytes()    {return receivedBytes; }
    inline uint64_t GetStatSentMessages()     {return sentMessages; }
    inline uint64_t GetStatSentBytes()        {return sentBytes; }

    void start_receive();

private:

    void handle_receive(const boost::system::error_code& error, std::size_t bytes_transferred);
    void handle_send(boost::shared_ptr<std::string> /*message*/);

    udp::socket socket_;
    udp::endpoint remote_endpoint_;
    boost::array<uint8_t, 255> recv_buffer_;
    LockedQueue<AirMessage> recv_messages;

    uint64_t get_client_id(udp::endpoint endpoint);

    CommandServer(CommandServer&); // block default copy constructor

    // Statistics
    uint64_t receivedMessages;
    uint64_t receivedBytes;
    uint64_t sentMessages;
    uint64_t sentBytes;
};

#endif // UDP_H
