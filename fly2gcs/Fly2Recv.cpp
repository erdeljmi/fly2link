//  created:    03/04/2016
//  updated:    07/02/2017
//  filename:   FleetControl.cpp
//
//  author:     Milan Erdelj
//
//  version:    $Id: $
//
//  purpose:    Fleet control Qt application
//
//
/*********************************************************************/

#include "Fly2Recv.h"
#include "ui_fleetcontrol.h"
#include <cmath>

#include <zmq.hpp>
#include "include/flatbuffers/flatbuffers.h"
#include "include/fleetgcs-telemetry_generated.h"

#define TIMER_UPDATE_NEIGH_TABLE 500000
#define TIMER_INFO_UAV 500000
#define TIMER_SIMULATE_DRONES 500000
#define TIMER_POLL_ZMQ 100000

using namespace std;

Fly2Recv::Fly2Recv(int sysid, int cmdPort, int infoPort, bool debug_info, const string config_file, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::FleetControl)
{
    ui->setupUi(this);

    QLocale::setDefault(QLocale::c());

    connect(ui->btnRingForm, SIGNAL(clicked()), this, SLOT(SetRingFormation()));
    connect(ui->btnLineForm, SIGNAL(clicked()), this, SLOT(SetLineFormation()));
    connect(ui->btnColumnForm, SIGNAL(clicked()), this, SLOT(SetColumnFormation()));
    connect(ui->btnUpdate, SIGNAL(clicked()), this, SLOT(UpdateZmqHost()));
    //connect(ui->btnSimEnable, SIGNAL(clicked()), this, SLOT(simEnable()));

#ifdef NET_CONNECT
    connect(net_manager,SIGNAL(finished(QNetworkReply*)),this,SLOT(onfinish(QNetworkReply*)));
    // geo conv
    geoConv.initialiseReference(49.402214, 2.795260, 5);
#endif

    geoConv.initialiseReference(49.402214, 2.795260, 5);

    // table view init
    ui->tblUAVs->setColumnCount(COLUMN_COUNT);
    ui->tblUAVs->setRowCount(ROW_COUNT);
    ui->tblUAVs->verticalHeader()->setStyleSheet("color: black");
    ui->tblUAVs->setEditTriggers(QAbstractItemView::NoEditTriggers);
    ui->tblUAVs->verticalHeader()->setVisible(false);
    ui->tblUAVs->horizontalHeader()->setStretchLastSection(true);
    QStringList header_labels;
    header_labels<<"Sys ID"<<"Latitude"<<"Longitude"<<"Altitude"<<"Battery";
    for(unsigned int i = 0; i<ROW_COUNT; i++) {
        for(unsigned int j = 0; j<COLUMN_COUNT; j++) {
            ui->tblUAVs->setItem(i, j, new QTableWidgetItem(""));
            ui->tblUAVs->item(i,j)->setTextAlignment(Qt::AlignCenter);
        }
    }
    ui->tblUAVs->setHorizontalHeaderLabels(header_labels);
    clearTable();

    // target values initialization
    targetX = targetY = 0.0;
    targetZ = 1.0;
    targetPerimeter = 10.0;
    target_id = 0;
    targetHeading = 0.0;

    // set the uav mode to standby
    system_status = MAV_STATE_STANDBY;

    num_drones = ui->numDrones->value();

    // start simulating UAV positions
    timerSimulateDrones.start(TIMER_SIMULATE_DRONES, std::bind(&Fly2Recv::SimulateDrones,this));
    // start updating the table widget
    timerUpdateUAVTable.start(TIMER_UPDATE_NEIGH_TABLE, std::bind(&Fly2Recv::UpdateUAVTable,this));

    timerPollZMQ.start(TIMER_POLL_ZMQ, std::bind(&Fly2Recv::PollZMQ,this));

    current_formation = FLEET_FORMATION_RING;

    zmq_host = ui->textZmqHost->text().toStdString();
}

Fly2Recv::~Fly2Recv() {
    timerSimulateDrones.stop();
    timerUpdateUAVTable.stop();
#ifdef NET_CONNECT
    delete net_manager;
    delete request;
#endif
    delete ui;
}

void Fly2Recv::PollZMQ() {
    // show the received zmq messages
    while(!zeromq.recvMsg.empty())
    {
        QString msg = QString::fromStdString(zeromq.recvMsg.front());
        ui->textRecvCommands->append(msg);
    }
}

void Fly2Recv::SimulateDrones() {
    num_drones = ui->numDrones->value();
    //printf("SIMULATE DRONES Num drones: %d\n", num_drones);
    for(int i=0; i<num_drones; i++)
    {
        infoUAV[i].system_id = i+100; // sysid starting from 100
        PosUAV temp_pos = CalculateFormationPositions(current_formation, targetX, targetY, targetZ, targetPerimeter, targetHeading, num_drones, i);
        infoUAV[i].x = temp_pos.x;
        infoUAV[i].y = temp_pos.y;
        infoUAV[i].z = temp_pos.z;
        infoUAV[i].custom_mode = 0;
        infoUAV[i].system_status = 0;
        double latitude, longitude, altitude;
        geoConv.ned2Geodetic(temp_pos.x, temp_pos.y,temp_pos.z, &latitude, &longitude, &altitude);
        infoUAV[i].latitude = (uint32_t)(latitude*10000000);
        infoUAV[i].longitude = (uint32_t)(longitude*10000000);
        infoUAV[i].altitude = (uint32_t)altitude;
        infoUAV[i].voltage_battery = 12.5;
        //printf("Drone %d lat %ld long %ld\n", infoUAV[i].system_id, infoUAV[i].latitude, infoUAV[i].longitude);
    }

    // TODO send info with ZMQ
}

void Fly2Recv::SetRingFormation() {
    current_formation = FLEET_FORMATION_RING;
    ui->textRecvCommands->append("Ring formation set.");
}

void Fly2Recv::SetColumnFormation() {
    current_formation = FLEET_FORMATION_COLUMN;
    ui->textRecvCommands->append("Column formation set.");
}

void Fly2Recv::SetLineFormation() {
    current_formation = FLEET_FORMATION_LINE;
    ui->textRecvCommands->append("Line formation set.");
}

PosUAV Fly2Recv::CalculateFormationPositions(uint8_t formation, float targ_x, float targ_y, float alt, float perimeter, float heading, uint8_t num_drones, uint8_t id)
{
    PosUAV pos;
    switch(formation) {
    case FLEET_FORMATION_RING:
        pos.x = targ_x + perimeter*cos(id*to_rad(360)/num_drones + to_rad(heading))/2;
        pos.y = targ_y + perimeter*sin(id*to_rad(360)/num_drones + to_rad(heading))/2;
        pos.z = alt;
        break;
    case FLEET_FORMATION_LINE:
        pos.x = targ_x + perimeter*(id/((float)num_drones-1)-0.5)*cos(to_rad(heading-90));
        pos.y = targ_y + perimeter*(id/((float)num_drones-1)-0.5)*sin(to_rad(heading-90));
        pos.z = alt;
        break;
    case FLEET_FORMATION_COLUMN:
        pos.x = targ_x + perimeter*(id/((float)num_drones-1)-0.5)*cos(to_rad(heading));
        pos.y = targ_y + perimeter*(id/((float)num_drones-1)-0.5)*sin(to_rad(heading));
        pos.z = alt;
        break;
    case FLEET_FORMATION_STACK:
        pos.x = targ_x;
        pos.y = targ_y;
        pos.z = alt + id*(perimeter/((float)num_drones-1));
        break;
    default:
        break;
    }
    //printf("[FORMATION %d] ID %d X %.2f Y %.2f Z %.2f\n", formation, i, x, y, z);
    return pos;
}

void Fly2Recv::clearTable() {
    for(unsigned int i = 0; i<ROW_COUNT; i++) {
        for(unsigned int j = 0; j<COLUMN_COUNT; j++) {
            ui->tblUAVs->item(i,j)->setText("");
        }
    }
}

void Fly2Recv::UpdateUAVTable() {
    clearTable();
    num_drones = ui->numDrones->value();
    //num_drones = ui->numDrones->value();
    // populate the tablewidget
    for(int i = 0; i<num_drones; i++) {
        // sys ID
        //printf("UPDATE TABLE Drone %d of %d\n", i, num_drones);
        ui->tblUAVs->item(i,0)->setText(QString::number(infoUAV[i].system_id));
        // lat, long
        ui->tblUAVs->item(i,1)->setText(QString::number(infoUAV[i].latitude));
        ui->tblUAVs->item(i,2)->setText(QString::number(infoUAV[i].longitude));
        ui->tblUAVs->item(i,3)->setText(QString::number(infoUAV[i].altitude));
        // battery
        ui->tblUAVs->item(i,4)->setText(QString::number(infoUAV[i].voltage_battery));

#ifdef NET_CONNECT
        // JSON object
        QJsonObject json;
        json.insert("system_id",mavCom.neighUAVs[current_id].system_id);
        QString address = "192.168.6." + QString::number(mavCom.neighUAVs[current_id].system_id);
        json.insert("ip_address",address);
        json.insert("latitude",mavCom.neighUAVs[current_id].latitude);
        json.insert("longitude",mavCom.neighUAVs[current_id].longitude);
        json.insert("altitude",mavCom.neighUAVs[current_id].altitude);
        json.insert("relative_alt",mavCom.neighUAVs[current_id].relative_alt);
        json.insert("heading",mavCom.neighUAVs[current_id].heading);
        json.insert("type",mavCom.neighUAVs[current_id].type);
        json.insert("autopilot",mavCom.neighUAVs[current_id].autopilot);
        json.insert("base_mode",mavCom.neighUAVs[current_id].base_mode);
        json.insert("custom_mode",mavCom.neighUAVs[current_id].custom_mode);
        json.insert("system_status",mavCom.neighUAVs[current_id].system_status);
        json.insert("voltage_battery",mavCom.neighUAVs[current_id].voltage_battery);
        json.insert("drop_rate_comm",mavCom.neighUAVs[current_id].drop_rate_comm);
        json.insert("errors_comm",mavCom.neighUAVs[current_id].errors_comm);
        json.insert("vx",mavCom.neighUAVs[current_id].vx);
        json.insert("vy",mavCom.neighUAVs[current_id].vy);
        json.insert("vz",mavCom.neighUAVs[current_id].vz);
        json.insert("sensors_present",(uint8_t)mavCom.neighUAVs[current_id].sensors_present);
        json.insert("sensors_enabled",(uint8_t)mavCom.neighUAVs[current_id].sensors_enabled);
        json.insert("sensors_health",(uint8_t)mavCom.neighUAVs[current_id].sensors_health);
        json.insert("mainloop_load",mavCom.neighUAVs[current_id].mainloop_load);
        json.insert("current_battery",mavCom.neighUAVs[current_id].current_battery);
        json.insert("battery_remaining",mavCom.neighUAVs[current_id].battery_remaining);

        QJsonDocument json_doc(json);
        QByteArray json_bytes = json_doc.toJson();
        QByteArray postDataSize = QByteArray::number(json_bytes.size());

        // Add the headers specifying their names and their values
        request->setRawHeader("User-Agent", "StationFleet");
        request->setRawHeader("X-Custom-User-Agent", "StationFleet");
        request->setRawHeader("Content-Type", "application/x-www-form-urlencoded");
        request->setRawHeader("Content-Length", postDataSize);
        request->setHeader(QNetworkRequest::ContentTypeHeader, "application/json");

        // upload request
        net_manager->post(*request,json_bytes);
#endif

        // send this info with ZMQ
        //sendZMQ(i);
    }
    // tablewidget has to be refreshed
    ui->tblUAVs->viewport()->update();
}

void Fly2Recv::UpdateZmqHost() {
    zmq_host = ui->textZmqHost->text().toStdString();
    std::cout << "ZMQ host: " << zmq_host << std::endl;
}

void Fly2Recv::sendZMQ(uint8_t current_id) {
    /**
     * Create a flatbuffer telemetry object
     * Send it with ZeroMQ
     */
    flatbuffers::FlatBufferBuilder builder(1024);

    // IP address
    //QString address = "127.0.0." + QString::number(infoUAV[current_id].system_id);
    //QString address = "127.0.0.1";
    string stdStringAddress = address.toStdString();
    auto systemIp = builder.CreateString(stdStringAddress);

    // UAV data
    auto currentTelemetry = TXData::CreateNeighUAV(
                builder,
                infoUAV[current_id].system_id,
                infoUAV[current_id].comp_id,
                infoUAV[current_id].type,
                infoUAV[current_id].autopilot,
                infoUAV[current_id].base_mode,
                infoUAV[current_id].custom_mode,
                infoUAV[current_id].system_status,
                infoUAV[current_id].voltage_battery,
                infoUAV[current_id].battery_remaining,
                infoUAV[current_id].drop_rate_comm,
                infoUAV[current_id].errors_comm,
                infoUAV[current_id].time_boot_ms,
                infoUAV[current_id].x,
                infoUAV[current_id].y,
                infoUAV[current_id].z,
                infoUAV[current_id].vx,
                infoUAV[current_id].vy,
                infoUAV[current_id].vz,
                infoUAV[current_id].sensors_present,
                infoUAV[current_id].sensors_enabled,
                infoUAV[current_id].sensors_health,
                infoUAV[current_id].mainloop_load,
                infoUAV[current_id].current_battery,
                infoUAV[current_id].latitude,
                infoUAV[current_id].longitude,
                infoUAV[current_id].altitude,
                infoUAV[current_id].relative_alt,
                infoUAV[current_id].heading,
                infoUAV[current_id].last_update,
                systemIp
                );

    builder.Finish(currentTelemetry);
    uint8_t *buf = builder.GetBufferPointer();
    int sizeTel = builder.GetSize();
    zmq::context_t context(1);
    zmq::socket_t sender(context, ZMQ_PUSH);
    //     TODO: let the user configure it
    //    std::string zmq_host = ui->textZmqHost->text().toStdString();
    //    std::cout << "ZMQ host: " << zmq_host << std::endl;
    //    //sender.connect("tcp://localhost:1337");
    sender.connect(zmq_host);

    zmq::message_t msg(sizeTel);
    zmq::message_t envl(sizeof("Telemetry"));
    memcpy((void *) envl.data(), "Telemetry", sizeof("Telemetry"));
    memcpy((void *) msg.data(), buf, sizeTel);
    sender.send(envl, ZMQ_SNDMORE);
    sender.send(msg);
}

#ifdef NET_CONNECT
// connection with the web server
void FleetControl::onfinish(QNetworkReply *reply)
{
    /*
     * Reply is finished!
     * We'll ask for the reply about the Redirection attribute
     * http://doc.trolltech.com/qnetworkrequest.html#Attribute-enum
     */

    //ui->textBox->clear();

    QVariant possibleRedirectUrl = reply->attribute(QNetworkRequest::RedirectionTargetAttribute);

    /* We'll deduct if the redirection is valid in the redirectUrl function */
    _urlRedirectedTo = this->redirectUrl(possibleRedirectUrl.toUrl(),_urlRedirectedTo);

    /* If the URL is not empty, we're being redirected. */
    if(!_urlRedirectedTo.isEmpty()) {
        //QString text = QString("QNAMRedirect::replyFinished: Redirected to ").append(_urlRedirectedTo.toString());
        //ui->textBox->append(text);

        /* We'll do another request to the redirection url. */
        net_manager->get(QNetworkRequest(_urlRedirectedTo));
    }
    else {
        //QString text = QString("QNAMRedirect::replyFinished: Arrived to ").append(reply->url().toString());
        //ui->textBox->append(text);
        _urlRedirectedTo.clear();
    }

    //    /// try to get the data
    //    QByteArray bts = reply->readAll();
    //    QString str(bts);
    //    ui->textBox->append(str);

    /* Clean up. */
    reply->deleteLater();
}

QUrl FleetControl::redirectUrl(const QUrl& possibleRedirectUrl, const QUrl& oldRedirectUrl) const {
    QUrl redirectUrl;
    /*
     * Check if the URL is empty and
     * that we aren't being fooled into a infinite redirect loop.
     * We could also keep track of how many redirects we have been to
     * and set a limit to it, but we'll leave that to you.
     */
    if(!possibleRedirectUrl.isEmpty() &&
            possibleRedirectUrl != oldRedirectUrl) {
        redirectUrl = possibleRedirectUrl;
    }
    return redirectUrl;
}
#endif

double Fly2Recv::to_rad(double deg)
{
    return deg * M_PI / 180.0;
}
