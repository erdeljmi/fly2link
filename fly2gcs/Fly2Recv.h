/*********************************************************************/
//  created:    07/05/2018
//
//  author:     Milan Erdelj, <erdeljmi@gmail.com>
//
//  version:    $Id: $
//
/*********************************************************************/

#ifndef FLEETCONTROL_H
#define FLEETCONTROL_H

#include <QWidget>
#include <QTextStream>
#include <QTableWidget>
#include <vector>
#include <unistd.h>
#include <csignal>
#include "InfoUAV.h"
#include "geodetic_conv.hpp"
#include "CallbackTimer.h"
#include <math.h>
#include "include/HDS_AIRMES/mavlink.h"
#include "ZeroMQ.h"

#define UAV_INFO_TIMEOUT 2500000

#define COLUMN_COUNT 5
#define ROW_COUNT 10

#define MAX_UAVS 10

#define VOLTAGE_CRITICAL 11.5
#define VOLTAGE_REPLACEMENT 12.3

//#define NET_CONNECT
//#define USE_ZEROMQ

namespace Ui {
class FleetControl;
}

struct WaypointElement {
    uint16_t seq;
    uint16_t frame;
    uint16_t command;
    uint16_t type; // 0 mission waypoint, 1 entrance, 2 exit
    uint16_t current;
    uint16_t autocontinue;
    float param1;
    float param2;
    float param3;
    float param4;
    float x;
    float y;
    float z;
};

struct PosUAV {
    float x;
    float y;
    float z;
};

class Fly2Recv : public QWidget
{
    Q_OBJECT
public:
    explicit Fly2Recv(int sysid, int cmdPort, int infoPort, bool debug_info, const std::string config_file, QWidget *parent = 0);
    ~Fly2Recv();

#ifdef NET_CONNECT
    QUrl _urlRedirectedTo;
    QNetworkRequest* request = new QNetworkRequest(QUrl("https://imatisse.hds.utc.fr/fly2/insertDronePos.php"));
    //QNetworkRequest request;
    QNetworkAccessManager *net_manager = new QNetworkAccessManager(this);
#endif

    // setpoint values are stored here
    float targetX, targetY, targetZ;
    float targetHeading;
    float targetPerimeter; // perimeter for the ring formation
    int target_id;

    int num_drones = 0;

    uint8_t current_formation;

    // geodetic converter
    GeodeticConverter geoConv;

    uint8_t system_status;

    // from fleet manager:
    uint8_t my_system_id;

    int system_id;

private:
    Ui::FleetControl *ui;

    CallbackTimer timerSimulateDrones;
    CallbackTimer timerUpdateUAVTable;
    CallbackTimer timerPollZMQ;

    ZeroMQ zeromq;

    std::string zmq_host;

    //std::vector<InfoUAV> infoUAV;
    InfoUAV infoUAV[MAX_UAVS];

public slots:

    void SetRingFormation();
    void SetLineFormation();
    void SetColumnFormation();

    void UpdateZmqHost();

    void SimulateDrones();
    void UpdateUAVTable();

    void clearTable();

    PosUAV CalculateFormationPositions(uint8_t formation, float targ_x, float targ_y, float alt, float perimeter, float heading, uint8_t num_drones, uint8_t id);

    void sendZMQ(uint8_t current_id);

    void PollZMQ();

#ifdef NET_CONNECT
    // network
    void onfinish(QNetworkReply *reply);
    QUrl redirectUrl(const QUrl& possibleRedirectUrl, const QUrl& oldRedirectUrl) const;
#endif

    double to_rad(double);
};

#endif // MISSIONPLANNER_H
