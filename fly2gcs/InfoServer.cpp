/*********************************************************************/
//  created:    17/10/2017
//
//  author:     Milan Erdelj, <erdeljmi@gmail.com>
//
//  version:    $Id: $
//
/*********************************************************************/

#include "InfoServer.h"

InfoServer::InfoServer(boost::asio::io_service &io_service, int port) :
    bcast_endpoint(boost::asio::ip::address_v4::broadcast(), port),
    socket_(io_service, udp::endpoint(udp::v4(), port))
{
    // socket options
    socket_.set_option(boost::asio::socket_base::broadcast(true));
    socket_.set_option(boost::asio::ip::udp::socket::reuse_address(true));
    start_receive();
}

void InfoServer::start_receive()
{
    socket_.async_receive_from(boost::asio::buffer(recv_buffer_), remote_endpoint_, boost::bind(&InfoServer::handle_receive, this, boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred));
    //std::cout << "INFO async receive remote endpoint: " << remote_endpoint_ << std::endl;
}

void InfoServer::handle_receive(const boost::system::error_code& error, std::size_t bytes_transferred)
{
    if (!error || error == boost::asio::error::message_size)
    {
        //std::cout << "[CMD] Received " << bytes_transferred << " bytes from " << remote_endpoint_ << std::endl;
        AirMessage msg;
        unsigned char buff[MAX_BUFF_SIZE];
        for(unsigned int i = 0; i < bytes_transferred; i++) {
            buff[i] = recv_buffer_[i];
        }
        if(!msg.Unpack(buff)){
            msg.endpoint_address = remote_endpoint_.address().to_string();
            recv_messages.push(msg);
        }
        start_receive();
    }
}

void InfoServer::handle_send(boost::shared_ptr<std::string> message)
{
}

InfoServer::~InfoServer()
{
    socket_.close();
}

void InfoServer::broadcast(string message, uint8_t sourceId, uint16_t sequence, AirmeshMessageType type)
{
    AirMessage msg;
    //int Pack(string msg_body, uint8_t sourceId, uint8_t destId, uint16_t msg_len, uint16_t sequence, uint8_t type);
    msg.Pack(message, sourceId, 255, message.length(), sequence, type);
    //std::cout << "sendMinInfo" << std::endl;
    socket_.send_to(boost::asio::buffer(msg.messageBuffer), bcast_endpoint);
}

void InfoServer::discovery(uint8_t sourceId, AirmeshMessageType type)
{
    AirMessage msg;
    string discovery("x");
    msg.Pack(discovery, sourceId, 255, discovery.length(), 0, type);
    socket_.send_to(boost::asio::buffer(msg.messageBuffer), bcast_endpoint);
}

uint64_t InfoServer::get_time_usec() {
    struct timeval tv;
    gettimeofday(&tv, NULL);
    return ((uint64_t)tv.tv_sec) * 1000000 + tv.tv_usec;
}
