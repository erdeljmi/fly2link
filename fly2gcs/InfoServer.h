/*********************************************************************/
//  created:    17/10/2017
//
//  author:     Milan Erdelj, <erdeljmi@gmail.com>
//
//  version:    $Id: $
//
/*********************************************************************/

#ifndef INFOUDP_H
#define INFOUDP_H

#include <boost/bind.hpp>
#include <boost/array.hpp>
#include <boost/asio.hpp>
#include "AirMessage.h"
#include "LockedQueue.h"

#define MAX_BUFF_SIZE 400

using boost::asio::ip::udp;

class InfoServer
{
public:
    InfoServer(boost::asio::io_service& io_service, int port);
    ~InfoServer();

    //void sendInfo(cid_hds::CidInfo ros_msg, uint8_t sourceId, uint16_t sequence, AirmeshMessageType type);
    void broadcast(string msg, uint8_t sourceId, uint16_t sequence, AirmeshMessageType type);
    void discovery(uint8_t sourceId, AirmeshMessageType type);

    bool HasMessages() {return !recv_messages.empty();}
    AirMessage PopMessage() { return recv_messages.pop();}

    uint64_t get_time_usec();

    void start_receive();

private:

    void handle_receive(const boost::system::error_code& error, std::size_t bytes_transferred);
    void handle_send(boost::shared_ptr<std::string> /*message*/);

    udp::socket socket_;
    udp::endpoint remote_endpoint_;
    boost::array<uint8_t, MAX_BUFF_SIZE> recv_buffer_;
    LockedQueue<AirMessage> recv_messages;

    udp::endpoint bcast_endpoint;

    InfoServer(InfoServer&); // block default copy constructor
};

#endif // UDP_H
