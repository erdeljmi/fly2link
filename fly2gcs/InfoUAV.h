#ifndef NEIGHUAV_H
#define NEIGHUAV_H

#include <stdint.h>

class InfoUAV
{
public:
    InfoUAV() {}
    ~InfoUAV() {}
    uint8_t system_id;
    uint8_t comp_id;
    // from heartbeat
    uint8_t type;
    uint8_t autopilot;
    uint8_t base_mode;
    uint8_t custom_mode;
    uint8_t system_status;
    // from system status
    uint16_t voltage_battery; // in millivolts
    int8_t battery_remaining; // in percent, if -1 it means that autopilot estimates
    uint16_t drop_rate_comm;
    uint16_t errors_comm;
    // from local position ned
    uint32_t time_boot_ms;
    float x, y, z;
    float vx, vy, vz;
    uint32_t sensors_present;
    uint32_t sensors_enabled;
    uint32_t sensors_health;
    uint16_t mainloop_load;
    uint16_t current_battery;
    int32_t latitude;
    int32_t longitude;
    int32_t altitude;
    int32_t relative_alt;
    uint16_t heading;
    uint64_t last_update;
private:
};

#endif // NEIGHUAV_H
