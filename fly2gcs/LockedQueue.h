/*********************************************************************/
//  created:    07/05/2015
//
//  author:     Milan Erdelj, <erdeljmi@gmail.com>
//
//  version:    $Id: $
//
/*********************************************************************/

#ifndef LOCKED_QUEUE_H
#define LOCKED_QUEUE_H

#include <boost/thread/mutex.hpp>
#include <queue>
#include <list>

template<typename _T> class LockedQueue
{
private:
    boost::mutex mutex;
    std::queue<_T> queue;
public:
    void push(_T value)
    {
        boost::mutex::scoped_lock lock(mutex);
        queue.push(value);
    };

    _T pop()
    {
        boost::mutex::scoped_lock lock(mutex);
        _T value;
        std::swap(value,queue.front());
        queue.pop();
        return value;
    };

    bool empty() {
        boost::mutex::scoped_lock lock(mutex);
        return queue.empty();
    }
};

#endif // LOCKED_QUEUE_H
