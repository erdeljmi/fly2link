/***************************************************/
//  created:    04/04/2018
//
//  author:     Milan Erdelj, <erdeljmi@gmail.com>
//
//  version:    $Id: $
//
/***************************************************/

#include "MavlinkManager.h"
#include <sstream>
#include <iostream>

using namespace std;

MavlinkManager::MavlinkManager(uint8_t sysid, int cmdPort, int infoPort, bool debug_info, string config_file) : my_system_id(sysid), config_file(config_file)
{
    home_position_set=false;
    ack=false;
    request=false;
    seq=-1;
    seqr=-1;
    seqold=-1;
    waypoint=false;
    Waycount=0;
    Wayseq=-1;
    message_interval = 1000; // ms
    state=-1;
    debug_messages = true;

    req_accepted = false;
    replacement_in_position = false;

    compt=0;
    current_messages.sysid  = system_id;
    current_messages.compid = autopilot_id;
}

MavlinkManager::~MavlinkManager() {
}

inline void MavlinkManager::clearBuffer(uint8_t *buffer, int len) {
    memset(buffer, 0, len);
}

std::string MavlinkManager::mavlink_to_string(const mavlink_message_t *msg) {
    uint8_t message_buff[BUFF_OUT_LEN];
    uint16_t length = mavlink_msg_to_send_buffer(message_buff,msg);
    std::string s(reinterpret_cast<char const*>(message_buff), length);
    return s;
}

// pack Heartbeat message
string MavlinkManager::Heartbeat(uint8_t heartbeat_uav_type, uint8_t uav_autopilot, uint8_t uav_base_mode, uint32_t uav_custom_mode, uint8_t uav_system_status) {
    mavlink_message_t message;
    mavlink_msg_heartbeat_pack(my_system_id, 0, &message, heartbeat_uav_type, uav_autopilot, uav_base_mode, uav_custom_mode, uav_system_status);
    return mavlink_to_string(&message);
}

// pack CommandLong message
string MavlinkManager::CommandLong(uint8_t targetSystem, uint8_t targetComponent, uint16_t command, uint8_t confirmation, float param1, float param2, float param3, float param4, float param5, float param6, float param7) {
    mavlink_message_t message;
    mavlink_msg_command_long_pack(my_system_id, 0, &message, targetSystem, targetComponent, command, confirmation, param1, param2, param3, param4, param5, param6, param7);
    return mavlink_to_string(&message);
}

// pack SystemStatus message
string MavlinkManager::SystemStatus(uint32_t onboardSensorsPresent, uint32_t onboardSensorsEnabled, uint32_t onboardSensorsHealth, uint16_t load, uint16_t voltage, int16_t current, int8_t batteryRemaining, uint16_t dropRateComm, uint16_t errorsComm, uint16_t errors1, uint16_t errors2, uint16_t errors3, uint16_t errors4) {
    mavlink_message_t message;
    mavlink_msg_sys_status_pack(my_system_id, my_comp_id, &message, onboardSensorsPresent, onboardSensorsEnabled, onboardSensorsHealth, load, voltage, current, batteryRemaining, dropRateComm, errorsComm, errors1, errors2, errors3, errors4);
    return mavlink_to_string(&message);
}

// pack LocalPostiionNED message
string MavlinkManager::LocalPositionNED(float x, float y, float z, float vx, float vy, float vz) {
    mavlink_message_t message;
    uint64_t current_time = get_time_usec();
    mavlink_msg_local_position_ned_pack(my_system_id, my_comp_id, &message, current_time, x, y, z, vx, vy, vz);
    return mavlink_to_string(&message);
}

//// send BatteryStatus message
//void MavlinkPack::sendBatteryStatus(uint8_t id, uint8_t battery_function, uint8_t type, int16_t temperature, uint16_t *voltages, int16_t current, int32_t currentConsumed, int32_t energyConsumed, int8_t batteryRemaining) {
//    mavlink_message_t message;
//    mavlink_msg_battery_status_pack(my_system_id, my_comp_id, &message, id, battery_function, type, temperature, voltages, current, currentConsumed, energyConsumed, batteryRemaining);
//    tempMessageUnicast.length = mavlink_msg_to_send_buffer(tempMessageUnicast.contents,&message);
//    queueUnicast.push(tempMessageUnicast);
//}

//// send PowerStatus message
//void MavlinkPack::sendPowerStatus(uint16_t Vcc, uint16_t Vservo, uint16_t flags)
//{
//    mavlink_message_t message;
//    uint64_t current_time = get_time_usec();
//    mavlink_msg_power_status_pack(my_system_id, my_comp_id, &message, Vcc, Vservo, flags);
//    tempMessageUnicast.length = mavlink_msg_to_send_buffer(tempMessageUnicast.contents,&message);
//    queueUnicast.push(tempMessageUnicast);
//    printf("%lld [SENT] POWER_STATUS %d %d %d\n", (long long int)current_time, Vcc, Vservo, flags);
//}

//// send SystemTime message
//void MavlinkPack::sendSystemTime() {
//    mavlink_system_time_t sys_time;
//    sys_time.time_unix_usec = get_time_usec();
//    mavlink_message_t message;
//    mavlink_msg_system_time_pack(my_system_id, my_comp_id, &message, sys_time.time_unix_usec, sys_time.time_boot_ms);
//    tempMessageUnicast.length = mavlink_msg_to_send_buffer(tempMessageUnicast.contents,&message);
//    queueUnicast.push(tempMessageUnicast);
//    printf("[SENT] System time\n");
//}

//// send GlobalPositionInt message
//void MavlinkPack::sendGlobalPositionInt(int32_t lat, int32_t lon, int32_t alt, int32_t relativeAlt, int16_t vx, int16_t vy, int16_t vz, uint16_t yawAngle) {
//    mavlink_message_t message;
//    mavlink_msg_global_position_int_pack(my_system_id, my_comp_id, &message, get_time_usec(), lat, lon, alt, relativeAlt, vx, vy, vz, yawAngle);
//    tempMessageUnicast.length = mavlink_msg_to_send_buffer(tempMessageUnicast.contents,&message);
//    queueUnicast.push(tempMessageUnicast);
//}

//// send Attitude message
//void MavlinkPack::sendAttitude(float roll, float pitch, float yaw, float rollspeed, float pitchspeed, float yawspeed) {
//    mavlink_message_t message;
//    mavlink_msg_attitude_pack(my_system_id, my_comp_id, &message, get_time_usec(), roll, pitch, yaw, rollspeed, pitchspeed, yawspeed);
//    tempMessageUnicast.length = mavlink_msg_to_send_buffer(tempMessageUnicast.contents,&message);
//    queueUnicast.push(tempMessageUnicast);
//}

//// send AttitudeTarget
//void MavlinkPack::sendAttitudeTarget(uint32_t timeBootMs, uint8_t typeMask, float *g, float bodyRollRate, float bodyPitch_rate, float bodyYawRate, float thrust) {
//    mavlink_message_t message;
//    mavlink_msg_attitude_target_pack(my_system_id, my_comp_id, &message, timeBootMs, typeMask, g, bodyRollRate, bodyPitch_rate, bodyYawRate, thrust);
//    tempMessageUnicast.length = mavlink_msg_to_send_buffer(tempMessageUnicast.contents,&message);
//    queueUnicast.push(tempMessageUnicast);
//}

//// send MissionAck message
//void MavlinkPack::sendMissionAck(uint8_t targetSystem, uint8_t targetComponent, uint8_t type) {
//    mavlink_message_t message;
//    mavlink_msg_mission_ack_pack(my_system_id, my_comp_id, &message, targetSystem, targetComponent, type);
//    tempMessageUnicast.length = mavlink_msg_to_send_buffer(tempMessageUnicast.contents,&message);
//    queueUnicast.push(tempMessageUnicast);
//    printf("[SENT] Mission ACK\n");
//}

//// send commandAck message
//void MavlinkPack::sendCommandAck(uint16_t command, uint8_t result) {
//    mavlink_message_t message;
//    mavlink_msg_command_ack_pack(my_system_id, my_comp_id, &message, command, result);
//    tempMessageUnicast.length = mavlink_msg_to_send_buffer(tempMessageUnicast.contents,&message);
//    queueUnicast.push(tempMessageUnicast);
//    printf("%lld [SENT] Command ACK: %d\n", (long long int)get_time_usec(), command);
//}

//// send autopilot version
//void MavlinkPack::sendAutopilotVersion(uint64_t capabilities, uint32_t flight_sw_version, uint32_t middleware_sw_version, uint32_t os_sw_version, uint32_t board_version, uint8_t *flight_custom_version, uint8_t *middleware_custom_version, uint8_t *os_custom_version, uint16_t vendor_id, uint16_t product_id, uint64_t uid) {
//    mavlink_message_t message;
//    // TODO get these values from autopilot
//    mavlink_msg_autopilot_version_pack(my_system_id, my_comp_id, &message, capabilities, flight_sw_version, middleware_sw_version, os_sw_version, board_version, flight_custom_version, middleware_custom_version, os_custom_version, vendor_id, product_id, uid);
//    tempMessageUnicast.length = mavlink_msg_to_send_buffer(tempMessageUnicast.contents,&message);
//    queueUnicast.push(tempMessageUnicast);
//    printf("[SENT] Autopilot vesrion\n");
//}

//// send MissionCount message
//void MavlinkPack::sendMissionCount(uint8_t targetSystem, uint8_t targetComponent, uint16_t count) {
//    mavlink_message_t message;
//    mavlink_msg_mission_count_pack(my_system_id, my_comp_id, &message, targetSystem, targetComponent, count);
//    tempMessageUnicast.length = mavlink_msg_to_send_buffer(tempMessageUnicast.contents,&message);
//    queueUnicast.push(tempMessageUnicast);
//}

//// send MissionWritePartialList message
//void MavlinkPack::sendMissionWritePartialList(uint8_t targetSystem, uint8_t targetComponent, uint16_t startIndex, uint16_t endIndex) {
//    mavlink_message_t message;
//    mavlink_msg_mission_write_partial_list_pack(my_system_id, my_comp_id, &message, targetSystem, targetComponent, startIndex, endIndex);
//    tempMessageUnicast.length = mavlink_msg_to_send_buffer(tempMessageUnicast.contents,&message);
//    queueUnicast.push(tempMessageUnicast);
//}

// send MissionItem message
string MavlinkManager::MissionItem(uint8_t targetSystem, uint8_t targetComponent, uint16_t seq, uint8_t frame, uint16_t command, uint8_t current, uint8_t autocontinue, float param1, float param2, float param3, float param4, float x, float y, float z) {
    mavlink_message_t message;
    mavlink_msg_mission_item_pack(my_system_id, my_comp_id, &message, targetSystem, targetComponent, seq, frame, command, current, autocontinue, param1, param2, param3, param4, x, y, z);
    return mavlink_to_string(&message);
}

//// send MissionRequestList message
//void MavlinkPack::sendMissionRequestList(uint8_t targetSystem, uint8_t targetComponent) {
//    mavlink_message_t message;
//    mavlink_msg_mission_request_list_pack(my_system_id, my_comp_id, &message, targetSystem, targetComponent);
//    tempMessageUnicast.length = mavlink_msg_to_send_buffer(tempMessageUnicast.contents,&message);
//    queueUnicast.push(tempMessageUnicast);
//}

//// send MissionItemReached message
//void MavlinkPack::sendMissionItemReached(uint16_t seq) {
//    mavlink_message_t message;
//    mavlink_msg_mission_item_reached_pack(my_system_id, my_comp_id, &message, seq);
//    tempMessageUnicast.length = mavlink_msg_to_send_buffer(tempMessageUnicast.contents,&message);
//    queueUnicast.push(tempMessageUnicast);
//    // XXX sent to broadcast queue too
//    tempMessageBroadcast.length = mavlink_msg_to_send_buffer(tempMessageBroadcast.contents,&message);
//    queueBroadcast.push(tempMessageBroadcast);
//    printf("%lld [SENT] MISSION_ITEM_REACHED %d\n", (long long int)get_time_usec(), seq);
//}

// send MissionSetCurrent message
string MavlinkManager::MissionSetCurrent(uint8_t targetSystem, uint8_t targetComponent, uint16_t seq) {
    mavlink_message_t message;
    mavlink_msg_mission_set_current_pack(my_system_id, my_comp_id, &message, targetSystem, targetComponent, seq);
    return mavlink_to_string(&message);
}

// send MissionClearAll message
string MavlinkManager::MissionClearAll(uint8_t targetSystem, uint8_t targetComponent) {
    mavlink_message_t message;
    mavlink_msg_mission_clear_all_pack(my_system_id, my_comp_id, &message, targetSystem, targetComponent);
    return mavlink_to_string(&message);
}

//// send SetPositionTargetLocalNED message
//void MavlinkPack::sendSetPositionTargetLocalNED(uint32_t time_boot_ms, uint8_t targetSystem, uint8_t targetComponent, uint8_t coordinateFrame, uint16_t typeMask, float x, float y, float z, float vx, float vy, float vz, float afx, float afy, float afz, float yaw, float yaw_rate) {
//    mavlink_message_t message;
//    mavlink_msg_set_position_target_local_ned_pack(my_system_id, my_comp_id, &message, time_boot_ms, targetSystem, targetComponent, coordinateFrame, typeMask, x, y, z, vx, vy, vz, afx, afy, afz, yaw, yaw_rate);
//    printf("%lld [SENT] SET_POSITION_TARGET_LOCAL_NED x = %.2f y = %.2f z = %.2f\n", (long long int)get_time_usec(), x, y, z);
//    tempMessageUnicast.length = mavlink_msg_to_send_buffer(tempMessageUnicast.contents,&message);
//    queueUnicast.push(tempMessageUnicast);
//}

//// send PositionTargetLocalNED message
//void MavlinkPack::sendPositionTargetLocalNED(uint32_t time_boot_ms, uint8_t coordinateFrame, uint16_t typeMask, float x, float y, float z, float vx, float vy, float vz, float afx, float afy, float afz, float yaw, float yaw_rate) {
//    mavlink_message_t message;
//    mavlink_msg_position_target_local_ned_pack(my_system_id, my_comp_id, &message, time_boot_ms, coordinateFrame, typeMask, x, y, z, vx, vy, vz, afx, afy, afz, yaw, yaw_rate);
//    tempMessageUnicast.length = mavlink_msg_to_send_buffer(tempMessageUnicast.contents,&message);
//    queueUnicast.push(tempMessageUnicast);
//}

//// send SetPositionTargetGlobalInt message
//void MavlinkPack::sendSetPositionTargetGlobalInt(uint32_t time_boot_ms, uint8_t targetSystem, uint8_t targetComponent, uint8_t coordinateFrame, uint16_t typeMask, int32_t lat_int, int32_t lon_int, float alt, float vx, float vy, float vz, float afx, float afy, float afz, float yaw, float yaw_rate) {
//    mavlink_message_t message;
//    mavlink_msg_set_position_target_global_int_pack(my_system_id, my_comp_id, &message, time_boot_ms, targetSystem, targetComponent, coordinateFrame, typeMask, lat_int, lon_int, alt, vx, vy, vz, afx, afy, afz, yaw, yaw_rate);
//    tempMessageUnicast.length = mavlink_msg_to_send_buffer(tempMessageUnicast.contents,&message);
//    queueUnicast.push(tempMessageUnicast);
//}

//// send PositionTargetGlobalInt message
//void MavlinkPack::sendPositionTargetGlobalInt(uint32_t time_boot_ms, uint8_t coordinateFrame, uint16_t typeMask, int32_t lat_int, int32_t lon_int, float alt, float vx, float vy, float vz, float afx, float afy, float afz, float yaw, float yaw_rate) {
//    mavlink_message_t message;
//    mavlink_msg_position_target_global_int_pack(my_system_id, my_comp_id, &message, time_boot_ms, coordinateFrame, typeMask, lat_int, lon_int, alt, vx, vy, vz, afx, afy, afz, yaw, yaw_rate);
//    tempMessageUnicast.length = mavlink_msg_to_send_buffer(tempMessageUnicast.contents,&message);
//    queueUnicast.push(tempMessageUnicast);
//}

//// send RC_Channels
//void MavlinkPack::sendRCChannels(uint32_t time_boot_ms, uint8_t chancount, uint16_t chan1_raw, uint16_t chan2_raw, uint16_t chan3_raw, uint16_t chan4_raw, uint16_t chan5_raw, uint16_t chan6_raw, uint16_t chan7_raw, uint16_t chan8_raw, uint16_t chan9_raw, uint16_t chan10_raw, uint16_t chan11_raw, uint16_t chan12_raw, uint16_t chan13_raw, uint16_t chan14_raw, uint16_t chan15_raw, uint16_t chan16_raw, uint16_t chan17_raw, uint16_t chan18_raw, uint8_t rssi) {
//    mavlink_message_t message;
//    mavlink_msg_rc_channels_pack(my_system_id, my_comp_id, &message, time_boot_ms, chancount, chan1_raw, chan2_raw, chan3_raw, chan4_raw, chan5_raw, chan6_raw, chan7_raw, chan8_raw, chan9_raw, chan10_raw, chan11_raw, chan12_raw, chan13_raw, chan14_raw, chan15_raw, chan16_raw, chan17_raw, chan18_raw, rssi);
//    tempMessageUnicast.length = mavlink_msg_to_send_buffer(tempMessageUnicast.contents,&message);
//    queueUnicast.push(tempMessageUnicast);
//}

//// send HighresIMU
//void MavlinkPack::sendHighresIMU(uint64_t time_usec, float xacc, float yacc, float zacc, float xgyro, float ygyro, float zgyro, float xmag, float ymag, float zmag, float abs_pressure, float diff_pressure, float pressure_alt, float temperature, uint16_t fields_updated) {
//    mavlink_message_t message;
//    mavlink_msg_highres_imu_pack(my_system_id, my_comp_id, &message, time_usec, xacc, yacc, zacc, xgyro, ygyro, zgyro, xmag, ymag, zmag, abs_pressure, diff_pressure, pressure_alt, temperature, fields_updated);
//    tempMessageUnicast.length = mavlink_msg_to_send_buffer(tempMessageUnicast.contents,&message);
//    queueUnicast.push(tempMessageUnicast);
//}

//// send OpticalFlowRad
//void MavlinkPack::sendOpticalFlowRad(uint64_t time_usec, uint8_t sensor_id, uint32_t integration_time_us, float integrated_x, float integrated_y, float integrated_xgyro, float integrated_ygyro, float integrated_zgyro, int16_t temperature, uint8_t quality, uint32_t time_delta_distance_us, float distance) {
//    mavlink_message_t message;
//    mavlink_msg_optical_flow_rad_pack(my_system_id, my_comp_id, &message, time_usec, sensor_id, integration_time_us, integrated_x, integrated_y, integrated_xgyro, integrated_ygyro, integrated_zgyro, temperature, quality, time_delta_distance_us, distance);
//    tempMessageUnicast.length = mavlink_msg_to_send_buffer(tempMessageUnicast.contents,&message);
//    queueUnicast.push(tempMessageUnicast);
//}

//// send ManualControl
//void MavlinkPack::sendManualControl(uint8_t target, int16_t x, int16_t y, int16_t z, int16_t r, uint16_t buttons) {
//    mavlink_message_t message;
//    mavlink_msg_manual_control_pack(my_system_id, my_comp_id, &message, target, x, y, z, r, buttons);
//    tempMessageUnicast.length = mavlink_msg_to_send_buffer(tempMessageUnicast.contents,&message);
//    queueUnicast.push(tempMessageUnicast);
//}

//// send FleetUAVInfoLocal message
//void MavlinkPack::sendFleetUavInfoLocal(uint8_t base_mode, uint8_t custom_state, uint8_t system_status, uint16_t voltage_battery, uint16_t battery_remaining, uint16_t drop_rate_comm, uint16_t errors_comm, float x, float y, float z, float heading) {
//    mavlink_message_t message;
//    mavlink_msg_fleet_uav_info_local_pack(my_system_id, my_comp_id, &message, base_mode, custom_state, system_status, voltage_battery, battery_remaining, drop_rate_comm, errors_comm, x, y, z, heading);
//    tempMessageBroadcast.length = mavlink_msg_to_send_buffer(tempMessageBroadcast.contents,&message);
//    queueBroadcast.push(tempMessageBroadcast);
//    printf("%lld [SENT] FLEET_UAV_INFO_LOCAL %d %d %d %d %d %d %d %.2f %.2f %.2f %.2f\n", (long long int)get_time_usec(), base_mode, custom_state, system_status, voltage_battery, battery_remaining, drop_rate_comm, errors_comm, x, y, z, heading);
//}

//// send FleetUAVInfoGlobal message
//void MavlinkPack::sendFleetUavInfoGlobal(uint8_t base_mode, uint8_t custom_state, uint8_t system_status, uint16_t voltage_battery, uint16_t battery_remaining, uint16_t drop_rate_comm, uint16_t errors_comm, uint32_t latitude, uint32_t longitude, uint16_t altitude, uint16_t relative_alt, int16_t heading) {
//    mavlink_message_t message;
//    mavlink_msg_fleet_uav_info_global_pack(my_system_id, my_comp_id, &message, base_mode, custom_state, system_status, voltage_battery, battery_remaining, drop_rate_comm, errors_comm, latitude, longitude, altitude, relative_alt, heading);
//    tempMessageBroadcast.length = mavlink_msg_to_send_buffer(tempMessageBroadcast.contents,&message);
//    queueBroadcast.push(tempMessageBroadcast);
//    printf("%lld [SENT] FLEET_UAV_INFO_GLOBAL %d %d %d %d %d %d %d %d %d %d %d %d\n", (long long int)get_time_usec(), base_mode, custom_state, system_status, voltage_battery, battery_remaining, drop_rate_comm, errors_comm, latitude, longitude, altitude, relative_alt, heading);
//}

// send FleetUAVRequestLocal message
string MavlinkManager::FleetUavRequestLocal(uint8_t requestType, uint8_t requestID, uint8_t idToReplace, float x, float y, float z, uint8_t urgency) {
    mavlink_message_t message;
    mavlink_msg_fleet_uav_request_local_pack(my_system_id, my_comp_id, &message, requestType, requestID, idToReplace, x, y, z, urgency);
    return mavlink_to_string(&message);
}

//// send FleetUAVRequestGlobal message
//void MavlinkPack::sendFleetUavRequestGlobal(uint8_t requestType, uint8_t requestID, uint8_t idToReplace, uint32_t latitude, uint32_t longitude, uint16_t altitude, uint8_t urgency) {
//    mavlink_message_t message;
//    mavlink_msg_fleet_uav_request_global_pack(my_system_id, my_comp_id, &message, requestType, requestID, idToReplace, latitude, longitude, altitude, urgency);
//    tempMessageBroadcast.length = mavlink_msg_to_send_buffer(tempMessageBroadcast.contents,&message);
//    queueBroadcast.push(tempMessageBroadcast);
//    printf("%lld [SENT] FLEET_UAV_REQUEST_GLOBAL %d %d %d %d %d %d %d\n", (long long int)get_time_usec(), requestType, requestID, idToReplace, latitude, longitude, altitude, urgency);
//}

//// send FleetUavRequestResponse message
//void MavlinkPack::sendFleetUavRequestResponse(uint8_t request_id, uint64_t arrival_time) {
//    mavlink_message_t message;
//    mavlink_msg_fleet_uav_request_response_pack(my_system_id, my_comp_id, &message, request_id, arrival_time);
//    // FIXME change to unicast
//    tempMessageBroadcast.length = mavlink_msg_to_send_buffer(tempMessageBroadcast.contents,&message);
//    queueBroadcast.push(tempMessageBroadcast);
//    printf("%lld [SENT] FLEET_UAV_REQUEST_RESPONSE %d %lld\n", (long long int)get_time_usec(), request_id, (long long int)arrival_time);
//}

// send FleetUavElectedID message
string MavlinkManager::FleetUavElectedID(uint8_t request_id, uint8_t elected_id) {
    mavlink_message_t message;
    mavlink_msg_fleet_uav_elected_id_pack(my_system_id, my_comp_id, &message, request_id, elected_id);
    return mavlink_to_string(&message);
}

//// send FleetUavElectedAccept message
//void MavlinkPack::sendFleetUavElectedAccept(uint8_t request_id) {
//    mavlink_message_t message;
//    mavlink_msg_fleet_uav_elected_accept_pack(my_system_id, my_comp_id, &message, request_id);
//    // FIXME change to unicast
//    tempMessageBroadcast.length = mavlink_msg_to_send_buffer(tempMessageBroadcast.contents,&message);
//    queueBroadcast.push(tempMessageBroadcast);
//    printf("%lld [SENT] FLEET_UAV_ELECTED_ACCEPT %d\n", (long long int)get_time_usec(), request_id);
//}

//// send FleetReplacementInPosition message
//void MavlinkPack::sendFleetReplacementInPosition(uint8_t request_id) {
//    mavlink_message_t message;
//    mavlink_msg_fleet_replacement_in_position_pack(my_system_id, my_comp_id, &message, request_id);
//    // FIXME change to unicast
//    tempMessageBroadcast.length = mavlink_msg_to_send_buffer(tempMessageBroadcast.contents,&message);
//    queueBroadcast.push(tempMessageBroadcast);
//    printf("%lld [SENT] FLEET_REPLACEMENT_IN_POSITION %d\n", (long long int)get_time_usec(), request_id);
//}

//// send FleetSetFormation message
//void MavlinkPack::sendFleetSetFormation(uint8_t formationType, uint8_t leaderID, uint32_t barycenter_lat, uint32_t barycenter_long, uint16_t barycenter_alt, uint16_t barycenter_relative_alt, float width_bound, float length_bound, float height_bound, double d, double epsilon, double FoV, double AttractMax, double RepulsionMax, double h_, double c1, double c2, double Kd_form, double Kp_form, double Ki_form, double T, double ux_sat, double uy_sat, double uz_sat, double Radius, uint8_t norm_type) {
//    mavlink_message_t message;
//    mavlink_msg_fleet_set_formation_pack(my_system_id, my_comp_id, &message, formationType, leaderID, barycenter_lat, barycenter_long, barycenter_alt, barycenter_relative_alt, width_bound, length_bound, height_bound, d, epsilon, FoV, AttractMax, RepulsionMax, h_, c1, c2, Kd_form, Kp_form, Ki_form, T, ux_sat, uy_sat, uz_sat, Radius, norm_type);
//    tempMessageBroadcast.length = mavlink_msg_to_send_buffer(tempMessageBroadcast.contents,&message);
//    queueBroadcast.push(tempMessageBroadcast);
//    printf("%lld [SENT] FLEET_SET_FORMATION %d %d %d %d %d %d %.2f %.2f %.2f\n", (long long int)get_time_usec(), formationType, leaderID, barycenter_lat, barycenter_long, barycenter_alt, barycenter_relative_alt, width_bound, length_bound, height_bound);
//}

// send FleetSetTargetLocal message
string MavlinkManager::FleetSetTargetLocal(uint8_t target_id, uint8_t target_type, float x, float y, float z, int16_t heading, uint8_t add_replace, uint16_t drone_count, float perimeter) {
    mavlink_message_t message;
    mavlink_msg_fleet_set_target_local_pack(my_system_id, my_comp_id, &message, target_id, target_type, x, y, z, heading, add_replace, drone_count, perimeter);
    return mavlink_to_string(&message);
}

//// send FleetSetTarget message
//void MavlinkPack::sendFleetSetTarget(uint8_t target_id, uint8_t target_type, uint32_t latitude, uint32_t longitude, uint16_t altitude, uint16_t relative_alt, int16_t heading, uint8_t add_replace, uint16_t drone_count, float perimeter) {
//    mavlink_message_t message;
//    mavlink_msg_fleet_set_target_pack(my_system_id, my_comp_id, &message, target_id, target_type, latitude, longitude, altitude, relative_alt, heading, add_replace, drone_count, perimeter);
//    tempMessageBroadcast.length = mavlink_msg_to_send_buffer(tempMessageBroadcast.contents,&message);
//    queueBroadcast.push(tempMessageBroadcast);
//    printf("%lld [SENT] FLEET_SET_TARGET %d %d %d %d %d %d %d %d %d %.2f\n", (long long int)get_time_usec(), target_id, target_type, latitude, longitude, altitude, relative_alt, heading, add_replace, drone_count, perimeter);
//}

//// send FleetClearTarget message
//void MavlinkPack::sendFleetClearTarget(uint8_t target_id) {
//    mavlink_message_t message;
//    mavlink_msg_fleet_clear_target_pack(my_system_id, my_comp_id, &message, target_id);
//    tempMessageBroadcast.length = mavlink_msg_to_send_buffer(tempMessageBroadcast.contents,&message);
//    queueBroadcast.push(tempMessageBroadcast);
//    printf("%lld [SENT] FLEET_CLEAR_TARGET %d\n", (long long int)get_time_usec(), target_id);
//}

//// send FleetTargetStreamReady message
//void MavlinkPack::sendFleetTargetStreamReady(uint8_t target_id) {
//    mavlink_message_t message;
//    mavlink_msg_fleet_target_stream_ready_pack(my_system_id, my_comp_id, &message, target_id);
//    // FIXME change to unicast
//    tempMessageBroadcast.length = mavlink_msg_to_send_buffer(tempMessageBroadcast.contents,&message);
//    queueBroadcast.push(tempMessageBroadcast);
//    printf("%lld [SENT] FLEET_TARGET_STREAM_READY %d\n", (long long int)get_time_usec(), target_id);
//}

//// command Waypoint
//void MavlinkPack::cmdNavWaypoint(uint8_t targetSystem, uint8_t targetComponent, float holdTime, float proximityRadius, float passRadius, float desiredYaw, float latitude, float longitude, float altitude) {
//    CommandLong(targetSystem, targetComponent, MAV_CMD_NAV_WAYPOINT, 0, holdTime, proximityRadius, passRadius, desiredYaw, latitude, longitude, altitude);
//}

//// command SetMessageInterval
//void MavlinkPack::cmdSetMessageInterval(uint8_t targetSystem, uint8_t targetComponent, uint8_t messageID, int64_t interval_usec) {
//    CommandLong(targetSystem, targetComponent, MAV_CMD_SET_MESSAGE_INTERVAL, 0, (float)messageID, (float)interval_usec, 0, 0, 0, 0, 0);
//}

// command Land
string MavlinkManager::cmdNavLand(uint8_t targetSystem, uint8_t targetComponent, float abortAlt, float desiredYaw, float latitude, float longitude, float altitude) {
    return MavlinkManager::CommandLong(targetSystem, targetComponent, MAV_CMD_NAV_LAND, 0, abortAlt, 0, 0, desiredYaw, latitude, longitude, altitude);
}

//// command LandLocal
//void MavlinkPack::cmdNavLandLocal(uint8_t targetSystem, uint8_t targetComponent, float landingTargetNumber, float maxAcceptedOffset, float landingDescentRate, float desiredYaw, float x, float y, float z) {
//    CommandLong(targetSystem, targetComponent, MAV_CMD_NAV_LAND_LOCAL, 0, landingTargetNumber, maxAcceptedOffset, landingDescentRate, desiredYaw, y, x, z);
//}

//// command LandStart
//void MavlinkPack::cmdDoLandStart(uint8_t targetSystem, uint8_t targetComponent, float latitude, float longitude) {
//    CommandLong(targetSystem, targetComponent, MAV_CMD_DO_LAND_START, 0, 0, 0, 0, 0, latitude, longitude, 0);
//}

// command TakeOff
string MavlinkManager::cmdNavTakeoff(uint8_t targetSystem, uint8_t targetComponent, float desiredPitch, float magnetometerYaw, float latitude, float longitude, float altitude) {
    return MavlinkManager::CommandLong(targetSystem, targetComponent, MAV_CMD_NAV_TAKEOFF, 0, desiredPitch, 0, 0, magnetometerYaw, latitude, longitude, altitude);
}

// command DoSetMode
string MavlinkManager::cmdDoSetMode(uint8_t targetSystem, uint8_t targetComponent, uint8_t mavMode) {
    return MavlinkManager::CommandLong(targetSystem, targetComponent, MAV_CMD_DO_SET_MODE, 0, mavMode, 0, 0, 0, 0, 0, 0);
}

// command DoPauseContinue
string MavlinkManager::cmdDoPauseContinue(uint8_t targetSystem, uint8_t targetComponent, uint8_t pauseContinue) {
    return MavlinkManager::CommandLong(targetSystem, targetComponent, MAV_CMD_DO_PAUSE_CONTINUE, 0, pauseContinue, 0, 0, 0, 0, 0, 0);
}

//// command DoSetHome
//void MavlinkPack::cmdDoSetHome(uint8_t targetSystem, uint8_t targetComponent, uint8_t useCurrent) {
//    CommandLong(targetSystem, targetComponent, MAV_CMD_DO_SET_HOME, 0, (float)useCurrent, 0, 0, 0, 0, 0, 0);
//}

// command DoFollow
string MavlinkManager::cmdDoFollow(uint8_t targetSystem, uint8_t targetComponent, uint8_t follow_id)
{
    return MavlinkManager::CommandLong(targetSystem, targetComponent, MAV_CMD_DO_FOLLOW, 0, (float)follow_id, 0, 0, 0, 0, 0, 0);
}

//// command GetHomePosition
//void MavlinkPack::cmdGetHomePosition(uint8_t targetSystem, uint8_t targetComponent) {
//    CommandLong(targetSystem, targetComponent, MAV_CMD_GET_HOME_POSITION, 0, 0, 0, 0, 0, 0, 0, 0);
//}

// command MissionStart
string MavlinkManager::cmdMissionStart(uint8_t targetSystem, uint8_t targetComponent, uint8_t firstItem, uint8_t lastItem) {
    return MavlinkManager::CommandLong(targetSystem, targetComponent, MAV_CMD_MISSION_START, 0, (float)firstItem, (float)lastItem, 0, 0, 0, 0, 0);
}

// command DoSetParameter
string MavlinkManager::cmdDoSetParameter(uint8_t targetSystem, uint8_t targetComponent, uint8_t paramNumber, float paramValue) {
    return MavlinkManager::CommandLong(targetSystem, targetComponent, MAV_CMD_DO_SET_PARAMETER, 0, (float)paramNumber, paramValue, 0, 0, 0, 0, 0);
}

//// command RequestAutopilotCapabilities
//void MavlinkPack::cmdRequestAutopilotCapabilities(uint8_t targetSystem, uint8_t targetComponent) {
//    CommandLong(targetSystem, targetComponent, MAV_CMD_REQUEST_AUTOPILOT_CAPABILITIES, 0, 1, 0, 0, 0, 0, 0, 0);
//}

// command ReturnToLaunch
string MavlinkManager::cmdNavReturnToLaunch(uint8_t targetSystem, uint8_t targetComponent) {
    return MavlinkManager::CommandLong(targetSystem, targetComponent, MAV_CMD_NAV_RETURN_TO_LAUNCH, 0, 0, 0, 0, 0, 0, 0, 0);
}

//// command Shutdown
//void MavlinkPack::cmdRebootShutdown(uint8_t targetSystem, uint8_t targetComponent, uint8_t autopilot, uint8_t onboardComputer) {
//    // for autopilot and onboardComputer, see TMAV_REBOOT_SHUTDOWN_PARAM
//    CommandLong(targetSystem, targetComponent, MAV_CMD_PREFLIGHT_REBOOT_SHUTDOWN, 0, autopilot, onboardComputer, 0, 0, 0, 0, 0);
//}

//// command FleetUavClearToLeave
//void MavlinkPack::cmdFleetUavClearToLeave(uint8_t targetSystem, uint8_t targetComponent) {
//    CommandLong(targetSystem, targetComponent, MAV_CMD_FLEET_UAV_CLEAR_TO_LEAVE, 0, 0, 0, 0, 0, 0, 0, 0);
//}

string MavlinkManager::cmdFleetStartMission(uint8_t targetSystem, uint8_t targetComponent, uint8_t mission_id) {
    return MavlinkManager::CommandLong(targetSystem, targetComponent, MAV_CMD_FLEET_START_MISSION, 0, mission_id, 0, 0, 0, 0, 0, 0);
}

string MavlinkManager::cmdFleetEndMission(uint8_t targetSystem, uint8_t targetComponent, uint8_t mission_id) {
    return MavlinkManager::CommandLong(targetSystem, targetComponent, MAV_CMD_FLEET_END_MISSION, 0, mission_id, 0, 0, 0, 0, 0, 0);
}

//// command FleetTargetSnapshot
//void MavlinkPack::cmdFleetTargetSnapshot(uint8_t targetSystem, uint8_t targetComponent, uint8_t target_id) {
//    CommandLong(targetSystem, targetComponent, MAV_CMD_FLEET_TARGET_SNAPSHOT, 0, target_id, 0, 0, 0, 0, 0, 0);
//}

// check the received message CRC, return true if OK
bool MavlinkManager::check_mavlink_crc(u_int8_t *recv_packet, ssize_t recsize, u_int8_t msgid) {
    u_int16_t *crc_accum = new u_int16_t(X25_INIT_CRC);
    u_int16_t recv_crc;
    for (int i = 1; i < (recsize-2); ++i)
        crc_accumulate(recv_packet[i],crc_accum);
    crc_accumulate(hds_mavlink_message_crcs[msgid],crc_accum);
    recv_crc = recv_packet[recsize-1]<<8 ^ recv_packet[recsize-2];
    recv_crc -= *crc_accum;
    delete crc_accum;
    if(!recv_crc) return true;
    return false;
}

void MavlinkManager::parse_string(string str_in, ssize_t msg_size) {
    mavlink_message_t recv_msg;
    mavlink_status_t recv_msg_status;

    uint16_t char_counter = 0;
    uint16_t temp_index = 0;
    uint8_t temp_buffer[BUFF_IN_LEN];
    uint8_t recv_buff[BUFF_IN_LEN];

    std::vector<uint8_t> cstr(str_in.c_str(), str_in.c_str() + str_in.size() + 1);

    clearBuffer(temp_buffer, BUFF_IN_LEN);
    while(char_counter<msg_size) {
        // insert characters into a temp buffer
        temp_buffer[temp_index] = recv_buff[char_counter];
        if(mavlink_parse_char(MAVLINK_COMM_0, recv_buff[char_counter], &recv_msg, &recv_msg_status)) {
            //print_message(buff_in, recsize);
            // put in the received mesages vector
            recvMessages.push(recv_msg);
            //decode_mavlink(temp_buffer, temp_index+1, recv_msg);
            clearBuffer(temp_buffer, BUFF_IN_LEN);
            temp_index = 0;
        } else {
            temp_index++;
        }
        char_counter++;
    }
}

void MavlinkManager::parse_buffer(uint8_t *recv_buff, ssize_t msg_size) {
    mavlink_message_t recv_msg;
    mavlink_status_t recv_msg_status;

    uint16_t char_counter = 0;
    uint16_t temp_index = 0;
    uint8_t temp_buffer[BUFF_IN_LEN];

    clearBuffer(temp_buffer, BUFF_IN_LEN);
    while(char_counter<msg_size) {
        // insert characters into a temp buffer
        temp_buffer[temp_index] = recv_buff[char_counter];
        if(mavlink_parse_char(MAVLINK_COMM_0, recv_buff[char_counter], &recv_msg, &recv_msg_status)) {
            //print_message(buff_in, recsize);
            // put in the received mesages vector
            //recvMessages.push(recv_msg);
            decode_mavlink(temp_buffer, temp_index+1, recv_msg);
            clearBuffer(temp_buffer, BUFF_IN_LEN);
            temp_index = 0;
        } else {
            temp_index++;
        }
        char_counter++;
    }
}

void MavlinkManager::decode_mavlink(uint8_t *recv_packet, ssize_t msg_size, mavlink_message_t message) {
    // handle message ID
//    current_messages.sysid  = message.sysid;
//    current_messages.compid = message.compid;

        if(check_mavlink_crc(recv_packet, msg_size, message.msgid)) {
            switch(message.msgid) {
            case MAVLINK_MSG_ID_HEARTBEAT: {
                mavlink_heartbeat_t heartbeat_msg;
                mavlink_msg_heartbeat_decode(&message, &heartbeat_msg);
                // insert values in the neigh table
                neighUAVs[message.sysid].system_id = message.sysid;
                neighUAVs[message.sysid].type = heartbeat_msg.type;
                neighUAVs[message.sysid].autopilot = heartbeat_msg.autopilot;
                neighUAVs[message.sysid].base_mode = heartbeat_msg.base_mode;
                neighUAVs[message.sysid].custom_mode = heartbeat_msg.custom_mode;
                neighUAVs[message.sysid].system_status = heartbeat_msg.system_status;
                neighUAVs[message.sysid].last_update = get_time_usec();
                //printf("%lld [RECV %d] HEARTBEAT %d %d %d %d %d\n", (long long int)current_messages.time_stamps.heartbeat, message.sysid, current_messages.heartbeat.type, current_messages.heartbeat.autopilot, current_messages.heartbeat.base_mode, current_messages.heartbeat.custom_mode, current_messages.heartbeat.system_status);
                break;
            }
//            case MAVLINK_MSG_ID_SYS_STATUS: {
//                mavlink_msg_sys_status_decode(&message, &(current_messages.sys_status));
//                current_messages.time_stamps.sys_status = get_time_usec();
//                this_timestamps.sys_status = current_messages.time_stamps.sys_status;
//                // insert values in the neigh table
//                activeUAVs.insert(message.sysid);
//                neighUAVs[message.sysid].voltage_battery = current_messages.sys_status.voltage_battery;
//                neighUAVs[message.sysid].battery_remaining = current_messages.sys_status.battery_remaining;
//                neighUAVs[message.sysid].drop_rate_comm = current_messages.sys_status.drop_rate_comm;
//                neighUAVs[message.sysid].errors_comm = current_messages.sys_status.errors_comm;
//                neighUAVs[message.sysid].last_update = current_messages.time_stamps.sys_status;
//                printf("%lld [RECV %d] SYS_STATUS %d %d %d %d\n", (long long int)current_messages.time_stamps.sys_status, message.sysid, current_messages.sys_status.voltage_battery, current_messages.sys_status.battery_remaining, current_messages.sys_status.drop_rate_comm, current_messages.sys_status.errors_comm);
//                //USB_port->write_message(message);
//                break;
//            }
//            case MAVLINK_MSG_ID_LOCAL_POSITION_NED: {
//                mavlink_msg_local_position_ned_decode(&message, &(current_messages.local_position_ned));
//                current_messages.time_stamps.local_position_ned = get_time_usec();
//                this_timestamps.local_position_ned = current_messages.time_stamps.local_position_ned;
//                // insert values in the neigh table
//                activeUAVs.insert(message.sysid);
//                neighUAVs[message.sysid].time_boot_ms = current_messages.local_position_ned.time_boot_ms;
//                neighUAVs[message.sysid].x = current_messages.local_position_ned.x;
//                neighUAVs[message.sysid].y = current_messages.local_position_ned.y;
//                neighUAVs[message.sysid].z = current_messages.local_position_ned.z;
//                neighUAVs[message.sysid].vx = current_messages.local_position_ned.vx;
//                neighUAVs[message.sysid].vy = current_messages.local_position_ned.vy;
//                neighUAVs[message.sysid].vz = current_messages.local_position_ned.vz;
//                neighUAVs[message.sysid].last_update = current_messages.time_stamps.local_position_ned;
//                printf("%lld [RECV %d] LOCAL_POSITION_NED %.2f %.2f %.2f %.2f %.2f %.2f\n", (long long int)current_messages.time_stamps.local_position_ned, message.sysid, current_messages.local_position_ned.x, current_messages.local_position_ned.y, current_messages.local_position_ned.z, current_messages.local_position_ned.vx, current_messages.local_position_ned.vy, current_messages.local_position_ned.vz);
//                break;
//            }
//            case MAVLINK_MSG_ID_POWER_STATUS: {
//                mavlink_msg_power_status_decode(&message, &(current_messages.power_status));
//                current_messages.time_stamps.power_status = get_time_usec();
//                this_timestamps.power_status = current_messages.time_stamps.power_status;
//                recv_voltage = current_messages.power_status.Vcc;
//                recv_signal = current_messages.power_status.Vservo;
//                printf("%lld [RECV %d] POWER_STATUS %d %d\n", (long long int)current_messages.time_stamps.power_status, message.sysid, current_messages.power_status.Vcc, current_messages.power_status.Vservo);
//                //printf("MAV_PWR_RECV %.2f %.2f %.2f\n", (double)(current_messages.time_stamps.power_status-timeProgramStart)/DEFAULT_PERIOD_ANY, sin(((double)recv_signal)/SIGNAL_DIVISOR), (double)recv_voltage/100);
//                //if(!started_receiving) started_receiving_time = current_messages.time_stamps.power_status;
//                started_receiving = true;
//                break;
//            }
//            case MAVLINK_MSG_ID_MISSION_CLEAR_ALL: {
//                printf("[RECV] MAVLINK_MSG_ID_MISSION_CLEAR_ALL\n");
//                mavlink_msg_mission_clear_all_decode(&message,&(current_messages.clear_all));
//                current_messages.time_stamps.clear_all = get_time_usec();
//                this_timestamps.clear_all = current_messages.time_stamps.clear_all;
//                // clear the queue
//                clearMissionPlan();
//                MavlinkItem mavItem;
//                mavItem.sender_id = message.sysid;
//                mavItem.msg_id = message.msgid;
//                recvCommands.push(mavItem);
//                //printf("[DEBUG] sysid = %d compid = %d\n", message.sysid, message.compid);
//                sendMissionAck(message.sysid, message.compid, MAV_MISSION_ACCEPTED);
//                break;
//            }
//            case MAVLINK_MSG_ID_MISSION_ITEM_REACHED: {
//                printf("[RECV] MAVLINK_MSG_ID_MISSION_ITEM_REACHED\n");
//                mavlink_msg_mission_item_reached_decode(&message, &(current_messages.mission_reached));
//                current_messages.time_stamps.mission_reached = get_time_usec();
//                this_timestamps.mission_reached = current_messages.time_stamps.mission_reached;
//                //seqr=current_messages.mission_reached.seq;
//                currentMissionItem = current_messages.mission_reached.seq;
//                sendMissionAck(message.sysid, message.compid, MAV_MISSION_ACCEPTED);
//                break;
//            }
//            case MAVLINK_MSG_ID_MISSION_COUNT: {
//                printf("[RECV] MAVLINK_MSG_ID_MISSION_COUNT\n");
//                mavlink_msg_mission_count_decode(&message, &(current_messages.mission_count));
//                current_messages.time_stamps.mission_count = get_time_usec();
//                this_timestamps.mission_count = current_messages.time_stamps.mission_count;
//                Waycount=current_messages.mission_count.count;
//                setMissionCount(current_messages.mission_count.count);
//                compt=0;
//                Wayseq=-1;
//                break;
//            }
//            case MAVLINK_MSG_ID_MISSION_SET_CURRENT: {
//                printf("[RECV] MAVLINK_MSG_ID_MISSION_SET_CURRENT\n");
//                mavlink_msg_mission_set_current_decode(&message, &(current_messages.mission_set_current));
//                current_messages.time_stamps.mission_set_current = get_time_usec();
//                this_timestamps.mission_set_current = current_messages.time_stamps.mission_set_current;
//                MavlinkItem mavItem;
//                mavItem.msg_id = message.msgid;
//                mavItem.set_current = current_messages.mission_set_current.seq;
//                // adding commands to the mission plan
//                //missionPlan.push(mavItem);
//                recvCommands.push(mavItem);
//                break;
//            }
//            case MAVLINK_MSG_ID_MISSION_REQUEST_LIST: {
//                printf("[RECV] MAVLINK_MSG_ID_MISSION_REQUEST_LIST\n");
//                mavlink_msg_mission_request_list_decode(&message, &(current_messages.mission_request_list));
//                current_messages.time_stamps.mission_request_list = get_time_usec();
//                this_timestamps.mission_request_list = current_messages.time_stamps.mission_request_list;
//                // send the mission count
//                sendMissionCount(message.sysid, message.compid, getMissionCount());
//                break;
//            }
//            case MAVLINK_MSG_ID_MISSION_WRITE_PARTIAL_LIST: {
//                printf("[RECV] MAVLINK_MSG_ID_MISSION_WRITE_PARTIAL_LIST\n");
//                mavlink_msg_mission_write_partial_list_decode(&message, &(current_messages).mission_write_partial_list);
//                current_messages.time_stamps.mission_write_partial_list = get_time_usec();
//                this_timestamps.mission_write_partial_list = current_messages.time_stamps.mission_write_partial_list;
//                //
//                // TODO process the partial list message
//                //
//                sendMissionAck(message.sysid, message.compid, MAV_MISSION_ACCEPTED);
//                break;
//            }
//            case MAVLINK_MSG_ID_TIMESYNC: {
//                //  printf("MAVLINK_MSG_ID_TIMESYNC\n");
//                mavlink_msg_timesync_decode(&message, &(current_messages.timesync));
//                current_messages.time_stamps.timesync = get_time_usec();
//                this_timestamps.timesync = current_messages.time_stamps.timesync;
//                Xtimec=current_messages.timesync.tc1;
//                Xtimes=current_messages.timesync.ts1;
//                Xtimesync=true;
//                break;
//            }
//            case MAVLINK_MSG_ID_SYSTEM_TIME: {
//                printf("[RECV] MAVLINK_MSG_ID_SYSTEM_TIME\n");
//                mavlink_msg_system_time_decode(&message, &(current_messages.system_time));
//                current_messages.time_stamps.system_time = get_time_usec();
//                this_timestamps.system_time = current_messages.time_stamps.system_time;
//                break;
//            }
            case MAVLINK_MSG_ID_MISSION_ITEM: {
                mavlink_mission_item_t mission_item_msg;
                mavlink_msg_mission_item_decode(&message, &mission_item_msg);

                // * Mission items queue:
                // * 16   MAV_CMD_NAV_WAYPOINT            (hold time, acceptance radius, yaw angle, lat, long, altitude)
                // * 21   MAV_CMD_NAV_LAND                (abort alt, yaw angle, lat, long, altitude)
                // * 22   MAV_CMD_NAV_TAKEOFF             (yaw angle, lat, long, altitude)
                // * 177  MAV_CMD_DO_JUMP                 (sequence, repeat count)
                // * 20   MAV_CMD_NAV_RETURN_TO_LAUNCH    (empty)

                command = mission_item_msg.command;
                if(mission_item_msg.target_system == my_system_id) {
                    switch(command) {
                    case MAV_CMD_NAV_TAKEOFF: {
                        MavlinkItem mavItem;
                        mavItem.sender_id = message.sysid;
                        mavItem.msg_id = message.msgid;
                        mavItem.cmd_id = command;
                        mavItem.sequence = mission_item_msg.seq;
                        mavItem.yaw_angle = mission_item_msg.param4;
                        mavItem.latitude = mission_item_msg.x;
                        mavItem.longitude = mission_item_msg.y;
                        mavItem.altitude = mission_item_msg.z;
                        // adding commands to the mission plan
                        missionPlan.push(mavItem);
                        //missionCommands.push(mavItem);
                        //printf("[RECV] MISSION_ITEM: TAKEOFF\n");
                        break;
                    }
                    case MAV_CMD_NAV_LAND: {
                        MavlinkItem mavItem;
                        mavItem.sender_id = message.sysid;
                        mavItem.msg_id = message.msgid;
                        mavItem.cmd_id = command;
                        mavItem.sequence = mission_item_msg.seq;
                        mavItem.abort_altitude = mission_item_msg.param1;
                        mavItem.yaw_angle = mission_item_msg.param4;
                        mavItem.latitude = mission_item_msg.x;
                        mavItem.longitude = mission_item_msg.y;
                        mavItem.altitude = mission_item_msg.z;
                        // adding commands to the mission plan
                        missionPlan.push(mavItem);
                        //printf("[RECV] MISSION_ITEM: LAND\n");
                        break;
                    }
//                    case MAV_CMD_NAV_LAND_LOCAL: {
//                        MavlinkItem mavItem;
//                        mavItem.sender_id = message.sysid;
//                        mavItem.msg_id = message.msgid;
//                        mavItem.cmd_id = command;
//                        mavItem.sequence = current_messages.mission_item.seq;
//                        mavItem.landing_target_number = current_messages.mission_item.param1;
//                        mavItem.max_accepted_offset = current_messages.mission_item.param2;
//                        mavItem.descent_rate = current_messages.mission_item.param3;
//                        mavItem.desired_yaw = current_messages.mission_item.param4;
//                        mavItem.x = current_messages.mission_item.x;
//                        mavItem.y = current_messages.mission_item.y;
//                        mavItem.z = current_messages.mission_item.z;
//                        // adding commands to the mission plan
//                        missionPlan.push(mavItem);
//                        printf("[RECV] MISSION_ITEM: LAND\n");
//                        break;
//                    }
//                    case MAV_CMD_DO_JUMP: {
//                        MavlinkItem mavItem;
//                        mavItem.sender_id = message.sysid;
//                        mavItem.msg_id = message.msgid;
//                        mavItem.cmd_id = command;
//                        mavItem.sequence = current_messages.mission_item.seq;
//                        mavItem.jump_sequence = current_messages.mission_item.param1;
//                        mavItem.jump_repeat_count = current_messages.mission_item.param2;
//                        // adding commands to the mission plan
//                        missionPlan.push(mavItem);
//                        printf("[RECV] MISSION_ITEM: MAV_CMD_DO_JUMP seq %d repeat %d\n", mavItem.jump_sequence, mavItem.jump_repeat_count);
//                        break;
//                    }
//                    case MAV_CMD_NAV_RETURN_TO_LAUNCH: {
//                        MavlinkItem mavItem;
//                        mavItem.sender_id = message.sysid;
//                        mavItem.msg_id = message.msgid;
//                        mavItem.cmd_id = command;
//                        mavItem.sequence = current_messages.mission_item.seq;
//                        // adding commands to the mission plan
//                        missionPlan.push(mavItem);
//                        printf("[RECV] MISSION_ITEM: MAV_CMD_NAV_RETURN_TO_LAUNCH\n");
//                        break;
//                    }
//                    case MAV_CMD_NAV_WAYPOINT: {
//                        MavlinkItem mavItem;
//                        mavItem.sender_id = message.sysid;
//                        mavItem.msg_id = message.msgid;
//                        mavItem.cmd_id = command;
//                        mavItem.sequence = current_messages.mission_item.seq;
//                        mavItem.hold_time = current_messages.mission_item.param1;
//                        mavItem.yaw_angle = current_messages.mission_item.param4;
//                        mavItem.desired_yaw = current_messages.mission_item.param4;
//                        mavItem.latitude = current_messages.mission_item.x;
//                        mavItem.longitude = current_messages.mission_item.y;
//                        mavItem.altitude = current_messages.mission_item.z;
//                        // adding commands to the mission plan
//                        missionPlan.push(mavItem);
//                        printf("[RECV] MISSION_ITEM: WAYPOINT seq = %d, lat = %.2f, long = %.2f, alt = %.2f\n", mavItem.sequence, mavItem.latitude, mavItem.longitude, mavItem.altitude);
//                        break;
//                    }
                    }
//                    // respond with ACK immediately
//                    sendMissionAck(message.sysid, message.compid, MAV_MISSION_ACCEPTED);
                }
                break;
            }
//            case MAVLINK_MSG_ID_MISSION_ACK: {
//                mavlink_msg_mission_ack_decode(&message, &(current_messages.mission_ack));
//                // check if it is the right mission ack
//                current_messages.time_stamps.mission_ack = get_time_usec();
//                this_timestamps.mission_ack = current_messages.time_stamps.mission_ack;
//                setMissionAck();
//                //typeack=current_messages.mission_ack.type;
//                printf("%lld [RECV %d] MAVLINK_MSG_ID_MISSION_ACK\n", (long long int)current_messages.time_stamps.mission_ack, message.sysid);
//                break;
//            }
//            case MAVLINK_MSG_ID_COMMAND_ACK: {
//                printf("[RECV] MAVLINK_MSG_ID_COMMAND_ACK\n");
//                mavlink_msg_command_ack_decode(&message, &(current_messages.command_ack));
//                // check if it is the right command ack
//                current_messages.time_stamps.command_ack = get_time_usec();
//                this_timestamps.command_ack = current_messages.time_stamps.command_ack;
//                setCommandAck();
//                printf("%lld [RECV %d] MAVLINK_MSG_ID_COMMAND_ACK\n", (long long int)current_messages.time_stamps.command_ack, message.sysid);
//                break;
//            }
//            case MAVLINK_MSG_ID_MISSION_REQUEST: {
//                //    printf("MAVLINK_MSG_ID_MISSION_REQUEST\n");
//                mavlink_msg_mission_request_decode(&message, &(current_messages.mission_request));
//                seq=current_messages.mission_request.seq;
//                current_messages.time_stamps.mission_request = get_time_usec();
//                this_timestamps.mission_request = current_messages.time_stamps.mission_request;
//                request=true;
//                break;
//            }
//            case MAVLINK_MSG_ID_HOME_POSITION: {
//                printf("[RECV] MAVLINK_MSG_ID_HOME_POSITION\n");
//                mavlink_msg_home_position_decode(&message, &(current_messages.home_position));
//                current_messages.time_stamps.home_position = get_time_usec();
//                this_timestamps.home_position = current_messages.time_stamps.home_position;
//                home_position_set=true;
//                break;
//            }

//            case MAVLINK_MSG_ID_BATTERY_STATUS: {
//                //printf("MAVLINK_MSG_ID_BATTERY_STATUS\n");
//                mavlink_msg_battery_status_decode(&message, &(current_messages.battery_status));
//                current_messages.time_stamps.battery_status = get_time_usec();
//                this_timestamps.battery_status = current_messages.time_stamps.battery_status;
//                //USB_port->write_message(message);
//                break;
//            }

//            case MAVLINK_MSG_ID_RADIO_STATUS: {
//                //printf("MAVLINK_MSG_ID_RADIO_STATUS\n");
//                mavlink_msg_radio_status_decode(&message, &(current_messages.radio_status));
//                current_messages.time_stamps.radio_status = get_time_usec();
//                this_timestamps.radio_status = current_messages.time_stamps.radio_status;
//                break;
//            }

//            case MAVLINK_MSG_ID_GLOBAL_POSITION_INT: {
//                printf("[RECV] MAVLINK_MSG_ID_GLOBAL_POSITION_INT\n");
//                mavlink_msg_global_position_int_decode(&message, &(current_messages.global_position_int));
//                current_messages.time_stamps.global_position_int = get_time_usec();
//                this_timestamps.global_position_int = current_messages.time_stamps.global_position_int;
//                // insert it into the command queue
//                MavlinkItem mavItem;
//                mavItem.sender_id = message.sysid;
//                mavItem.msg_id = message.msgid;
//                mavItem.time_boot_ms = current_messages.global_position_int.time_boot_ms;
//                mavItem.latitude = current_messages.global_position_int.lat;
//                mavItem.longitude = current_messages.global_position_int.lon;
//                mavItem.altitude = current_messages.global_position_int.alt;
//                mavItem.relative_alt = current_messages.global_position_int.relative_alt;
//                mavItem.vx = current_messages.global_position_int.vx;
//                mavItem.vy = current_messages.global_position_int.vy;
//                mavItem.vz = current_messages.global_position_int.vz;
//                mavItem.yaw_angle = current_messages.global_position_int.hdg;
//                recvCommands.push(mavItem);
//                break;
//            }

//            case MAVLINK_MSG_ID_SET_POSITION_TARGET_LOCAL_NED: {
//                mavlink_msg_set_position_target_local_ned_decode(&message, &(current_messages.set_position_target_local_ned));
//                current_messages.time_stamps.set_position_target_local_ned = get_time_usec();
//                this_timestamps.set_position_target_local_ned = current_messages.time_stamps.set_position_target_local_ned;
//                // insert it into the command queue
//                MavlinkItem mavItem;
//                mavItem.sender_id = message.sysid;
//                mavItem.msg_id = message.msgid;
//                mavItem.time_boot_ms = current_messages.set_position_target_local_ned.time_boot_ms;
//                mavItem.target_system = current_messages.set_position_target_local_ned.target_system;
//                mavItem.target_component = current_messages.set_position_target_local_ned.target_component;
//                mavItem.coordinate_frame = current_messages.set_position_target_local_ned.coordinate_frame;
//                mavItem.type_mask = current_messages.set_position_target_local_ned.type_mask;
//                mavItem.x = current_messages.set_position_target_local_ned.x;
//                mavItem.y = current_messages.set_position_target_local_ned.y;
//                mavItem.z = current_messages.set_position_target_local_ned.z;
//                mavItem.vx = current_messages.set_position_target_local_ned.vx;
//                mavItem.vy = current_messages.set_position_target_local_ned.vy;
//                mavItem.vz = current_messages.set_position_target_local_ned.vz;
//                mavItem.afx = current_messages.set_position_target_local_ned.afx;
//                mavItem.afy = current_messages.set_position_target_local_ned.afy;
//                mavItem.afz = current_messages.set_position_target_local_ned.afz;
//                mavItem.yaw = current_messages.set_position_target_local_ned.yaw;
//                mavItem.yaw_rate = current_messages.set_position_target_local_ned.yaw_rate;
//                recvCommands.push(mavItem);
//                printf("%lld [RECV %d] MAVLINK_MSG_ID_SET_POSITION_TARGET_LOCAL_NED x = %.2f y = %.2f z = %.2f\n", (long long int)current_messages.time_stamps.set_position_target_local_ned, message.sysid, mavItem.x, mavItem.y, mavItem.z);
//                break;
//            }

//            case MAVLINK_MSG_ID_POSITION_TARGET_LOCAL_NED: {
//                printf("[RECV] MAVLINK_MSG_ID_POSITION_TARGET_LOCAL_NED\n");
//                mavlink_msg_position_target_local_ned_decode(&message, &(current_messages.position_target_local_ned));
//                current_messages.time_stamps.position_target_local_ned = get_time_usec();
//                this_timestamps.position_target_local_ned = current_messages.time_stamps.position_target_local_ned;
//                // insert it into the command queue
//                MavlinkItem mavItem;
//                mavItem.sender_id = message.sysid;
//                mavItem.msg_id = message.msgid;
//                mavItem.time_boot_ms = current_messages.position_target_local_ned.time_boot_ms;
//                mavItem.coordinate_frame = current_messages.position_target_local_ned.coordinate_frame;
//                mavItem.type_mask = current_messages.position_target_local_ned.type_mask;
//                mavItem.x = current_messages.position_target_local_ned.x;
//                mavItem.y = current_messages.position_target_local_ned.y;
//                mavItem.z = current_messages.position_target_local_ned.z;
//                mavItem.vx = current_messages.position_target_local_ned.vx;
//                mavItem.vy = current_messages.position_target_local_ned.vy;
//                mavItem.vz = current_messages.position_target_local_ned.vz;
//                mavItem.afx = current_messages.position_target_local_ned.afx;
//                mavItem.afy = current_messages.position_target_local_ned.afy;
//                mavItem.afz = current_messages.position_target_local_ned.afz;
//                mavItem.yaw = current_messages.position_target_local_ned.yaw;
//                mavItem.yaw_rate = current_messages.position_target_local_ned.yaw_rate;
//                recvCommands.push(mavItem);
//                break;
//            }

//            case MAVLINK_MSG_ID_SET_POSITION_TARGET_GLOBAL_INT: {
//                printf("[RECV] MAVLINK_MSG_ID_SET_POSITION_TARGET_GLOBAL_INT\n");
//                mavlink_msg_set_position_target_global_int_decode(&message, &(current_messages.set_position_target_global_int));
//                current_messages.time_stamps.set_position_target_global_int = get_time_usec();
//                this_timestamps.set_position_target_global_int = current_messages.time_stamps.set_position_target_global_int;
//                // insert it into the command queue
//                MavlinkItem mavItem;
//                mavItem.sender_id = message.sysid;
//                mavItem.msg_id = message.msgid;
//                mavItem.time_boot_ms = current_messages.set_position_target_global_int.time_boot_ms;
//                mavItem.target_system = current_messages.set_position_target_global_int.target_system;
//                mavItem.target_component = current_messages.set_position_target_global_int.target_component;
//                mavItem.coordinate_frame = current_messages.set_position_target_global_int.coordinate_frame;
//                mavItem.type_mask = current_messages.set_position_target_global_int.type_mask;
//                mavItem.lat_int = current_messages.set_position_target_global_int.lat_int;
//                mavItem.lon_int = current_messages.set_position_target_global_int.lon_int;
//                mavItem.altitude = current_messages.set_position_target_global_int.alt;
//                mavItem.vx = current_messages.set_position_target_global_int.vx;
//                mavItem.vy = current_messages.set_position_target_global_int.vy;
//                mavItem.vz = current_messages.set_position_target_global_int.vz;
//                mavItem.afx = current_messages.set_position_target_global_int.afx;
//                mavItem.afy = current_messages.set_position_target_global_int.afy;
//                mavItem.afz = current_messages.set_position_target_global_int.afz;
//                mavItem.yaw = current_messages.set_position_target_global_int.yaw;
//                mavItem.yaw_rate = current_messages.set_position_target_global_int.yaw_rate;
//                recvCommands.push(mavItem);
//                break;
//            }

//            case MAVLINK_MSG_ID_POSITION_TARGET_GLOBAL_INT: {
//                printf("[RECV] MAVLINK_MSG_ID_POSITION_TARGET_GLOBAL_INT\n");
//                mavlink_msg_position_target_global_int_decode(&message, &(current_messages.position_target_global_int));
//                current_messages.time_stamps.position_target_global_int = get_time_usec();
//                this_timestamps.position_target_global_int = current_messages.time_stamps.position_target_global_int;
//                // insert it into the command queue
//                MavlinkItem mavItem;
//                mavItem.sender_id = message.sysid;
//                mavItem.msg_id = message.msgid;
//                mavItem.time_boot_ms = current_messages.position_target_global_int.time_boot_ms;
//                mavItem.coordinate_frame = current_messages.position_target_global_int.coordinate_frame;
//                mavItem.type_mask = current_messages.position_target_global_int.type_mask;
//                mavItem.lat_int = current_messages.position_target_global_int.lat_int;
//                mavItem.lon_int = current_messages.position_target_global_int.lon_int;
//                mavItem.altitude = current_messages.position_target_global_int.alt;
//                mavItem.vx = current_messages.position_target_global_int.vx;
//                mavItem.vy = current_messages.position_target_global_int.vy;
//                mavItem.vz = current_messages.position_target_global_int.vz;
//                mavItem.afx = current_messages.position_target_global_int.afx;
//                mavItem.afy = current_messages.position_target_global_int.afy;
//                mavItem.afz = current_messages.position_target_global_int.afz;
//                mavItem.yaw = current_messages.position_target_global_int.yaw;
//                mavItem.yaw_rate = current_messages.position_target_global_int.yaw_rate;
//                recvCommands.push(mavItem);
//                break;
//            }
//            case MAVLINK_MSG_ID_HIGHRES_IMU: {
//                //printf("[RECV] MAVLINK_MSG_ID_HIGHRES_IMU\n");
//                mavlink_msg_highres_imu_decode(&message, &(current_messages.highres_imu));
//                current_messages.time_stamps.highres_imu = get_time_usec();
//                this_timestamps.highres_imu = current_messages.time_stamps.highres_imu;
//                break;
//            }

//            case MAVLINK_MSG_ID_ATTITUDE: {
//                //printf("MAVLINK_MSG_ID_ATTITUDE\n");
//                mavlink_msg_attitude_decode(&message, &(current_messages.attitude));
//                current_messages.time_stamps.attitude = get_time_usec();
//                this_timestamps.attitude = current_messages.time_stamps.attitude;
//                break;
//            }

//            case MAVLINK_MSG_ID_FLEET_UAV_INFO_LOCAL: {
//                mavlink_msg_fleet_uav_info_local_decode(&message, &(current_messages.fleet_uav_info_local));
//                current_messages.time_stamps.fleet_uav_info_local = get_time_usec();
//                this_timestamps.fleet_uav_info_local = current_messages.time_stamps.fleet_uav_info_local;
//                // TODO
//                printf("%lld [RECV %d] FLEET_UAV_INFO_LOCAL *** TODO ***\n", (long long int)current_messages.time_stamps.fleet_uav_info_local, message.sysid);
//                break;
//            }
//            case MAVLINK_MSG_ID_FLEET_UAV_INFO_GLOBAL: {
//                mavlink_msg_fleet_uav_info_global_decode(&message, &(current_messages.fleet_uav_info_global));
//                current_messages.time_stamps.fleet_uav_info_global = get_time_usec();
//                this_timestamps.fleet_uav_info_global = current_messages.time_stamps.fleet_uav_info_global;
//                // TODO
//                printf("%lld [RECV %d] FLEET_UAV_INFO_GLOBAL *** TODO ***\n", (long long int)current_messages.time_stamps.fleet_uav_info_global, message.sysid);
//                break;
//            }
//            case MAVLINK_MSG_ID_FLEET_UAV_REQUEST_LOCAL: {
//                mavlink_msg_fleet_uav_request_local_decode(&message, &(current_messages.fleet_uav_request_local));
//                current_messages.time_stamps.fleet_uav_request_local = get_time_usec();
//                this_timestamps.fleet_uav_request_local = current_messages.time_stamps.fleet_uav_request_local;
//                MavlinkItem mavItem;
//                mavItem.sender_id = message.sysid;
//                mavItem.msg_id = message.msgid;
//                mavItem.request_type = current_messages.fleet_uav_request_local.request_type;
//                mavItem.request_id = current_messages.fleet_uav_request_local.request_ID;
//                mavItem.id_to_replace = current_messages.fleet_uav_request_local.ID_to_replace;
//                mavItem.x = current_messages.fleet_uav_request_local.x;
//                mavItem.y = current_messages.fleet_uav_request_local.y;
//                mavItem.z = current_messages.fleet_uav_request_local.z;
//                mavItem.request_urgency = current_messages.fleet_uav_request_local.request_urgency;
//                recvCommands.push(mavItem);
//                printf("%lld [RECV %d] FLEET_UAV_REQUEST_LOCAL %d %d %d %.2f %.2f %.2f %d\n", (long long int)current_messages.time_stamps.fleet_uav_request_local, message.sysid, current_messages.fleet_uav_request_local.request_ID, current_messages.fleet_uav_request_local.request_type, current_messages.fleet_uav_request_local.ID_to_replace, current_messages.fleet_uav_request_local.x, current_messages.fleet_uav_request_local.y, current_messages.fleet_uav_request_local.z, current_messages.fleet_uav_request_local.request_urgency);
//                break;
//            }
//            case MAVLINK_MSG_ID_FLEET_UAV_REQUEST_GLOBAL: {
//                mavlink_msg_fleet_uav_request_global_decode(&message, &(current_messages.fleet_uav_request_global));
//                current_messages.time_stamps.fleet_uav_request_global = get_time_usec();
//                this_timestamps.fleet_uav_request_global = current_messages.time_stamps.fleet_uav_request_global;
//                MavlinkItem mavItem;
//                mavItem.sender_id = message.sysid;
//                mavItem.msg_id = message.msgid;
//                mavItem.request_type = current_messages.fleet_uav_request_global.request_type;
//                mavItem.request_id = current_messages.fleet_uav_request_global.request_ID;
//                mavItem.id_to_replace = current_messages.fleet_uav_request_global.ID_to_replace;
//                mavItem.latitude = current_messages.fleet_uav_request_global.latitude;
//                mavItem.longitude = current_messages.fleet_uav_request_global.longitude;
//                mavItem.altitude = current_messages.fleet_uav_request_global.altitude;
//                mavItem.request_urgency = current_messages.fleet_uav_request_global.request_urgency;
//                recvCommands.push(mavItem);
//                printf("%lld [RECV %d] FLEET_UAV_REQUEST_GLOBAL %d %d %d %d %d %d %d\n", (long long int)current_messages.time_stamps.fleet_uav_request_global, message.sysid, current_messages.fleet_uav_request_global.request_ID, current_messages.fleet_uav_request_global.request_type, current_messages.fleet_uav_request_global.ID_to_replace, current_messages.fleet_uav_request_global.latitude, current_messages.fleet_uav_request_global.longitude, current_messages.fleet_uav_request_global.altitude, current_messages.fleet_uav_request_global.request_urgency);
//                break;
//            }
//            case MAVLINK_MSG_ID_FLEET_UAV_REQUEST_RESPONSE: {
//                mavlink_msg_fleet_uav_request_response_decode(&message, &(current_messages.fleet_uav_request_response));
//                current_messages.time_stamps.fleet_uav_request_response = get_time_usec();
//                this_timestamps.fleet_uav_request_response = current_messages.time_stamps.fleet_uav_request_response;
//                MavlinkItem mavItem;
//                mavItem.sender_id = message.sysid;
//                mavItem.msg_id = message.msgid;
//                mavItem.request_id = current_messages.fleet_uav_request_response.request_ID;
//                mavItem.arrival_time = current_messages.fleet_uav_request_response.arrival_time;
//                recvCommands.push(mavItem);
//                ReqResp reqResp;
//                reqResp.arrival_time = current_messages.fleet_uav_request_response.arrival_time;
//                reqResp.request_id = current_messages.fleet_uav_request_response.request_ID;
//                reqResp.system_id = message.sysid;
//                reqResponses.push_back(reqResp);
//                printf("%lld [RECV %d] FLEET_UAV_REQUEST_RESPONSE %d %lld \n", (long long int)current_messages.time_stamps.fleet_uav_request_response, message.sysid, current_messages.fleet_uav_request_response.request_ID, (long long int)current_messages.fleet_uav_request_response.arrival_time);
//                break;
//            }
//            case MAVLINK_MSG_ID_FLEET_UAV_ELECTED_ID: {
//                mavlink_msg_fleet_uav_elected_id_decode(&message, &(current_messages.fleet_uav_elected_id));
//                current_messages.time_stamps.fleet_uav_elected_id = get_time_usec();
//                this_timestamps.fleet_uav_elected_id = current_messages.time_stamps.fleet_uav_elected_id;
//                MavlinkItem mavItem;
//                mavItem.sender_id = message.sysid;
//                mavItem.msg_id = message.msgid;
//                mavItem.request_id = current_messages.fleet_uav_elected_id.request_ID;
//                mavItem.elected_id = current_messages.fleet_uav_elected_id.elected_ID;
//                recvCommands.push(mavItem);
//                printf("%lld [RECV %d] FLEET_UAV_ELECTED_ID %d %d\n", (long long int)current_messages.time_stamps.fleet_uav_elected_id, message.sysid, current_messages.fleet_uav_elected_id.request_ID, current_messages.fleet_uav_elected_id.elected_ID);
//                break;
//            }
//            case MAVLINK_MSG_ID_FLEET_UAV_ELECTED_ACCEPT: {
//                mavlink_msg_fleet_uav_elected_accept_decode(&message, &(current_messages.fleet_uav_elected_accept));
//                current_messages.time_stamps.fleet_uav_elected_accept = get_time_usec();
//                this_timestamps.fleet_uav_elected_accept = current_messages.time_stamps.fleet_uav_elected_accept;
//                MavlinkItem mavItem;
//                mavItem.sender_id = message.sysid;
//                mavItem.msg_id = message.msgid;
//                mavItem.request_id = current_messages.fleet_uav_elected_accept.request_ID;
//                recvCommands.push(mavItem);
//                req_accepted = true;
//                printf("%lld [RECV %d] FLEET_UAV_ELECTED_ACCEPT %d\n", (long long int)current_messages.time_stamps.fleet_uav_elected_accept, message.sysid, current_messages.fleet_uav_elected_accept.request_ID);
//                break;
//            }
//            case MAVLINK_MSG_ID_FLEET_REPLACEMENT_IN_POSITION: {
//                mavlink_msg_fleet_replacement_in_position_decode(&message, &(current_messages.fleet_replacement_in_position));
//                current_messages.time_stamps.fleet_replacement_in_position = get_time_usec();
//                this_timestamps.fleet_replacement_in_position = current_messages.time_stamps.fleet_replacement_in_position;
//                MavlinkItem mavItem;
//                mavItem.sender_id = message.sysid;
//                mavItem.msg_id = message.msgid;
//                mavItem.request_id = current_messages.fleet_replacement_in_position.request_ID;
//                recvCommands.push(mavItem);
//                replacement_in_position = true;
//                printf("%lld [RECV %d] FLEET_REPLACEMENT_IN_POSITION %d\n", (long long int)current_messages.time_stamps.fleet_replacement_in_position, message.sysid, current_messages.fleet_replacement_in_position.request_ID);
//                break;
//            }
//            case MAVLINK_MSG_ID_FLEET_SET_FORMATION: {
//                mavlink_msg_fleet_set_formation_decode(&message, &(current_messages.fleet_set_formation));
//                current_messages.time_stamps.fleet_set_formation = get_time_usec();
//                this_timestamps.fleet_set_formation = current_messages.time_stamps.fleet_set_formation;
//                MavlinkItem mavItem;
//                mavItem.sender_id = message.sysid;
//                mavItem.msg_id = message.msgid;
//                mavItem.formation_type = current_messages.fleet_set_formation.formation_type;
//                mavItem.leader_id = current_messages.fleet_set_formation.leader_ID;
//                mavItem.barycenter_lat = current_messages.fleet_set_formation.barycenter_lat;
//                mavItem.barycenter_long = current_messages.fleet_set_formation.barycenter_long;
//                mavItem.barycenter_alt = current_messages.fleet_set_formation.barycenter_alt;
//                mavItem.barycenter_relative_alt = current_messages.fleet_set_formation.barycenter_relative_alt;
//                mavItem.width_bound = current_messages.fleet_set_formation.width_bound;
//                mavItem.length_bound = current_messages.fleet_set_formation.length_bound;
//                mavItem.height_bound = current_messages.fleet_set_formation.height_bound;
//                recvCommands.push(mavItem);
//                printf("%lld [RECV %d] FLEET_SET_FORMATION %d %d %d %d %d %d %.2f %.2f %.2f\n", (long long int)current_messages.time_stamps.fleet_set_formation, message.sysid, mavItem.formation_type, mavItem.leader_id, mavItem.barycenter_lat, mavItem.barycenter_long, mavItem.barycenter_alt, mavItem.barycenter_relative_alt, mavItem.width_bound, mavItem.length_bound, mavItem.height_bound);
//                break;
//            }
//            case MAVLINK_MSG_ID_FLEET_SET_TARGET_LOCAL: {
//                mavlink_msg_fleet_set_target_local_decode(&message, &(current_messages.fleet_set_target_local));
//                current_messages.time_stamps.fleet_set_target_local = get_time_usec();
//                this_timestamps.fleet_set_target_local = current_messages.time_stamps.fleet_set_target_local;
//                MavlinkItem mavItem;
//                mavItem.sender_id = message.sysid;
//                mavItem.msg_id = message.msgid;
//                mavItem.target_id = current_messages.fleet_set_target_local.target_ID;
//                mavItem.target_type = current_messages.fleet_set_target_local.target_type;
//                mavItem.x = current_messages.fleet_set_target_local.x;
//                mavItem.y = current_messages.fleet_set_target_local.y;
//                mavItem.z = current_messages.fleet_set_target_local.z;
//                mavItem.heading = current_messages.fleet_set_target_local.heading;
//                mavItem.add_replace = current_messages.fleet_set_target_local.add_replace;
//                mavItem.drone_count = current_messages.fleet_set_target_local.drone_count;
//                mavItem.perimeter = current_messages.fleet_set_target_local.perimeter;
//                recvCommands.push(mavItem);
//                printf("%lld [RECV %d] FLEET_SET_TARGET_LOCAL %d %d %.2f %.2f %.2f %d %d %d %.2f\n", (long long int)current_messages.time_stamps.fleet_set_target_local, message.sysid, mavItem.target_id, mavItem.target_type, mavItem.x, mavItem.y, mavItem.z, mavItem.heading, mavItem.add_replace, mavItem.drone_count, mavItem.perimeter);
//                break;
//            }
//            case MAVLINK_MSG_ID_FLEET_SET_TARGET: {
//                mavlink_msg_fleet_set_target_decode(&message, &(current_messages.fleet_set_target));
//                current_messages.time_stamps.fleet_set_target = get_time_usec();
//                this_timestamps.fleet_set_target = current_messages.time_stamps.fleet_set_target;
//                MavlinkItem mavItem;
//                mavItem.sender_id = message.sysid;
//                mavItem.msg_id = message.msgid;
//                mavItem.target_id = current_messages.fleet_set_target.target_ID;
//                mavItem.target_type = current_messages.fleet_set_target.target_type;
//                mavItem.latitude = current_messages.fleet_set_target.latitude;
//                mavItem.longitude = current_messages.fleet_set_target.longitude;
//                mavItem.altitude = current_messages.fleet_set_target.altitude;
//                mavItem.relative_alt = current_messages.fleet_set_target.relative_alt;
//                mavItem.heading = current_messages.fleet_set_target.heading;
//                mavItem.add_replace = current_messages.fleet_set_target.add_replace;
//                mavItem.drone_count = current_messages.fleet_set_target.drone_count;
//                mavItem.perimeter = current_messages.fleet_set_target.perimeter;
//                recvCommands.push(mavItem);
//                printf("%lld [RECV %d] FLEET_SET_TARGET %d %d %d %d %d %d %d %d %d %.2f\n", (long long int)current_messages.time_stamps.fleet_set_target, message.sysid, mavItem.target_id, mavItem.target_type, mavItem.latitude, mavItem.longitude, mavItem.altitude, mavItem.relative_alt, mavItem.heading, mavItem.add_replace, mavItem.drone_count, mavItem.perimeter);
//                break;
//            }
//            case MAVLINK_MSG_ID_FLEET_CLEAR_TARGET: {
//                mavlink_msg_fleet_clear_target_decode(&message, &(current_messages.fleet_clear_target));
//                current_messages.time_stamps.fleet_clear_target = get_time_usec();
//                this_timestamps.fleet_clear_target = current_messages.time_stamps.fleet_clear_target;
//                MavlinkItem mavItem;
//                mavItem.sender_id = message.sysid;
//                mavItem.msg_id = message.msgid;
//                mavItem.target_id = current_messages.fleet_clear_target.target_ID;
//                recvCommands.push(mavItem);
//                printf("%lld [RECV %d] FLEET_CLEAR_TARGET %d\n", (long long int)current_messages.time_stamps.fleet_clear_target, message.sysid, mavItem.target_id);
//                break;
//            }
//            case MAVLINK_MSG_ID_FLEET_TARGET_STREAM_READY: {
//                mavlink_msg_fleet_target_stream_ready_decode(&message, &(current_messages.fleet_target_stream_ready));
//                current_messages.time_stamps.fleet_target_stream_ready = get_time_usec();
//                this_timestamps.fleet_target_stream_ready = current_messages.time_stamps.fleet_target_stream_ready;
//                MavlinkItem mavItem;
//                mavItem.sender_id = message.sysid;
//                mavItem.msg_id = message.msgid;
//                mavItem.target_id = current_messages.fleet_target_stream_ready.target_ID;
//                recvCommands.push(mavItem);
//                printf("%lld [RECV %d] FLEET_TARGET_STREAM_READY %d\n", (long long int)current_messages.time_stamps.fleet_target_stream_ready, message.sysid, mavItem.target_id);
//                break;
//            }

            case MAVLINK_MSG_ID_COMMAND_LONG: {
                //printf("[RECV] MAVLINK_MSG_ID_COMMAND_LONG\n");
                mavlink_command_long_t command_long_msg;
                mavlink_msg_command_long_decode(&message, &command_long_msg);
                if((command_long_msg.target_system == my_system_id)||(command_long_msg.target_system == 255)) {
                    // process the received command
                    //command_ack_result = MAV_RESULT_TEMPORARILY_REJECTED;
                    //printf("%lld [RECV %d] ", (long long int)current_messages.time_stamps.command_long, message.sysid);
                    switch(command_long_msg.command) {
//                    case MAV_CMD_SET_MESSAGE_INTERVAL: {
//                        int64_t desired_interval = (int64_t)current_messages.command_long.param2;
//                        printf("[RECV] COMMAND_LONG: MAV_CMD_SET_MESSAGE_INTERVAL %d %lld\n", (int)current_messages.command_long.param1, (long long int)current_messages.command_long.param2);
//                        // adding to the command queue
//                        MavlinkItem mavItem;
//                        mavItem.msg_id = message.msgid;
//                        mavItem.sender_id = message.sysid;
//                        mavItem.cmd_id = current_messages.command_long.command;
//                        mavItem.callback_message = (uint16_t)current_messages.command_long.param1;
//                        //(desired_interval == -1)?(mavItem.callback_flag = false):(mavItem.callback_flag = true);
//                        if(desired_interval == -1) {
//                            // stop the callback
//                            mavItem.callback_flag = false;
//                        } else if(desired_interval == 0) {
//                            // start with default interval
//                            mavItem.callback_flag = true;
//                            mavItem.callback_period = DEFAULT_PERIOD_ANY;
//                        } else {
//                            // start with the provided interval
//                            mavItem.callback_flag = true;
//                            mavItem.callback_period = desired_interval*1000;
//                        }
//                        // finally add the command to the queue
//                        recvCommands.push(mavItem);
//                        command_ack_result = MAV_RESULT_ACCEPTED;
//                        break;
//                    }
                        // * Commands queue:
                        // * 176  MAV_CMD_DO_SET_MODE             (mode MAV_MODE)
                        // * 179  MAV_CMD_DO_SET_HOME             (use current, lat, long, altitude)
                        // * 193  MAV_CMD_DO_PAUSE_CONTINUE       (holdContinue: 0=hold, 1=continue)
                        // * 300  MAV_CMD_MISSION_START           (first item, last item)
                        // * 410  MAV_CMD_GET_HOME_POS            (empty)
                        // * 20   MAV_CMD_NAV_RETURN_TO_LAUNCH    (empty)

                    case MAV_CMD_MISSION_START: {
                        MavlinkItem mavItem;
                        mavItem.sender_id = message.sysid;
                        mavItem.msg_id = message.msgid;
                        mavItem.cmd_id = command_long_msg.command;
                        mavItem.first_item = (uint16_t)command_long_msg.param1;
                        mavItem.last_item = (uint16_t)command_long_msg.param2;
                        recvCommands.push(mavItem);
                        //printf("[RECV] COMMAND_LONG: MAV_CMD_MISSION_START firstItem = %d, lastItem = %d\n", mavItem.first_item, mavItem.last_item);
                        //missionStarted();
                        //command_ack_result = MAV_RESULT_ACCEPTED;
                        break;
                    }
//                    case MAV_CMD_DO_SET_HOME: {
//                        MavlinkItem mavItem;
//                        mavItem.sender_id = message.sysid;
//                        mavItem.msg_id = message.msgid;
//                        mavItem.cmd_id = current_messages.command_long.command;
//                        mavItem.use_current = (uint8_t)current_messages.command_long.param1;
//                        mavItem.latitude = current_messages.command_long.param5;
//                        mavItem.longitude = current_messages.command_long.param6;
//                        mavItem.altitude = current_messages.command_long.param7;
//                        recvCommands.push(mavItem);
//                        printf("[RECV] COMMAND_LONG: MAV_CMD_DO_SET_HOME use_current = %d, lat = %.2f, long = %.2f, altitude = %.2f\n", mavItem.use_current, mavItem.latitude, mavItem.longitude, mavItem.altitude);
//                        command_ack_result = MAV_RESULT_ACCEPTED;
//                        break;
//                    }
//                    case MAV_CMD_GET_HOME_POSITION: {
//                        MavlinkItem mavItem;
//                        mavItem.sender_id = message.sysid;
//                        mavItem.msg_id = message.msgid;
//                        mavItem.cmd_id = current_messages.command_long.command;
//                        recvCommands.push(mavItem);
//                        // TODO respond with a home position
//                        printf("[RECV] COMMAND_LONG: MAV_CMD_GET_HOME_POSITION\n");
//                        command_ack_result = MAV_RESULT_ACCEPTED;
//                        break;
//                    }
//                    case MAV_CMD_DO_SET_MODE: {
//                        MavlinkItem mavItem;
//                        mavItem.sender_id = message.sysid;
//                        mavItem.msg_id = message.msgid;
//                        mavItem.cmd_id = current_messages.command_long.command;
//                        mavItem.mode = current_messages.command_long.param1;
//                        recvCommands.push(mavItem);
//                        printf("[RECV] COMMAND_LONG: MAV_CMD_DO_SET_MODE %d\n", mavItem.mode);
//                        command_ack_result = MAV_RESULT_ACCEPTED;
//                        break;
//                    }
//                    case MAV_CMD_NAV_GUIDED_ENABLE: {
//                        MavlinkItem mavItem;
//                        mavItem.sender_id = message.sysid;
//                        mavItem.msg_id = message.msgid;
//                        mavItem.cmd_id = current_messages.command_long.command;
//                        if(current_messages.command_long.param1>0.5) {
//                            mavItem.guided_enable = 1;
//                        } else {
//                            mavItem.guided_enable = 0;
//                        }
//                        recvCommands.push(mavItem);
//                        printf("[RECV] COMMAND_LONG: MAV_CMD_NAV_GUIDED_ENABLE\n");
//                        command_ack_result = MAV_RESULT_ACCEPTED;
//                        break;
//                    }
//                    case MAV_CMD_COMPONENT_ARM_DISARM: {
//                        MavlinkItem mavItem;
//                        mavItem.sender_id = message.sysid;
//                        mavItem.msg_id = message.msgid;
//                        mavItem.cmd_id = current_messages.command_long.command;
//                        if(current_messages.command_long.param1==1) {
//                            mavItem.armed = 1;
//                        } else {
//                            mavItem.armed = 0;
//                        }
//                        recvCommands.push(mavItem);
//                        printf("[RECV] COMMAND_LONG: MAV_CMD_COMPONENT_ARM_DISARM\n");
//                        command_ack_result = MAV_RESULT_ACCEPTED;
//                        break;
//                    }
//                    case MAV_CMD_NAV_RETURN_TO_LAUNCH: {
//                        MavlinkItem mavItem;
//                        mavItem.sender_id = message.sysid;
//                        mavItem.msg_id = message.msgid;
//                        mavItem.cmd_id = current_messages.command_long.command;
//                        recvCommands.push(mavItem);
//                        printf("[RECV] COMMAND_LONG: MAV_CMD_NAV_RETURN_TO_LAUNCH\n");
//                        command_ack_result = MAV_RESULT_ACCEPTED;
//                        break;
//                    }
                    case MAV_CMD_NAV_TAKEOFF: {
                        MavlinkItem mavItem;
                        mavItem.sender_id = message.sysid;
                        mavItem.msg_id = message.msgid;
                        mavItem.cmd_id = command_long_msg.command;
                        mavItem.min_pitch = command_long_msg.param1;
                        mavItem.yaw_angle = command_long_msg.param4;
                        mavItem.latitude = command_long_msg.param5;
                        mavItem.longitude = command_long_msg.param6;
                        mavItem.altitude = command_long_msg.param7;
                        recvCommands.push(mavItem);
                        break;
                    }
                    case MAV_CMD_NAV_LAND: {
                        MavlinkItem mavItem;
                        mavItem.sender_id = message.sysid;
                        mavItem.msg_id = message.msgid;
                        mavItem.cmd_id = command_long_msg.command;
                        mavItem.abort_altitude = command_long_msg.param1;
                        //mavItem.desired_yaw = command_long_msg.param4;
                        mavItem.latitude = command_long_msg.param5;
                        mavItem.longitude = command_long_msg.param6;
                        mavItem.altitude = command_long_msg.param7;
                        // adding commands to the command queue
                        recvCommands.push(mavItem);
//                        printf("%lld [RECV %d] MAV_CMD_NAV_LAND\n", (long long int)current_messages.time_stamps.command_long, message.sysid);
//                        command_ack_result = MAV_RESULT_ACCEPTED;
                        break;
                    }
//                    case MAV_CMD_DO_FOLLOW: {
//                        MavlinkItem mavItem;
//                        mavItem.sender_id = message.sysid;
//                        mavItem.msg_id = message.msgid;
//                        mavItem.cmd_id = current_messages.command_long.command;
//                        mavItem.follow_id = current_messages.command_long.param1;
//                        recvCommands.push(mavItem);
//                        printf("%lld [RECV %d] MAV_CMD_DO_FOLLOW\n", (long long int)current_messages.time_stamps.command_long, message.sysid);
//                        command_ack_result = MAV_RESULT_ACCEPTED;
//                        break;
//                    }
//                    case MAV_CMD_DO_PAUSE_CONTINUE: {
//                        MavlinkItem mavItem;
//                        mavItem.sender_id = message.sysid;
//                        mavItem.msg_id = message.msgid;
//                        mavItem.cmd_id = current_messages.command_long.command;
//                        mavItem.pause_continue = (uint16_t)current_messages.command_long.param1;
//                        recvCommands.push(mavItem);
//                        printf("%lld [RECV %d] MAV_CMD_DO_PAUSE_CONTINUE pauseContinue = %d\n", (long long int)current_messages.time_stamps.command_long, message.sysid, mavItem.pause_continue);
//                        command_ack_result = MAV_RESULT_ACCEPTED;
//                        break;
//                    }
//                    case MAV_CMD_PREFLIGHT_REBOOT_SHUTDOWN: {
//                        // shutdown signal received
//                        shutdown_flag = true;
//                        printf("[RECV] COMMAND_LONG: MAV_CMD_PREFLIGHT_REBOOT_SHUTDOWN\n");
//                        command_ack_result = MAV_RESULT_ACCEPTED;
//                        break;
//                    }
//                    case MAV_CMD_DO_REPOSITION: {
//                        // TODO reposition
//                        printf("[TODO] DO_REPOSITION\n");
//                        command_ack_result = MAV_RESULT_ACCEPTED;
//                        break;
//                    }
//                    case MAV_CMD_REQUEST_AUTOPILOT_CAPABILITIES: {
//                        // TODO request autopilot capabilities
//                        break;
//                    }
//                        // fleet commands
//                    case MAV_CMD_FLEET_UAV_CLEAR_TO_LEAVE: {
//                        MavlinkItem mavItem;
//                        mavItem.sender_id = message.sysid;
//                        mavItem.msg_id = message.msgid;
//                        mavItem.cmd_id = current_messages.command_long.command;
//                        mavItem.request_id = current_messages.command_long.param1;
//                        recvCommands.push(mavItem);
//                        printf("%lld [RECV %d] MAV_CMD_FLEET_UAV_CLEAR_TO_LEAVE %d\n", (long long int)current_messages.time_stamps.command_long, message.sysid, mavItem.request_id);
//                        command_ack_result = MAV_RESULT_ACCEPTED;
//                        break;
//                    }
//                    case MAV_CMD_FLEET_START_MISSION: {
//                        MavlinkItem mavItem;
//                        mavItem.sender_id = message.sysid;
//                        mavItem.msg_id = message.msgid;
//                        mavItem.cmd_id = current_messages.command_long.command;
//                        mavItem.mission_id = current_messages.command_long.param1;
//                        recvCommands.push(mavItem);
//                        printf("%lld [RECV %d] MAV_CMD_FLEET_START_MISSION %d\n", (long long int)current_messages.time_stamps.command_long, message.sysid, mavItem.mission_id);
//                        command_ack_result = MAV_RESULT_ACCEPTED;
//                        break;
//                    }
//                    case MAV_CMD_FLEET_END_MISSION: {
//                        MavlinkItem mavItem;
//                        mavItem.sender_id = message.sysid;
//                        mavItem.msg_id = message.msgid;
//                        mavItem.cmd_id = current_messages.command_long.command;
//                        mavItem.mission_id = current_messages.command_long.param1;
//                        recvCommands.push(mavItem);
//                        printf("%lld [RECV %d] MAV_CMD_FLEET_END_MISSION %d\n", (long long int)current_messages.time_stamps.command_long, message.sysid, mavItem.mission_id);
//                        command_ack_result = MAV_RESULT_ACCEPTED;
//                        break;
//                    }
//                    case MAV_CMD_FLEET_TARGET_SNAPSHOT: {
//                        MavlinkItem mavItem;
//                        mavItem.sender_id = message.sysid;
//                        mavItem.msg_id = message.msgid;
//                        mavItem.cmd_id = current_messages.command_long.command;
//                        mavItem.target_id = current_messages.command_long.param1;
//                        recvCommands.push(mavItem);
//                        printf("%lld [RECV %d] MAV_CMD_FLEET_TARGET_SNAPSHOT %d\n", (long long int)current_messages.time_stamps.command_long, message.sysid, mavItem.target_id);
//                        command_ack_result = MAV_RESULT_ACCEPTED;
//                        break;
//                    }
                    default:
                        printf("[ERRR] UNKNOWN_CMD\n");
                        break;
                    }
//                    // respond with ACK immediately
//                    sendCommandAck(current_messages.command_long.command, command_ack_result);
                }
                break;
            }
            default: {
                printf("Warning, did not handle message id %i\n",message.msgid);
                break;
            }
            }
        } else {
            printf("ERROR: CRC check failed!\n");
        }
}

uint64_t MavlinkManager::get_time_usec() {
    struct timeval tv;
    gettimeofday(&tv, NULL);
    return ((uint64_t)tv.tv_sec) * 1000000 + tv.tv_usec;
}
