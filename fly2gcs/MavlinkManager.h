/*********************************************************************/
//  created:    04/04/2018
//
//  author:     Milan Erdelj, <erdeljmi@gmail.com>
//
//  version:    $Id: $
//
/*********************************************************************/

#ifndef MAVLINKWRAPPER_H
#define MAVLINKWRAPPER_H

#include <thread>
#include <iostream>
#include <queue>
#include <map>
#include <set>
#include <cstring>
#include <unistd.h>
#include <sys/time.h>
#include "include/HDS_AIRMES/mavlink.h"
#include "InfoUAV.h"
#include "AirMesh.h"
#include <fcntl.h>

#define BUFF_IN_LEN 255
#define BUFF_OUT_LEN 255

#define MAX_NEIGH 255

//#define ACK_TIMEOUT 100000 // us

//#define HANDOVER_PARAM 67
//#define SIGNAL_DIVISOR 50

//#ifndef REBOOT_SHUTDOWN_PARAMS
//#define REBOOT_SHUTDOWN_PARAMS

//typedef std::pair<OutMavMessage, int64_t> ClientMavMessage;

//enum TRebootShutdownParam {
//    NO_EFFECT=0,
//    REBOOT=1,
//    SHUTDOWN=2,
//    REBOOT_KEEP_IN_BOOTLOADER=3
//};

//#endif

//struct Time_Stamps {
//    Time_Stamps() {
//        reset_timestamps();
//    }
//    uint64_t mission_ack;
//    uint64_t command_ack;
//    uint64_t command_long;
//    uint64_t timesync;
//    uint64_t mission_count;
//    uint64_t mission_request;
//    uint64_t mission_reached;
//    uint64_t mission_item;
//    uint64_t mission_request_list;
//    uint64_t mission_set_current;
//    uint64_t mission_write_partial_list;
//    uint64_t heartbeat;
//    uint64_t home_position;
//    uint64_t sys_status;
//    uint64_t battery_status;
//    uint64_t radio_status;
//    uint64_t local_position_ned;
//    uint64_t global_position_int;
//    uint64_t set_position_target_local_ned;
//    uint64_t set_position_target_global_int;
//    uint64_t position_target_local_ned;
//    uint64_t position_target_global_int;
//    uint64_t highres_imu;
//    uint64_t attitude;
//    uint64_t system_time;
//    uint64_t clear_all;
//    uint64_t manual_control;
//    uint64_t rc_channels;
//    uint64_t optical_flow_rad;
//    uint64_t fleet_uav_info_local;
//    uint64_t fleet_uav_info_global;
//    uint64_t fleet_uav_request_local;
//    uint64_t fleet_uav_request_global;
//    uint64_t fleet_uav_request_response;
//    uint64_t fleet_uav_elected_id;
//    uint64_t fleet_uav_elected_accept;
//    uint64_t fleet_replacement_in_position;
//    uint64_t fleet_set_formation;
//    uint64_t fleet_set_target;
//    uint64_t fleet_set_target_local;
//    uint64_t fleet_clear_target;
//    uint64_t fleet_target_stream_ready;
//    uint64_t power_status;

//    void reset_timestamps() {
//        mission_request=0;
//        mission_request_list=0;
//        mission_write_partial_list=0;
//        mission_reached=0;
//        command_long=0;
//        timesync=0;
//        mission_count=0;
//        mission_set_current = 0;
//        mission_item=0;
//        mission_ack=0;
//        home_position =0;
//        heartbeat = 0;
//        sys_status = 0;
//        battery_status = 0;
//        radio_status = 0;
//        local_position_ned = 0;
//        global_position_int = 0;
//        set_position_target_local_ned = 0;
//        set_position_target_global_int = 0;
//        position_target_local_ned = 0;
//        position_target_global_int = 0;
//        highres_imu = 0;
//        attitude = 0;
//        system_time = 0;
//        clear_all = 0;
//        manual_control = 0;
//        rc_channels = 0;
//        optical_flow_rad = 0;
//        fleet_uav_info_local = 0;
//        fleet_uav_info_global = 0;
//        fleet_uav_request_local = 0;
//        fleet_uav_request_global = 0;
//        fleet_uav_request_response = 0;
//        fleet_uav_elected_id = 0;
//        fleet_uav_elected_accept = 0;
//        fleet_replacement_in_position = 0;
//        fleet_set_formation = 0;
//        fleet_set_target = 0;
//        fleet_set_target_local = 0;
//        fleet_clear_target = 0;
//        fleet_target_stream_ready = 0;
//        power_status = 0;
//    }
//};

// Struct containing information on the MAV we are currently connected to
struct Mavlink_Messages {
    int sysid;
    int compid;
    mavlink_system_time_t system_time;
    mavlink_mission_request_t mission_request;
    mavlink_mission_request_list_t mission_request_list;
    mavlink_mission_write_partial_list_t mission_write_partial_list;
    mavlink_mission_item_reached_t mission_reached;
    mavlink_mission_clear_all_t clear_all;
    mavlink_command_long_t command_long;
    mavlink_command_ack_t command_ack;
    mavlink_timesync_t timesync;
    mavlink_mission_count_t mission_count;
    mavlink_mission_ack_t mission_ack;
    mavlink_mission_item_t mission_item;
    mavlink_mission_set_current_t mission_set_current;
    mavlink_home_position_t home_position;
    mavlink_heartbeat_t heartbeat;
    mavlink_sys_status_t sys_status;
    mavlink_battery_status_t battery_status;
    mavlink_radio_status_t radio_status;
    mavlink_local_position_ned_t local_position_ned;
    mavlink_global_position_int_t global_position_int;
    mavlink_set_position_target_local_ned_t set_position_target_local_ned;
    mavlink_set_position_target_global_int_t set_position_target_global_int;
    mavlink_position_target_local_ned_t position_target_local_ned;
    mavlink_position_target_global_int_t position_target_global_int;
    mavlink_highres_imu_t highres_imu;
    mavlink_attitude_t attitude;
    mavlink_rc_channels_t rc_channels;
    mavlink_optical_flow_rad_t optical_flow_rad;
    mavlink_manual_control_t manual_control;
    mavlink_fleet_uav_info_local_t fleet_uav_info_local;
    mavlink_fleet_uav_info_global_t fleet_uav_info_global;
    mavlink_fleet_uav_request_local_t fleet_uav_request_local;
    mavlink_fleet_uav_request_global_t fleet_uav_request_global;
    mavlink_fleet_uav_request_response_t fleet_uav_request_response;
    mavlink_fleet_uav_elected_id_t fleet_uav_elected_id;
    mavlink_fleet_uav_elected_accept_t fleet_uav_elected_accept;
    mavlink_fleet_replacement_in_position_t fleet_replacement_in_position;
    mavlink_fleet_set_formation_t fleet_set_formation;
    mavlink_fleet_set_target_t fleet_set_target;
    mavlink_fleet_set_target_local_t fleet_set_target_local;
    mavlink_fleet_clear_target_t fleet_clear_target;
    mavlink_fleet_target_stream_ready_t fleet_target_stream_ready;
    mavlink_power_status_t power_status;
};


struct MavlinkItem {
    // common
    uint8_t sender_id;
    uint16_t msg_id;
    uint16_t cmd_id;
    uint8_t target_system;
    uint8_t target_component;

    // mission commands

    uint16_t sequence;
    uint16_t jump_sequence;
    uint16_t jump_repeat_count;
    uint8_t use_current;
    uint16_t set_current;
    uint64_t hold_time;
    uint8_t follow_id;
    float acceptance_radius;
    float latitude, longitude, altitude, relative_alt;
    float x, y, z;
    float vx, vy, vz;
    float afx, afy, afz;
    float yaw, yaw_rate;
    float abort_altitude;
    float desired_yaw;
    float yaw_angle;
    float min_pitch;
    uint8_t pause_continue;
    uint16_t first_item, last_item;
    uint8_t mode;
    uint32_t time_boot_ms;
    uint8_t coordinate_frame;
    uint16_t type_mask;
    int32_t lat_int, lon_int;
    bool callback_flag;
    uint64_t callback_period;
    uint16_t callback_message;
    uint8_t guided_enable;
    uint8_t armed;
    float landing_target_number;
    float descent_rate;
    float max_accepted_offset;

    // fleet commands
    // replacement requests
    uint8_t request_type, request_id, id_to_replace, request_urgency;
    uint8_t elected_id;
    //float x, y, z;
    //uint32_t latitude, longitude;
    //uint16_t altitude, relative_alt;
    uint64_t arrival_time;
    // set formation
    uint8_t formation_type;
    uint8_t leader_id;
    uint32_t barycenter_lat, barycenter_long;
    uint16_t barycenter_alt, barycenter_relative_alt;
    float width_bound, length_bound, height_bound;
    // set target
    uint8_t target_id, target_type;
    int16_t heading;
    uint8_t add_replace;
    uint16_t drone_count;
    float perimeter; // only if POI coverage
    // start/end mission
    uint8_t mission_id;

    MavlinkItem() {
        sender_id = 0;
        set_current = 0;
        target_system = target_component = 0;
        first_item = last_item = 0;
        mode = 0;
        armed = 0;
        follow_id = 0;
        pause_continue = use_current = 0;
        hold_time = 0;
        guided_enable = 0;
        msg_id = cmd_id = sequence = 0;
        jump_sequence = jump_repeat_count = 0;
        acceptance_radius = yaw_angle = latitude = longitude = altitude = relative_alt = x = y = z = 0;
        desired_yaw = abort_altitude = 0;
        min_pitch = 0;
        vx = vy = vz = 0;
        afx = afy = afz = 0;
        yaw = yaw_rate = 0;
        time_boot_ms = 0;
        coordinate_frame = 0;
        landing_target_number = 0;
        descent_rate = 0;
        max_accepted_offset = 0;
        type_mask = 0;
        lat_int = lon_int = 0;
        callback_flag = false;
        callback_message = 0;
        callback_period = 1000000;
        request_type = request_id = id_to_replace = request_urgency = 0;
        elected_id = 0;
        arrival_time = 0;
        formation_type = FLEET_FORMATION_RING;
        leader_id = 0;
        barycenter_lat = barycenter_long = 0;
        barycenter_alt = barycenter_relative_alt = 0;
        width_bound = length_bound = height_bound = 0;
        target_id = 0;
        target_type = FLEET_TARGET_POINT;
        heading = 0;
        add_replace = 0;
        drone_count = 1;
        perimeter = 10;
        mission_id = 0;
    }
};

typedef struct FormationParams {
    double d = 1.5;
    double epsilon = 0.1;
    double FoV = 2;
    double AttractMax = 1, RepulsionMax = 1;
    double h_ = 0.6;
    double c1 = 0.1, c2 = 0.2;
    double Kd_form = 0.5, Kp_form = 1 , Ki_form = 0.2;
    double T= 0.1;
    double ux_sat = 0.1, uy_sat = 0.1;
    double uz_sat = 0.1;
    double Radius = 1.5;
    int norm_type = 0;//Olfati, Euclidean, Sum control, Average control

    /// formation params defined by Milan
    uint8_t formationType;
    uint8_t leaderID;
    uint32_t barycenter_lat;
    uint32_t barycenter_long;
    uint16_t barycenter_alt;
    uint16_t barycenter_relative_alt;
    float width_bound;
    float length_bound;
    float height_bound;

} FormationParams_t;


struct ReqResp {
    uint8_t system_id;
    uint8_t request_id;
    uint64_t arrival_time;
    float distance;
};

class MavlinkManager {
public:
    MavlinkManager(uint8_t sysid, int cmdPort, int infoPort, bool debug_info, string config_file);
    ~MavlinkManager();

    uint8_t my_system_id;
    uint8_t my_comp_id = 0;

    uint8_t buff_in[BUFF_IN_LEN];
    ssize_t recsize;

    int system_id;
    int autopilot_id;
    int component_id;
    bool home_position_set;
    bool ack;
    bool Xtimesync;
    int state;
    int typeack;
    bool request;
    bool waypoint;
    int frame;
    int command;
    int seq;
    int seqr;
    int seqold;
    int count;
    int Waycount;
    int Wayseq;
    int compt;
    uint16_t message_interval;
    bool debug_messages;

    InfoUAV neighUAVs[MAX_NEIGH];

    string config_file;

    std::vector<ReqResp> reqResponses;
    bool req_accepted;

    bool replacement_in_position;

    bool missionActive;
    bool getMissionStatus() const { return missionActive; }
    void missionStarted() { missionActive = true; }
    void missionStopped() { missionActive = false; }

    // queue of mission items (plan de vol)
    std::queue<MavlinkItem> missionPlan;
    const std::queue<MavlinkItem>& getMissionPlan() const { return missionPlan; }
    void clearMissionPlan() { std::queue<MavlinkItem> empty; std::swap(missionPlan, empty); }

    // queue of received commands (integrated both mission and fleet commands)
    std::queue<MavlinkItem> recvCommands;
    const std::queue<MavlinkItem>& getCommands() const { return recvCommands; }
    void clearCommands() { std::queue<MavlinkItem> empty; std::swap(recvCommands, empty); }

    Mavlink_Messages current_messages;

    queue<mavlink_message_t> recvMessages;

    string mavlink_to_string(const mavlink_message_t *msg);

    bool check_mavlink_crc(uint8_t *buff_in, ssize_t recsize, uint8_t msgid);
    void decode_mavlink(uint8_t *buff_in, ssize_t size, mavlink_message_t message);
    void parse_buffer(uint8_t *buff_in, ssize_t size);
    void parse_string(std::string str_in, ssize_t size);

    // messages
    string Heartbeat(uint8_t heartbeat_uav_type, uint8_t uav_autopilot, uint8_t uav_base_mode, uint32_t uav_custom_mode, uint8_t uav_system_status);
    string SystemStatus(uint32_t onboardSensorsPresent, uint32_t onboardSensorsEnabled, uint32_t onboardSensorsHealth, uint16_t load, uint16_t voltage, int16_t current, int8_t batteryRemaining, uint16_t dropRateComm, uint16_t errorsComm, uint16_t errors1, uint16_t errors2, uint16_t errors3, uint16_t errors4); // callback
//    void sendBatteryStatus(uint8_t id, uint8_t battery_function, uint8_t type, int16_t temperature, uint16_t *voltages, int16_t current, int32_t currentConsumed, int32_t energyConsumed, int8_t batteryRemaining); // callback
//    void sendSystemTime();
//    void sendInfoUAV(uint8_t heartbeat_uav_type, uint8_t uav_autopilot, uint8_t uav_base_mode, uint32_t uav_custom_mode, uint8_t uav_system_status,  uint16_t voltage, int8_t batteryRemaining, uint16_t dropRateComm, uint16_t errorsComm, float x, float y, float z, float vx, float vy, float vz);
//    void sendAttitude(float roll, float pitch, float yaw, float rollspeed, float pitchspeed, float yawspeed);
//    void sendAttitudeTarget(uint32_t timeBootMs, uint8_t typeMask, float* q, float bodyRollRate, float bodyPitch_rate, float bodyYawRate, float thrust);
//    void updateSetpoint(mavlink_set_position_target_local_ned_t setpoint);
//    void sendMissionAck(uint8_t targetSystem, uint8_t targetComponent, uint8_t type);
//    void sendCommandAck(uint16_t command, uint8_t result);
//    void sendAutopilotVersion(uint64_t capabilities, uint32_t flight_sw_version, uint32_t middleware_sw_version, uint32_t os_sw_version, uint32_t board_version, uint8_t *flight_custom_version, uint8_t *middleware_custom_version, uint8_t *os_custom_version, uint16_t vendor_id, uint16_t product_id, uint64_t uid);
//    void sendMissionCount(uint8_t targetSystem, uint8_t targetComponent, uint16_t count);
    string CommandLong(uint8_t targetSystem, uint8_t targetComponent, uint16_t command, uint8_t confirmation, float param1, float param2, float param3, float param4, float param5, float param6, float param7);
//    void sendMissionWritePartialList(uint8_t targetSystem, uint8_t targetComponent, uint16_t startIndex, uint16_t endIndex);
    string MissionItem(uint8_t targetSystem, uint8_t targetComponent, uint16_t seq, uint8_t frame, uint16_t command, uint8_t current, uint8_t autocontinue, float param1, float param2, float param3, float param4, float x, float y, float z);
    string MissionSetCurrent(uint8_t targetSystem, uint8_t targetComponent, uint16_t seq);
    string MissionClearAll(uint8_t targetSystem, uint8_t targetComponent);
//    void sendPowerStatus(uint16_t Vcc, uint16_t Vservo, uint16_t flags);

//    void sendMissionRequestList(uint8_t targetSystem, uint8_t targetComponent);
//    void sendMissionItemReached(uint16_t seq);
    string LocalPositionNED(float x, float y, float z, float vx, float vy, float vz); // callback
//    void sendGlobalPositionInt(int32_t lat, int32_t lon, int32_t alt, int32_t relativeAlt, int16_t vx, int16_t vy, int16_t vz, uint16_t yawAngle);
//    void sendSetPositionTargetLocalNED(uint32_t timeBootMs, uint8_t targetSystem, uint8_t targetComponent, uint8_t coordinateFrame, uint16_t typeMask, float x, float y, float z, float vx, float vy, float vz, float afx, float afy, float afz, float yaw, float yaw_rate);
//    void sendPositionTargetLocalNED(uint32_t timeBootMs, uint8_t coordinateFrame, uint16_t typeMask, float x, float y, float z, float vx, float vy, float vz, float afx, float afy, float afz, float yaw, float yaw_rate);
//    void sendSetPositionTargetGlobalInt(uint32_t time_boot_ms, uint8_t targetSystem, uint8_t targetComponent, uint8_t coordinateFrame, uint16_t typeMask, int32_t lat_int, int32_t lon_int, float alt, float vx, float vy, float vz, float afx, float afy, float afz, float yaw, float yaw_rate);
//    void sendPositionTargetGlobalInt(uint32_t time_boot_ms, uint8_t coordinateFrame, uint16_t typeMask, int32_t lat_int, int32_t lon_int, float alt, float vx, float vy, float vz, float afx, float afy, float afz, float yaw, float yaw_rate);
//    void sendRCChannels(uint32_t time_boot_ms, uint8_t chancount, uint16_t chan1_raw, uint16_t chan2_raw, uint16_t chan3_raw, uint16_t chan4_raw, uint16_t chan5_raw, uint16_t chan6_raw, uint16_t chan7_raw, uint16_t chan8_raw, uint16_t chan9_raw, uint16_t chan10_raw, uint16_t chan11_raw, uint16_t chan12_raw, uint16_t chan13_raw, uint16_t chan14_raw, uint16_t chan15_raw, uint16_t chan16_raw, uint16_t chan17_raw, uint16_t chan18_raw, uint8_t rssi);
//    void sendHighresIMU(uint64_t time_usec, float xacc, float yacc, float zacc, float xgyro, float ygyro, float zgyro, float xmag, float ymag, float zmag, float abs_pressure, float diff_pressure, float pressure_alt, float temperature, uint16_t fields_updated);
//    void sendOpticalFlowRad(uint64_t time_usec, uint8_t sensor_id, uint32_t integration_time_us, float integrated_x, float integrated_y, float integrated_xgyro, float integrated_ygyro, float integrated_zgyro, int16_t temperature, uint8_t quality, uint32_t time_delta_distance_us, float distance);
//    void sendManualControl(uint8_t target, int16_t x, int16_t y, int16_t z, int16_t r, uint16_t buttons);
//    // AIRMES specific
//    void sendFleetUavInfoLocal(uint8_t base_mode, uint8_t custom_state, uint8_t system_status, uint16_t voltage_battery, uint16_t battery_remaining, uint16_t drop_rate_comm, uint16_t errors_comm, float x, float y, float z, float heading);
//    void sendFleetUavInfoGlobal(uint8_t base_mode, uint8_t custom_state, uint8_t system_status, uint16_t voltage_battery, uint16_t battery_remaining, uint16_t drop_rate_comm, uint16_t errors_comm, uint32_t latitude, uint32_t longitude, uint16_t altitude, uint16_t relative_alt, int16_t heading);
    string FleetUavRequestLocal(uint8_t requestType, uint8_t requestID, uint8_t idToReplace, float x, float y, float z, uint8_t urgency);
//    void sendFleetUavRequestGlobal(uint8_t requestType, uint8_t requestID, uint8_t idToReplace, uint32_t latitude, uint32_t longitude, uint16_t altitude, uint8_t urgency);
//    void sendFleetUavRequestResponse(uint8_t request_id, uint64_t arrival_time);
    string FleetUavElectedID(uint8_t request_id, uint8_t elected_id);
//    void sendFleetUavElectedAccept(uint8_t request_id);
//    void sendFleetReplacementInPosition(uint8_t request_id);
//    void sendFleetSetFormation(uint8_t formationType, uint8_t leaderID, uint32_t barycenter_lat, uint32_t barycenter_long, uint16_t barycenter_alt, uint16_t barycenter_relative_alt, float width_bound, float length_bound, float height_bound, double d, double epsilon, double FoV, double AttractMax, double RepulsionMax, double h_, double c1, double c2, double Kd_form, double Kp_form, double Ki_form, double T, double ux_sat, double uy_sat, double uz_sat, double Radius, uint8_t norm_type);
//    void sendFleetSetTarget(uint8_t target_id, uint8_t target_type, uint32_t latitude, uint32_t longitude, uint16_t altitude, uint16_t relative_alt, int16_t heading, uint8_t add_replace, uint16_t drone_count, float perimeter);
    string FleetSetTargetLocal(uint8_t target_id, uint8_t target_type, float x, float y, float z, int16_t heading, uint8_t add_replace, uint16_t drone_count, float perimeter);
//    void sendFleetClearTarget(uint8_t target_id);
//    void sendFleetTargetStreamReady(uint8_t target_id);

//    // mavlink commands
//    void cmdSetMessageInterval(uint8_t targetSystem, uint8_t targetComponent, uint8_t messageID, int64_t interval_usec);
//    void cmdNavWaypoint(uint8_t targetSystem, uint8_t targetComponent, float holdTime, float proximityRadius, float passRadius, float desiredYaw, float latitude, float longitude, float altitude);
    string cmdNavLand(uint8_t targetSystem, uint8_t targetComponent, float abortAlt, float desiredYaw, float latitude, float longitude, float altitude); // land at location
//    void cmdNavLandLocal(uint8_t targetSystem, uint8_t targetComponent, float landingTargetNumber, float maxAcceptedOffset, float landingDescentRate, float desiredYaw, float x, float y, float z); // land at current location
//    void cmdDoLandStart(uint8_t targetSystem, uint8_t targetComponent, float latitude, float longitude); // mission command to perform landing
    string cmdNavTakeoff(uint8_t targetSystem, uint8_t targetComponent, float desiredPitch, float magnetometerYaw, float latitude, float longitude, float altitude);
    string cmdDoSetMode(uint8_t targetSystem, uint8_t targetComponent, uint8_t mavMode);
//    void cmdDoSetHome(uint8_t targetSystem, uint8_t targetComponent, uint8_t useCurrent);
    string cmdDoFollow(uint8_t targetSystem, uint8_t targetComponent, uint8_t follow_id);
//    void cmdGetHomePosition(uint8_t targetSystem, uint8_t targetComponent);
    string cmdMissionStart(uint8_t targetSystem, uint8_t targetComponent, uint8_t firstItem, uint8_t lastItem);
    string cmdDoSetParameter(uint8_t targetSystem, uint8_t targetComponent, uint8_t paramNumber, float paramValue);
//    void cmdRequestAutopilotCapabilities(uint8_t targetSystem, uint8_t targetComponent);
    string cmdNavReturnToLaunch(uint8_t targetSystem, uint8_t targetComponent);
    string cmdDoPauseContinue(uint8_t targetSystem, uint8_t targetComponent, uint8_t pauseContinue);
//    //void cmdVideoStartCapture(uint8_t targetSystem, uint8_t targetComponent);
//    //void cmdVideoStopCapture(uint8_t targetSystem, uint8_t targetComponent);
//    void cmdRebootShutdown(uint8_t targetSystem, uint8_t targetComponent, uint8_t autopilot, uint8_t onboardComputer);
//    // AIRMES specific
//    void cmdFleetUavClearToLeave(uint8_t targetSystem, uint8_t targetComponent);
    string cmdFleetStartMission(uint8_t targetSystem, uint8_t targetComponent, uint8_t mission_id);
    string cmdFleetEndMission(uint8_t targetSystem, uint8_t targetComponent, uint8_t mission_id);
//    void cmdFleetTargetSnapshot(uint8_t targetSystem, uint8_t targetComponent, uint8_t target_id);

    inline void clearBuffer(uint8_t *buffer, int len);

    uint64_t get_time_usec();

private:

    // FIXME work's only for C++11
    u_int8_t hds_mavlink_message_crcs[256] = MAVLINK_MESSAGE_CRCS;
};

#endif // MAVLINKSERVER_H
