#include "ZeroMQ.h"
#include <zmq.hpp>
#include "include/flatbuffers_generated.h"
#include <regex>
#include <iostream>
#include "include/flatbuffers/flatbuffers.h"
#include "include/fleetgcs-telemetry_generated.h"

using namespace std;

#define THREAD_IO_SLEEP 10

ZeroMQ::ZeroMQ()
{
    zmq_th = std::thread(&ZeroMQ::zeromq_thread, this);
}

ZeroMQ::~ZeroMQ()
{
    zmq_th.join();
}

void ZeroMQ::zeromq_thread() {
    zmq::context_t context(1);
    zmq::socket_t receiveSocket(context, ZMQ_PULL);
    // TODO : let the user configure the port
    receiveSocket.bind("tcp://*:9393");
    while (1) {
        printf("waiting for a message\n");
        zmq::message_t message;
        receiveSocket.recv(&message);
        char* envl = (char*) message.data();
        zmq::message_t dataMessage;
        receiveSocket.recv(&dataMessage);
        char* encodedMessage = (char*) dataMessage.data();

        string rawEnvl(envl);
        // regex to catch the correct message type : \;(.*)
        regex pattern{"\;(.*)"};
        auto matchIterator = sregex_iterator(rawEnvl.begin(), rawEnvl.end(), pattern);
        string strEnvl = matchIterator->str();
        cout << "  " << strEnvl << '\n';
        strEnvl = strEnvl.substr(1, strEnvl.size() - 1);

        cout << "enveloppe is :" << strEnvl << "\n";

        // TODO: compare enveloppe, parse the message into a flatbuffers, parse the flatbuffers into a mavlink message
        parseMessage(strEnvl, encodedMessage);

        // recv zeromq message
        recvMsg.push(strEnvl);

        // rest a bit
        this_thread::sleep_for(std::chrono::milliseconds(THREAD_IO_SLEEP));
    }
}

void ZeroMQ::parseMessage(string strEnvl, char* encodedMessage){
    flatbuffers::FlatBufferBuilder builder(1024);

    if (strEnvl.compare("fleet_set_formation")) {
        auto message = FleetMessage::GetFLEET_SET_FORMATION(encodedMessage);
        /*
        MavlinkItem mavItem;
        mavItem.msg_id = MAVLINK_MSG_ID_FLEET_SET_FORMATION;
        //mavItem.cmd_id;
        mavItem.formation_type = message->formation_type();
        mavItem.leader_id = message->leader_ID();
        mavItem.barycenter_lat = message->barycenter_lat();
        mavItem.barycenter_long = message->barycenter_lon();
        mavItem.barycenter_alt = message->barycenter_alt();
        mavItem.barycenter_relative_alt = message->barycenter_relative_alt();
        mavItem.width_bound = message->width_bound();
        mavItem.length_bound = message->length_bound();
        mavItem.height_bound = message->height_bound();
        recvCommands.push(mavItem);
        */
        // TODO call this:
//        sendFleetSetFormation(message->formation_type(), message->leader_ID(),
//                              message->barycenter_lat(), message->barycenter_lon(),
//                              message->barycenter_alt(), message->barycenter_relative_alt(),
//                              message->width_bound(), message->length_bound(), message->height_bound(),
//                              0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
    } else if (strEnvl.compare("fleet_start_mission")) {
        auto message = FleetMessage::GetFLEET_START_MISSION(encodedMessage);
        /*
        MavlinkItem mavItem;
        mavItem.msg_id = MAVLINK_MSG_ID_COMMAND_LONG;
        mavItem.cmd_id = MAV_CMD_FLEET_START_MISSION;
        mavItem.mission_id = message->mission_ID();
        recvCommands.push(mavItem);
        */
        // TODO call this:
        //cmdFleetStartMission(0,0,message->mission_ID());
    } else if (strEnvl.compare("fleet_set_target_local")) {
        auto message = FleetMessage::GetFLEET_SET_TARGET_LOCAL(encodedMessage);
        /*
        MavlinkItem mavItem;
        mavItem.msg_id = MAVLINK_MSG_ID_FLEET_SET_TARGET_LOCAL;
        mavItem.target_id = message->target_ID();
        mavItem.target_type = message->target_type();
        mavItem.x = message->x();
        mavItem.y = message->y();
        mavItem.z = message->z();
        mavItem.heading = message->heading();
        mavItem.add_replace = message->add_replace();
        mavItem.drone_count = message->drone_count();
        mavItem.perimeter = message->perimeter();
        recvCommands.push(mavItem);
        */
        // TODO call this:
//        sendFleetSetTargetLocal(message->target_ID(), message->target_type(), message->x(),
//                                message->y(), message->z(), message->heading(), message->add_replace(),
//                                message->drone_count(), message->perimeter());
    } else if (strEnvl.compare("fleet_clear_target")) {
        auto message = FleetMessage::GetFLEET_CLEAR_TARGET(encodedMessage);
       /*
        MavlinkItem mavItem;
        mavItem.msg_id = MAVLINK_MSG_ID_FLEET_CLEAR_TARGET;
        mavItem.target_id = message->target_ID();
        recvCommands.push(mavItem);
       */
        // TODO call this:
//        sendFleetClearTarget(message->target_ID());
    } else if (strEnvl.compare("fleet_stop_mission")) {
        auto message = FleetMessage::GetFLEET_STOP_MISSION(encodedMessage);
        // wtf
        // does not exist ?
    } else if (strEnvl.compare("fleet_set_target")) {
        auto message = FleetMessage::GetFLEET_SET_TARGET(encodedMessage);
        // TODO call this:
//        sendFleetSetTarget(message->target_ID(), message->target_type(),
//                           message->lat(), message->lon(),message->alt(), message->relative_alt(),
//                           message->heading(), message->add_replace(), message->drone_count(), message->perimeter());
        /*
        MavlinkItem mavItem;
        mavItem.msg_id = MAVLINK_MSG_ID_FLEET_SET_TARGET;
        mavItem.target_id = message->target_ID();
        mavItem.target_type = message->target_type();
        mavItem.latitude = message->lat();
        mavItem.longitude = message->lon();
        mavItem.altitude = message->alt();
        mavItem.relative_alt = message->alt();
        mavItem.heading = message->heading();
        mavItem.add_replace = message->add_replace();
        mavItem.drone_count = message->drone_count();
        mavItem.perimeter = message->perimeter();
        recvCommands.push(mavItem);
        */
    }

}
