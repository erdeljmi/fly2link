#ifndef ZEROMQ_H
#define ZEROMQ_H

#include <thread>
#include "include/fleet_clear_target_generated.h"
#include "include/fleet_set_formation_generated.h"
#include "include/fleet_set_target_generated.h"
#include "include/fleet_set_target_local_generated.h"
#include "include/fleet_start_mission_generated.h"
#include "include/fleet_stop_mission_generated.h"
#include <queue>

class ZeroMQ
{
public:
    ZeroMQ();
    ~ZeroMQ();
    std::queue<std::string> recvMsg;
private:
    std::thread zmq_th;
    void zeromq_thread();

    void parseMessage(std::string strEnvl, char* encodedMessage);
};

#endif // ZEROMQ_H
