/****************************************************************************
** Meta object code from reading C++ file 'FleetControl.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.5.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../FleetControl.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'FleetControl.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.5.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_FleetControl_t {
    QByteArrayData data[45];
    char stringdata0[604];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_FleetControl_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_FleetControl_t qt_meta_stringdata_FleetControl = {
    {
QT_MOC_LITERAL(0, 0, 12), // "FleetControl"
QT_MOC_LITERAL(1, 13, 20), // "missionStartWaypoint"
QT_MOC_LITERAL(2, 34, 0), // ""
QT_MOC_LITERAL(3, 35, 18), // "missionEndWaypoint"
QT_MOC_LITERAL(4, 54, 11), // "clearAllWPs"
QT_MOC_LITERAL(5, 66, 14), // "returnToLaunch"
QT_MOC_LITERAL(6, 81, 13), // "emergencyLand"
QT_MOC_LITERAL(7, 95, 13), // "sendWaypoints"
QT_MOC_LITERAL(8, 109, 7), // "uav_set"
QT_MOC_LITERAL(9, 117, 7), // "uint8_t"
QT_MOC_LITERAL(10, 125, 6), // "uav_id"
QT_MOC_LITERAL(11, 132, 13), // "pauseContinue"
QT_MOC_LITERAL(12, 146, 12), // "initialElect"
QT_MOC_LITERAL(13, 159, 18), // "replacementRequest"
QT_MOC_LITERAL(14, 178, 10), // "clearTable"
QT_MOC_LITERAL(15, 189, 16), // "checkReplacement"
QT_MOC_LITERAL(16, 206, 14), // "handleElection"
QT_MOC_LITERAL(17, 221, 17), // "handleReplacement"
QT_MOC_LITERAL(18, 239, 16), // "loadWaypointFile"
QT_MOC_LITERAL(19, 256, 12), // "StartMission"
QT_MOC_LITERAL(20, 269, 11), // "StopMission"
QT_MOC_LITERAL(21, 281, 9), // "SetTarget"
QT_MOC_LITERAL(22, 291, 12), // "fleetTakeOff"
QT_MOC_LITERAL(23, 304, 9), // "fleetLand"
QT_MOC_LITERAL(24, 314, 16), // "SetRingFormation"
QT_MOC_LITERAL(25, 331, 16), // "SetLineFormation"
QT_MOC_LITERAL(26, 348, 18), // "SetColumnFormation"
QT_MOC_LITERAL(27, 367, 17), // "SetStackFormation"
QT_MOC_LITERAL(28, 385, 18), // "callbackSendTarget"
QT_MOC_LITERAL(29, 404, 24), // "callbackUpdateNeighTable"
QT_MOC_LITERAL(30, 429, 17), // "callbackHeartbeat"
QT_MOC_LITERAL(31, 447, 15), // "callbackInfoUAV"
QT_MOC_LITERAL(32, 463, 12), // "UpdateTarget"
QT_MOC_LITERAL(33, 476, 27), // "CalculateFormationPositions"
QT_MOC_LITERAL(34, 504, 9), // "formation"
QT_MOC_LITERAL(35, 514, 6), // "targ_x"
QT_MOC_LITERAL(36, 521, 6), // "targ_y"
QT_MOC_LITERAL(37, 528, 3), // "alt"
QT_MOC_LITERAL(38, 532, 9), // "perimeter"
QT_MOC_LITERAL(39, 542, 7), // "heading"
QT_MOC_LITERAL(40, 550, 10), // "num_drones"
QT_MOC_LITERAL(41, 561, 7), // "sendZMQ"
QT_MOC_LITERAL(42, 569, 10), // "current_id"
QT_MOC_LITERAL(43, 580, 16), // "fakeTelemetryZMQ"
QT_MOC_LITERAL(44, 597, 6) // "to_rad"

    },
    "FleetControl\0missionStartWaypoint\0\0"
    "missionEndWaypoint\0clearAllWPs\0"
    "returnToLaunch\0emergencyLand\0sendWaypoints\0"
    "uav_set\0uint8_t\0uav_id\0pauseContinue\0"
    "initialElect\0replacementRequest\0"
    "clearTable\0checkReplacement\0handleElection\0"
    "handleReplacement\0loadWaypointFile\0"
    "StartMission\0StopMission\0SetTarget\0"
    "fleetTakeOff\0fleetLand\0SetRingFormation\0"
    "SetLineFormation\0SetColumnFormation\0"
    "SetStackFormation\0callbackSendTarget\0"
    "callbackUpdateNeighTable\0callbackHeartbeat\0"
    "callbackInfoUAV\0UpdateTarget\0"
    "CalculateFormationPositions\0formation\0"
    "targ_x\0targ_y\0alt\0perimeter\0heading\0"
    "num_drones\0sendZMQ\0current_id\0"
    "fakeTelemetryZMQ\0to_rad"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_FleetControl[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      32,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,  174,    2, 0x0a /* Public */,
       3,    0,  175,    2, 0x0a /* Public */,
       4,    0,  176,    2, 0x0a /* Public */,
       5,    0,  177,    2, 0x0a /* Public */,
       6,    0,  178,    2, 0x0a /* Public */,
       7,    2,  179,    2, 0x0a /* Public */,
      11,    0,  184,    2, 0x0a /* Public */,
      12,    0,  185,    2, 0x0a /* Public */,
      13,    0,  186,    2, 0x0a /* Public */,
      14,    0,  187,    2, 0x0a /* Public */,
      15,    0,  188,    2, 0x0a /* Public */,
      16,    0,  189,    2, 0x0a /* Public */,
      17,    0,  190,    2, 0x0a /* Public */,
      18,    0,  191,    2, 0x0a /* Public */,
      19,    0,  192,    2, 0x0a /* Public */,
      20,    0,  193,    2, 0x0a /* Public */,
      21,    0,  194,    2, 0x0a /* Public */,
      22,    0,  195,    2, 0x0a /* Public */,
      23,    0,  196,    2, 0x0a /* Public */,
      24,    0,  197,    2, 0x0a /* Public */,
      25,    0,  198,    2, 0x0a /* Public */,
      26,    0,  199,    2, 0x0a /* Public */,
      27,    0,  200,    2, 0x0a /* Public */,
      28,    0,  201,    2, 0x0a /* Public */,
      29,    0,  202,    2, 0x0a /* Public */,
      30,    0,  203,    2, 0x0a /* Public */,
      31,    0,  204,    2, 0x0a /* Public */,
      32,    0,  205,    2, 0x0a /* Public */,
      33,    7,  206,    2, 0x0a /* Public */,
      41,    1,  221,    2, 0x0a /* Public */,
      43,    0,  224,    2, 0x0a /* Public */,
      44,    1,  225,    2, 0x0a /* Public */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool, 0x80000000 | 9,    8,   10,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 9, QMetaType::Float, QMetaType::Float, QMetaType::Float, QMetaType::Float, QMetaType::Float, 0x80000000 | 9,   34,   35,   36,   37,   38,   39,   40,
    QMetaType::Void, 0x80000000 | 9,   42,
    QMetaType::Void,
    QMetaType::Double, QMetaType::Double,    2,

       0        // eod
};

void FleetControl::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        FleetControl *_t = static_cast<FleetControl *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->missionStartWaypoint(); break;
        case 1: _t->missionEndWaypoint(); break;
        case 2: _t->clearAllWPs(); break;
        case 3: _t->returnToLaunch(); break;
        case 4: _t->emergencyLand(); break;
        case 5: _t->sendWaypoints((*reinterpret_cast< bool(*)>(_a[1])),(*reinterpret_cast< uint8_t(*)>(_a[2]))); break;
        case 6: _t->pauseContinue(); break;
        case 7: _t->initialElect(); break;
        case 8: _t->replacementRequest(); break;
        case 9: _t->clearTable(); break;
        case 10: _t->checkReplacement(); break;
        case 11: _t->handleElection(); break;
        case 12: _t->handleReplacement(); break;
        case 13: _t->loadWaypointFile(); break;
        case 14: _t->StartMission(); break;
        case 15: _t->StopMission(); break;
        case 16: _t->SetTarget(); break;
        case 17: _t->fleetTakeOff(); break;
        case 18: _t->fleetLand(); break;
        case 19: _t->SetRingFormation(); break;
        case 20: _t->SetLineFormation(); break;
        case 21: _t->SetColumnFormation(); break;
        case 22: _t->SetStackFormation(); break;
        case 23: _t->callbackSendTarget(); break;
        case 24: _t->callbackUpdateNeighTable(); break;
        case 25: _t->callbackHeartbeat(); break;
        case 26: _t->callbackInfoUAV(); break;
        case 27: _t->UpdateTarget(); break;
        case 28: _t->CalculateFormationPositions((*reinterpret_cast< uint8_t(*)>(_a[1])),(*reinterpret_cast< float(*)>(_a[2])),(*reinterpret_cast< float(*)>(_a[3])),(*reinterpret_cast< float(*)>(_a[4])),(*reinterpret_cast< float(*)>(_a[5])),(*reinterpret_cast< float(*)>(_a[6])),(*reinterpret_cast< uint8_t(*)>(_a[7]))); break;
        case 29: _t->sendZMQ((*reinterpret_cast< uint8_t(*)>(_a[1]))); break;
        case 30: _t->fakeTelemetryZMQ(); break;
        case 31: { double _r = _t->to_rad((*reinterpret_cast< double(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< double*>(_a[0]) = _r; }  break;
        default: ;
        }
    }
}

const QMetaObject FleetControl::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_FleetControl.data,
      qt_meta_data_FleetControl,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *FleetControl::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *FleetControl::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_FleetControl.stringdata0))
        return static_cast<void*>(const_cast< FleetControl*>(this));
    return QWidget::qt_metacast(_clname);
}

int FleetControl::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 32)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 32;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 32)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 32;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
