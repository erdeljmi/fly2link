/****************************************************************************
** Meta object code from reading C++ file 'Fly2Recv.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.5.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../Fly2Recv.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'Fly2Recv.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.5.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_Fly2Recv_t {
    QByteArrayData data[24];
    char stringdata0[255];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Fly2Recv_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Fly2Recv_t qt_meta_stringdata_Fly2Recv = {
    {
QT_MOC_LITERAL(0, 0, 8), // "Fly2Recv"
QT_MOC_LITERAL(1, 9, 16), // "SetRingFormation"
QT_MOC_LITERAL(2, 26, 0), // ""
QT_MOC_LITERAL(3, 27, 16), // "SetLineFormation"
QT_MOC_LITERAL(4, 44, 18), // "SetColumnFormation"
QT_MOC_LITERAL(5, 63, 13), // "UpdateZmqHost"
QT_MOC_LITERAL(6, 77, 14), // "SimulateDrones"
QT_MOC_LITERAL(7, 92, 14), // "UpdateUAVTable"
QT_MOC_LITERAL(8, 107, 10), // "clearTable"
QT_MOC_LITERAL(9, 118, 27), // "CalculateFormationPositions"
QT_MOC_LITERAL(10, 146, 6), // "PosUAV"
QT_MOC_LITERAL(11, 153, 7), // "uint8_t"
QT_MOC_LITERAL(12, 161, 9), // "formation"
QT_MOC_LITERAL(13, 171, 6), // "targ_x"
QT_MOC_LITERAL(14, 178, 6), // "targ_y"
QT_MOC_LITERAL(15, 185, 3), // "alt"
QT_MOC_LITERAL(16, 189, 9), // "perimeter"
QT_MOC_LITERAL(17, 199, 7), // "heading"
QT_MOC_LITERAL(18, 207, 10), // "num_drones"
QT_MOC_LITERAL(19, 218, 2), // "id"
QT_MOC_LITERAL(20, 221, 7), // "sendZMQ"
QT_MOC_LITERAL(21, 229, 10), // "current_id"
QT_MOC_LITERAL(22, 240, 7), // "PollZMQ"
QT_MOC_LITERAL(23, 248, 6) // "to_rad"

    },
    "Fly2Recv\0SetRingFormation\0\0SetLineFormation\0"
    "SetColumnFormation\0UpdateZmqHost\0"
    "SimulateDrones\0UpdateUAVTable\0clearTable\0"
    "CalculateFormationPositions\0PosUAV\0"
    "uint8_t\0formation\0targ_x\0targ_y\0alt\0"
    "perimeter\0heading\0num_drones\0id\0sendZMQ\0"
    "current_id\0PollZMQ\0to_rad"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Fly2Recv[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      11,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   69,    2, 0x0a /* Public */,
       3,    0,   70,    2, 0x0a /* Public */,
       4,    0,   71,    2, 0x0a /* Public */,
       5,    0,   72,    2, 0x0a /* Public */,
       6,    0,   73,    2, 0x0a /* Public */,
       7,    0,   74,    2, 0x0a /* Public */,
       8,    0,   75,    2, 0x0a /* Public */,
       9,    8,   76,    2, 0x0a /* Public */,
      20,    1,   93,    2, 0x0a /* Public */,
      22,    0,   96,    2, 0x0a /* Public */,
      23,    1,   97,    2, 0x0a /* Public */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    0x80000000 | 10, 0x80000000 | 11, QMetaType::Float, QMetaType::Float, QMetaType::Float, QMetaType::Float, QMetaType::Float, 0x80000000 | 11, 0x80000000 | 11,   12,   13,   14,   15,   16,   17,   18,   19,
    QMetaType::Void, 0x80000000 | 11,   21,
    QMetaType::Void,
    QMetaType::Double, QMetaType::Double,    2,

       0        // eod
};

void Fly2Recv::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Fly2Recv *_t = static_cast<Fly2Recv *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->SetRingFormation(); break;
        case 1: _t->SetLineFormation(); break;
        case 2: _t->SetColumnFormation(); break;
        case 3: _t->UpdateZmqHost(); break;
        case 4: _t->SimulateDrones(); break;
        case 5: _t->UpdateUAVTable(); break;
        case 6: _t->clearTable(); break;
        case 7: { PosUAV _r = _t->CalculateFormationPositions((*reinterpret_cast< uint8_t(*)>(_a[1])),(*reinterpret_cast< float(*)>(_a[2])),(*reinterpret_cast< float(*)>(_a[3])),(*reinterpret_cast< float(*)>(_a[4])),(*reinterpret_cast< float(*)>(_a[5])),(*reinterpret_cast< float(*)>(_a[6])),(*reinterpret_cast< uint8_t(*)>(_a[7])),(*reinterpret_cast< uint8_t(*)>(_a[8])));
            if (_a[0]) *reinterpret_cast< PosUAV*>(_a[0]) = _r; }  break;
        case 8: _t->sendZMQ((*reinterpret_cast< uint8_t(*)>(_a[1]))); break;
        case 9: _t->PollZMQ(); break;
        case 10: { double _r = _t->to_rad((*reinterpret_cast< double(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< double*>(_a[0]) = _r; }  break;
        default: ;
        }
    }
}

const QMetaObject Fly2Recv::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_Fly2Recv.data,
      qt_meta_data_Fly2Recv,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *Fly2Recv::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Fly2Recv::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_Fly2Recv.stringdata0))
        return static_cast<void*>(const_cast< Fly2Recv*>(this));
    return QWidget::qt_metacast(_clname);
}

int Fly2Recv::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 11)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 11;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 11)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 11;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
