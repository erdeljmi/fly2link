/********************************************************************************
** Form generated from reading UI file 'fleetcontrol.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FLEETCONTROL_H
#define UI_FLEETCONTROL_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QTableWidget>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_FleetControl
{
public:
    QHBoxLayout *horizontalLayout;
    QVBoxLayout *verticalLayout_3;
    QGroupBox *groupBox_3;
    QHBoxLayout *horizontalLayout_7;
    QLabel *label_2;
    QSpinBox *numDrones;
    QPushButton *btnRingForm;
    QPushButton *btnColumnForm;
    QPushButton *btnLineForm;
    QGroupBox *groupBox_2;
    QVBoxLayout *verticalLayout_11;
    QTableWidget *tblUAVs;
    QVBoxLayout *verticalLayout;
    QGroupBox *groupBox;
    QVBoxLayout *verticalLayout_4;
    QHBoxLayout *horizontalLayout_2;
    QLineEdit *textZmqHost;
    QPushButton *btnUpdate;
    QGroupBox *groupBox_5;
    QVBoxLayout *verticalLayout_2;
    QTextEdit *textRecvCommands;

    void setupUi(QWidget *FleetControl)
    {
        if (FleetControl->objectName().isEmpty())
            FleetControl->setObjectName(QStringLiteral("FleetControl"));
        FleetControl->resize(1036, 478);
        horizontalLayout = new QHBoxLayout(FleetControl);
        horizontalLayout->setSpacing(6);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setSpacing(6);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        groupBox_3 = new QGroupBox(FleetControl);
        groupBox_3->setObjectName(QStringLiteral("groupBox_3"));
        horizontalLayout_7 = new QHBoxLayout(groupBox_3);
        horizontalLayout_7->setSpacing(6);
        horizontalLayout_7->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_7->setObjectName(QStringLiteral("horizontalLayout_7"));
        label_2 = new QLabel(groupBox_3);
        label_2->setObjectName(QStringLiteral("label_2"));

        horizontalLayout_7->addWidget(label_2);

        numDrones = new QSpinBox(groupBox_3);
        numDrones->setObjectName(QStringLiteral("numDrones"));
        numDrones->setMinimum(3);
        numDrones->setMaximum(10);
        numDrones->setValue(6);

        horizontalLayout_7->addWidget(numDrones);

        btnRingForm = new QPushButton(groupBox_3);
        btnRingForm->setObjectName(QStringLiteral("btnRingForm"));
        btnRingForm->setCheckable(true);
        btnRingForm->setChecked(true);
        btnRingForm->setAutoExclusive(true);

        horizontalLayout_7->addWidget(btnRingForm);

        btnColumnForm = new QPushButton(groupBox_3);
        btnColumnForm->setObjectName(QStringLiteral("btnColumnForm"));
        btnColumnForm->setCheckable(true);
        btnColumnForm->setAutoExclusive(true);

        horizontalLayout_7->addWidget(btnColumnForm);

        btnLineForm = new QPushButton(groupBox_3);
        btnLineForm->setObjectName(QStringLiteral("btnLineForm"));
        btnLineForm->setCheckable(true);
        btnLineForm->setAutoExclusive(true);

        horizontalLayout_7->addWidget(btnLineForm);


        verticalLayout_3->addWidget(groupBox_3);

        groupBox_2 = new QGroupBox(FleetControl);
        groupBox_2->setObjectName(QStringLiteral("groupBox_2"));
        verticalLayout_11 = new QVBoxLayout(groupBox_2);
        verticalLayout_11->setSpacing(6);
        verticalLayout_11->setContentsMargins(11, 11, 11, 11);
        verticalLayout_11->setObjectName(QStringLiteral("verticalLayout_11"));
        tblUAVs = new QTableWidget(groupBox_2);
        tblUAVs->setObjectName(QStringLiteral("tblUAVs"));

        verticalLayout_11->addWidget(tblUAVs);


        verticalLayout_3->addWidget(groupBox_2);


        horizontalLayout->addLayout(verticalLayout_3);

        verticalLayout = new QVBoxLayout();
        verticalLayout->setSpacing(6);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        groupBox = new QGroupBox(FleetControl);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        verticalLayout_4 = new QVBoxLayout(groupBox);
        verticalLayout_4->setSpacing(6);
        verticalLayout_4->setContentsMargins(11, 11, 11, 11);
        verticalLayout_4->setObjectName(QStringLiteral("verticalLayout_4"));
        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        textZmqHost = new QLineEdit(groupBox);
        textZmqHost->setObjectName(QStringLiteral("textZmqHost"));

        horizontalLayout_2->addWidget(textZmqHost);

        btnUpdate = new QPushButton(groupBox);
        btnUpdate->setObjectName(QStringLiteral("btnUpdate"));

        horizontalLayout_2->addWidget(btnUpdate);


        verticalLayout_4->addLayout(horizontalLayout_2);


        verticalLayout->addWidget(groupBox);

        groupBox_5 = new QGroupBox(FleetControl);
        groupBox_5->setObjectName(QStringLiteral("groupBox_5"));
        verticalLayout_2 = new QVBoxLayout(groupBox_5);
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        textRecvCommands = new QTextEdit(groupBox_5);
        textRecvCommands->setObjectName(QStringLiteral("textRecvCommands"));
        textRecvCommands->setReadOnly(true);

        verticalLayout_2->addWidget(textRecvCommands);


        verticalLayout->addWidget(groupBox_5);


        horizontalLayout->addLayout(verticalLayout);


        retranslateUi(FleetControl);

        QMetaObject::connectSlotsByName(FleetControl);
    } // setupUi

    void retranslateUi(QWidget *FleetControl)
    {
        FleetControl->setWindowTitle(QApplication::translate("FleetControl", "FleetGCS", 0));
        groupBox_3->setTitle(QApplication::translate("FleetControl", "Fleet sim", 0));
        label_2->setText(QApplication::translate("FleetControl", "Drones:", 0));
        btnRingForm->setText(QApplication::translate("FleetControl", "RING", 0));
        btnColumnForm->setText(QApplication::translate("FleetControl", "CO&LUMN", 0));
        btnLineForm->setText(QApplication::translate("FleetControl", "&LINE", 0));
        groupBox_2->setTitle(QApplication::translate("FleetControl", "Fleet status", 0));
        groupBox->setTitle(QApplication::translate("FleetControl", "ZeroMQ host", 0));
        textZmqHost->setText(QApplication::translate("FleetControl", "tcp://localhost:1337", 0));
        btnUpdate->setText(QApplication::translate("FleetControl", "Update", 0));
        groupBox_5->setTitle(QApplication::translate("FleetControl", "WebApp commands", 0));
    } // retranslateUi

};

namespace Ui {
    class FleetControl: public Ui_FleetControl {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FLEETCONTROL_H
