#-------------------------------------------------
#
# Project created by QtCreator 2016-07-21T23:20:55
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets printsupport

QMAKE_CXXFLAGS = -std=c++11
QMAKE_LFLAGS = -std=c++11

TARGET = fly2gcs
TEMPLATE = app

SOURCES += main.cpp\
    ZeroMQ.cpp \
    Fly2Recv.cpp

HEADERS  += \
    include/common/mavlink.h \
    include/flatbuffers/base.h \
    include/flatbuffers_generated.h \
    geodetic_conv.hpp \
    LockedQueue.h \
    CallbackTimer.h \
    ZeroMQ.h \
    Fly2Recv.h \
    InfoUAV.h

FORMS    += \
    fleetcontrol.ui

LIBS += \
    -lboost_system -lboost_thread -L/usr/local/lib -lzmq

INCLUDEPATH += \
    /usr/local/include
