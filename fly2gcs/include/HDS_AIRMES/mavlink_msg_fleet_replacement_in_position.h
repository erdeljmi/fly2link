#pragma once
// MESSAGE FLEET_REPLACEMENT_IN_POSITION PACKING

#define MAVLINK_MSG_ID_FLEET_REPLACEMENT_IN_POSITION 162

MAVPACKED(
typedef struct __mavlink_fleet_replacement_in_position_t {
 uint8_t request_ID; /*<  Request's unique ID*/
}) mavlink_fleet_replacement_in_position_t;

#define MAVLINK_MSG_ID_FLEET_REPLACEMENT_IN_POSITION_LEN 1
#define MAVLINK_MSG_ID_FLEET_REPLACEMENT_IN_POSITION_MIN_LEN 1
#define MAVLINK_MSG_ID_162_LEN 1
#define MAVLINK_MSG_ID_162_MIN_LEN 1

#define MAVLINK_MSG_ID_FLEET_REPLACEMENT_IN_POSITION_CRC 98
#define MAVLINK_MSG_ID_162_CRC 98



#if MAVLINK_COMMAND_24BIT
#define MAVLINK_MESSAGE_INFO_FLEET_REPLACEMENT_IN_POSITION { \
    162, \
    "FLEET_REPLACEMENT_IN_POSITION", \
    1, \
    {  { "request_ID", NULL, MAVLINK_TYPE_UINT8_T, 0, 0, offsetof(mavlink_fleet_replacement_in_position_t, request_ID) }, \
         } \
}
#else
#define MAVLINK_MESSAGE_INFO_FLEET_REPLACEMENT_IN_POSITION { \
    "FLEET_REPLACEMENT_IN_POSITION", \
    1, \
    {  { "request_ID", NULL, MAVLINK_TYPE_UINT8_T, 0, 0, offsetof(mavlink_fleet_replacement_in_position_t, request_ID) }, \
         } \
}
#endif

/**
 * @brief Pack a fleet_replacement_in_position message
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 *
 * @param request_ID  Request's unique ID
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_fleet_replacement_in_position_pack(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg,
                               uint8_t request_ID)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_FLEET_REPLACEMENT_IN_POSITION_LEN];
    _mav_put_uint8_t(buf, 0, request_ID);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_FLEET_REPLACEMENT_IN_POSITION_LEN);
#else
    mavlink_fleet_replacement_in_position_t packet;
    packet.request_ID = request_ID;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_FLEET_REPLACEMENT_IN_POSITION_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_FLEET_REPLACEMENT_IN_POSITION;
    return mavlink_finalize_message(msg, system_id, component_id, MAVLINK_MSG_ID_FLEET_REPLACEMENT_IN_POSITION_MIN_LEN, MAVLINK_MSG_ID_FLEET_REPLACEMENT_IN_POSITION_LEN, MAVLINK_MSG_ID_FLEET_REPLACEMENT_IN_POSITION_CRC);
}

/**
 * @brief Pack a fleet_replacement_in_position message on a channel
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param request_ID  Request's unique ID
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_fleet_replacement_in_position_pack_chan(uint8_t system_id, uint8_t component_id, uint8_t chan,
                               mavlink_message_t* msg,
                                   uint8_t request_ID)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_FLEET_REPLACEMENT_IN_POSITION_LEN];
    _mav_put_uint8_t(buf, 0, request_ID);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_FLEET_REPLACEMENT_IN_POSITION_LEN);
#else
    mavlink_fleet_replacement_in_position_t packet;
    packet.request_ID = request_ID;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_FLEET_REPLACEMENT_IN_POSITION_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_FLEET_REPLACEMENT_IN_POSITION;
    return mavlink_finalize_message_chan(msg, system_id, component_id, chan, MAVLINK_MSG_ID_FLEET_REPLACEMENT_IN_POSITION_MIN_LEN, MAVLINK_MSG_ID_FLEET_REPLACEMENT_IN_POSITION_LEN, MAVLINK_MSG_ID_FLEET_REPLACEMENT_IN_POSITION_CRC);
}

/**
 * @brief Encode a fleet_replacement_in_position struct
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 * @param fleet_replacement_in_position C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_fleet_replacement_in_position_encode(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg, const mavlink_fleet_replacement_in_position_t* fleet_replacement_in_position)
{
    return mavlink_msg_fleet_replacement_in_position_pack(system_id, component_id, msg, fleet_replacement_in_position->request_ID);
}

/**
 * @brief Encode a fleet_replacement_in_position struct on a channel
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param fleet_replacement_in_position C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_fleet_replacement_in_position_encode_chan(uint8_t system_id, uint8_t component_id, uint8_t chan, mavlink_message_t* msg, const mavlink_fleet_replacement_in_position_t* fleet_replacement_in_position)
{
    return mavlink_msg_fleet_replacement_in_position_pack_chan(system_id, component_id, chan, msg, fleet_replacement_in_position->request_ID);
}

/**
 * @brief Send a fleet_replacement_in_position message
 * @param chan MAVLink channel to send the message
 *
 * @param request_ID  Request's unique ID
 */
#ifdef MAVLINK_USE_CONVENIENCE_FUNCTIONS

static inline void mavlink_msg_fleet_replacement_in_position_send(mavlink_channel_t chan, uint8_t request_ID)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_FLEET_REPLACEMENT_IN_POSITION_LEN];
    _mav_put_uint8_t(buf, 0, request_ID);

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_FLEET_REPLACEMENT_IN_POSITION, buf, MAVLINK_MSG_ID_FLEET_REPLACEMENT_IN_POSITION_MIN_LEN, MAVLINK_MSG_ID_FLEET_REPLACEMENT_IN_POSITION_LEN, MAVLINK_MSG_ID_FLEET_REPLACEMENT_IN_POSITION_CRC);
#else
    mavlink_fleet_replacement_in_position_t packet;
    packet.request_ID = request_ID;

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_FLEET_REPLACEMENT_IN_POSITION, (const char *)&packet, MAVLINK_MSG_ID_FLEET_REPLACEMENT_IN_POSITION_MIN_LEN, MAVLINK_MSG_ID_FLEET_REPLACEMENT_IN_POSITION_LEN, MAVLINK_MSG_ID_FLEET_REPLACEMENT_IN_POSITION_CRC);
#endif
}

/**
 * @brief Send a fleet_replacement_in_position message
 * @param chan MAVLink channel to send the message
 * @param struct The MAVLink struct to serialize
 */
static inline void mavlink_msg_fleet_replacement_in_position_send_struct(mavlink_channel_t chan, const mavlink_fleet_replacement_in_position_t* fleet_replacement_in_position)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    mavlink_msg_fleet_replacement_in_position_send(chan, fleet_replacement_in_position->request_ID);
#else
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_FLEET_REPLACEMENT_IN_POSITION, (const char *)fleet_replacement_in_position, MAVLINK_MSG_ID_FLEET_REPLACEMENT_IN_POSITION_MIN_LEN, MAVLINK_MSG_ID_FLEET_REPLACEMENT_IN_POSITION_LEN, MAVLINK_MSG_ID_FLEET_REPLACEMENT_IN_POSITION_CRC);
#endif
}

#if MAVLINK_MSG_ID_FLEET_REPLACEMENT_IN_POSITION_LEN <= MAVLINK_MAX_PAYLOAD_LEN
/*
  This varient of _send() can be used to save stack space by re-using
  memory from the receive buffer.  The caller provides a
  mavlink_message_t which is the size of a full mavlink message. This
  is usually the receive buffer for the channel, and allows a reply to an
  incoming message with minimum stack space usage.
 */
static inline void mavlink_msg_fleet_replacement_in_position_send_buf(mavlink_message_t *msgbuf, mavlink_channel_t chan,  uint8_t request_ID)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char *buf = (char *)msgbuf;
    _mav_put_uint8_t(buf, 0, request_ID);

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_FLEET_REPLACEMENT_IN_POSITION, buf, MAVLINK_MSG_ID_FLEET_REPLACEMENT_IN_POSITION_MIN_LEN, MAVLINK_MSG_ID_FLEET_REPLACEMENT_IN_POSITION_LEN, MAVLINK_MSG_ID_FLEET_REPLACEMENT_IN_POSITION_CRC);
#else
    mavlink_fleet_replacement_in_position_t *packet = (mavlink_fleet_replacement_in_position_t *)msgbuf;
    packet->request_ID = request_ID;

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_FLEET_REPLACEMENT_IN_POSITION, (const char *)packet, MAVLINK_MSG_ID_FLEET_REPLACEMENT_IN_POSITION_MIN_LEN, MAVLINK_MSG_ID_FLEET_REPLACEMENT_IN_POSITION_LEN, MAVLINK_MSG_ID_FLEET_REPLACEMENT_IN_POSITION_CRC);
#endif
}
#endif

#endif

// MESSAGE FLEET_REPLACEMENT_IN_POSITION UNPACKING


/**
 * @brief Get field request_ID from fleet_replacement_in_position message
 *
 * @return  Request's unique ID
 */
static inline uint8_t mavlink_msg_fleet_replacement_in_position_get_request_ID(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint8_t(msg,  0);
}

/**
 * @brief Decode a fleet_replacement_in_position message into a struct
 *
 * @param msg The message to decode
 * @param fleet_replacement_in_position C-struct to decode the message contents into
 */
static inline void mavlink_msg_fleet_replacement_in_position_decode(const mavlink_message_t* msg, mavlink_fleet_replacement_in_position_t* fleet_replacement_in_position)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    fleet_replacement_in_position->request_ID = mavlink_msg_fleet_replacement_in_position_get_request_ID(msg);
#else
        uint8_t len = msg->len < MAVLINK_MSG_ID_FLEET_REPLACEMENT_IN_POSITION_LEN? msg->len : MAVLINK_MSG_ID_FLEET_REPLACEMENT_IN_POSITION_LEN;
        memset(fleet_replacement_in_position, 0, MAVLINK_MSG_ID_FLEET_REPLACEMENT_IN_POSITION_LEN);
    memcpy(fleet_replacement_in_position, _MAV_PAYLOAD(msg), len);
#endif
}
