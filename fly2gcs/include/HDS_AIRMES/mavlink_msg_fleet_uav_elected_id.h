#pragma once
// MESSAGE FLEET_UAV_ELECTED_ID PACKING

#define MAVLINK_MSG_ID_FLEET_UAV_ELECTED_ID 160

MAVPACKED(
typedef struct __mavlink_fleet_uav_elected_id_t {
 uint8_t request_ID; /*<  Request's unique ID*/
 uint8_t elected_ID; /*<  Elected UAV ID*/
}) mavlink_fleet_uav_elected_id_t;

#define MAVLINK_MSG_ID_FLEET_UAV_ELECTED_ID_LEN 2
#define MAVLINK_MSG_ID_FLEET_UAV_ELECTED_ID_MIN_LEN 2
#define MAVLINK_MSG_ID_160_LEN 2
#define MAVLINK_MSG_ID_160_MIN_LEN 2

#define MAVLINK_MSG_ID_FLEET_UAV_ELECTED_ID_CRC 65
#define MAVLINK_MSG_ID_160_CRC 65



#if MAVLINK_COMMAND_24BIT
#define MAVLINK_MESSAGE_INFO_FLEET_UAV_ELECTED_ID { \
    160, \
    "FLEET_UAV_ELECTED_ID", \
    2, \
    {  { "request_ID", NULL, MAVLINK_TYPE_UINT8_T, 0, 0, offsetof(mavlink_fleet_uav_elected_id_t, request_ID) }, \
         { "elected_ID", NULL, MAVLINK_TYPE_UINT8_T, 0, 1, offsetof(mavlink_fleet_uav_elected_id_t, elected_ID) }, \
         } \
}
#else
#define MAVLINK_MESSAGE_INFO_FLEET_UAV_ELECTED_ID { \
    "FLEET_UAV_ELECTED_ID", \
    2, \
    {  { "request_ID", NULL, MAVLINK_TYPE_UINT8_T, 0, 0, offsetof(mavlink_fleet_uav_elected_id_t, request_ID) }, \
         { "elected_ID", NULL, MAVLINK_TYPE_UINT8_T, 0, 1, offsetof(mavlink_fleet_uav_elected_id_t, elected_ID) }, \
         } \
}
#endif

/**
 * @brief Pack a fleet_uav_elected_id message
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 *
 * @param request_ID  Request's unique ID
 * @param elected_ID  Elected UAV ID
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_fleet_uav_elected_id_pack(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg,
                               uint8_t request_ID, uint8_t elected_ID)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_FLEET_UAV_ELECTED_ID_LEN];
    _mav_put_uint8_t(buf, 0, request_ID);
    _mav_put_uint8_t(buf, 1, elected_ID);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_FLEET_UAV_ELECTED_ID_LEN);
#else
    mavlink_fleet_uav_elected_id_t packet;
    packet.request_ID = request_ID;
    packet.elected_ID = elected_ID;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_FLEET_UAV_ELECTED_ID_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_FLEET_UAV_ELECTED_ID;
    return mavlink_finalize_message(msg, system_id, component_id, MAVLINK_MSG_ID_FLEET_UAV_ELECTED_ID_MIN_LEN, MAVLINK_MSG_ID_FLEET_UAV_ELECTED_ID_LEN, MAVLINK_MSG_ID_FLEET_UAV_ELECTED_ID_CRC);
}

/**
 * @brief Pack a fleet_uav_elected_id message on a channel
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param request_ID  Request's unique ID
 * @param elected_ID  Elected UAV ID
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_fleet_uav_elected_id_pack_chan(uint8_t system_id, uint8_t component_id, uint8_t chan,
                               mavlink_message_t* msg,
                                   uint8_t request_ID,uint8_t elected_ID)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_FLEET_UAV_ELECTED_ID_LEN];
    _mav_put_uint8_t(buf, 0, request_ID);
    _mav_put_uint8_t(buf, 1, elected_ID);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_FLEET_UAV_ELECTED_ID_LEN);
#else
    mavlink_fleet_uav_elected_id_t packet;
    packet.request_ID = request_ID;
    packet.elected_ID = elected_ID;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_FLEET_UAV_ELECTED_ID_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_FLEET_UAV_ELECTED_ID;
    return mavlink_finalize_message_chan(msg, system_id, component_id, chan, MAVLINK_MSG_ID_FLEET_UAV_ELECTED_ID_MIN_LEN, MAVLINK_MSG_ID_FLEET_UAV_ELECTED_ID_LEN, MAVLINK_MSG_ID_FLEET_UAV_ELECTED_ID_CRC);
}

/**
 * @brief Encode a fleet_uav_elected_id struct
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 * @param fleet_uav_elected_id C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_fleet_uav_elected_id_encode(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg, const mavlink_fleet_uav_elected_id_t* fleet_uav_elected_id)
{
    return mavlink_msg_fleet_uav_elected_id_pack(system_id, component_id, msg, fleet_uav_elected_id->request_ID, fleet_uav_elected_id->elected_ID);
}

/**
 * @brief Encode a fleet_uav_elected_id struct on a channel
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param fleet_uav_elected_id C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_fleet_uav_elected_id_encode_chan(uint8_t system_id, uint8_t component_id, uint8_t chan, mavlink_message_t* msg, const mavlink_fleet_uav_elected_id_t* fleet_uav_elected_id)
{
    return mavlink_msg_fleet_uav_elected_id_pack_chan(system_id, component_id, chan, msg, fleet_uav_elected_id->request_ID, fleet_uav_elected_id->elected_ID);
}

/**
 * @brief Send a fleet_uav_elected_id message
 * @param chan MAVLink channel to send the message
 *
 * @param request_ID  Request's unique ID
 * @param elected_ID  Elected UAV ID
 */
#ifdef MAVLINK_USE_CONVENIENCE_FUNCTIONS

static inline void mavlink_msg_fleet_uav_elected_id_send(mavlink_channel_t chan, uint8_t request_ID, uint8_t elected_ID)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_FLEET_UAV_ELECTED_ID_LEN];
    _mav_put_uint8_t(buf, 0, request_ID);
    _mav_put_uint8_t(buf, 1, elected_ID);

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_FLEET_UAV_ELECTED_ID, buf, MAVLINK_MSG_ID_FLEET_UAV_ELECTED_ID_MIN_LEN, MAVLINK_MSG_ID_FLEET_UAV_ELECTED_ID_LEN, MAVLINK_MSG_ID_FLEET_UAV_ELECTED_ID_CRC);
#else
    mavlink_fleet_uav_elected_id_t packet;
    packet.request_ID = request_ID;
    packet.elected_ID = elected_ID;

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_FLEET_UAV_ELECTED_ID, (const char *)&packet, MAVLINK_MSG_ID_FLEET_UAV_ELECTED_ID_MIN_LEN, MAVLINK_MSG_ID_FLEET_UAV_ELECTED_ID_LEN, MAVLINK_MSG_ID_FLEET_UAV_ELECTED_ID_CRC);
#endif
}

/**
 * @brief Send a fleet_uav_elected_id message
 * @param chan MAVLink channel to send the message
 * @param struct The MAVLink struct to serialize
 */
static inline void mavlink_msg_fleet_uav_elected_id_send_struct(mavlink_channel_t chan, const mavlink_fleet_uav_elected_id_t* fleet_uav_elected_id)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    mavlink_msg_fleet_uav_elected_id_send(chan, fleet_uav_elected_id->request_ID, fleet_uav_elected_id->elected_ID);
#else
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_FLEET_UAV_ELECTED_ID, (const char *)fleet_uav_elected_id, MAVLINK_MSG_ID_FLEET_UAV_ELECTED_ID_MIN_LEN, MAVLINK_MSG_ID_FLEET_UAV_ELECTED_ID_LEN, MAVLINK_MSG_ID_FLEET_UAV_ELECTED_ID_CRC);
#endif
}

#if MAVLINK_MSG_ID_FLEET_UAV_ELECTED_ID_LEN <= MAVLINK_MAX_PAYLOAD_LEN
/*
  This varient of _send() can be used to save stack space by re-using
  memory from the receive buffer.  The caller provides a
  mavlink_message_t which is the size of a full mavlink message. This
  is usually the receive buffer for the channel, and allows a reply to an
  incoming message with minimum stack space usage.
 */
static inline void mavlink_msg_fleet_uav_elected_id_send_buf(mavlink_message_t *msgbuf, mavlink_channel_t chan,  uint8_t request_ID, uint8_t elected_ID)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char *buf = (char *)msgbuf;
    _mav_put_uint8_t(buf, 0, request_ID);
    _mav_put_uint8_t(buf, 1, elected_ID);

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_FLEET_UAV_ELECTED_ID, buf, MAVLINK_MSG_ID_FLEET_UAV_ELECTED_ID_MIN_LEN, MAVLINK_MSG_ID_FLEET_UAV_ELECTED_ID_LEN, MAVLINK_MSG_ID_FLEET_UAV_ELECTED_ID_CRC);
#else
    mavlink_fleet_uav_elected_id_t *packet = (mavlink_fleet_uav_elected_id_t *)msgbuf;
    packet->request_ID = request_ID;
    packet->elected_ID = elected_ID;

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_FLEET_UAV_ELECTED_ID, (const char *)packet, MAVLINK_MSG_ID_FLEET_UAV_ELECTED_ID_MIN_LEN, MAVLINK_MSG_ID_FLEET_UAV_ELECTED_ID_LEN, MAVLINK_MSG_ID_FLEET_UAV_ELECTED_ID_CRC);
#endif
}
#endif

#endif

// MESSAGE FLEET_UAV_ELECTED_ID UNPACKING


/**
 * @brief Get field request_ID from fleet_uav_elected_id message
 *
 * @return  Request's unique ID
 */
static inline uint8_t mavlink_msg_fleet_uav_elected_id_get_request_ID(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint8_t(msg,  0);
}

/**
 * @brief Get field elected_ID from fleet_uav_elected_id message
 *
 * @return  Elected UAV ID
 */
static inline uint8_t mavlink_msg_fleet_uav_elected_id_get_elected_ID(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint8_t(msg,  1);
}

/**
 * @brief Decode a fleet_uav_elected_id message into a struct
 *
 * @param msg The message to decode
 * @param fleet_uav_elected_id C-struct to decode the message contents into
 */
static inline void mavlink_msg_fleet_uav_elected_id_decode(const mavlink_message_t* msg, mavlink_fleet_uav_elected_id_t* fleet_uav_elected_id)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    fleet_uav_elected_id->request_ID = mavlink_msg_fleet_uav_elected_id_get_request_ID(msg);
    fleet_uav_elected_id->elected_ID = mavlink_msg_fleet_uav_elected_id_get_elected_ID(msg);
#else
        uint8_t len = msg->len < MAVLINK_MSG_ID_FLEET_UAV_ELECTED_ID_LEN? msg->len : MAVLINK_MSG_ID_FLEET_UAV_ELECTED_ID_LEN;
        memset(fleet_uav_elected_id, 0, MAVLINK_MSG_ID_FLEET_UAV_ELECTED_ID_LEN);
    memcpy(fleet_uav_elected_id, _MAV_PAYLOAD(msg), len);
#endif
}
