#pragma once
// MESSAGE FLEET_UAV_INFO_GLOBAL PACKING

#define MAVLINK_MSG_ID_FLEET_UAV_INFO_GLOBAL 151

MAVPACKED(
typedef struct __mavlink_fleet_uav_info_global_t {
 uint32_t custom_mode; /*<  HDS AIRMES custom mode (consult ENUM_HANDOVER_CUSTOM_MODES)*/
 uint32_t latitude; /*<  Latitude GPS*/
 uint32_t longitude; /*<  Longitude GPS*/
 uint16_t voltage_battery; /*<  Battery voltage (in mV)*/
 uint16_t battery_remaining; /*<  Battery remaining percentage*/
 uint16_t drop_rate_comm; /*<  Communication drop rate*/
 uint16_t errors_drop; /*<  Communication errors*/
 uint16_t altitude; /*<  Altitude*/
 uint16_t relative_alt; /*<  Relative altitude*/
 int16_t heading; /*<  Heading*/
 uint8_t base_mode; /*<  MAVlink base mode*/
 uint8_t system_status; /*<  UAV system status*/
}) mavlink_fleet_uav_info_global_t;

#define MAVLINK_MSG_ID_FLEET_UAV_INFO_GLOBAL_LEN 28
#define MAVLINK_MSG_ID_FLEET_UAV_INFO_GLOBAL_MIN_LEN 28
#define MAVLINK_MSG_ID_151_LEN 28
#define MAVLINK_MSG_ID_151_MIN_LEN 28

#define MAVLINK_MSG_ID_FLEET_UAV_INFO_GLOBAL_CRC 199
#define MAVLINK_MSG_ID_151_CRC 199



#if MAVLINK_COMMAND_24BIT
#define MAVLINK_MESSAGE_INFO_FLEET_UAV_INFO_GLOBAL { \
    151, \
    "FLEET_UAV_INFO_GLOBAL", \
    12, \
    {  { "custom_mode", NULL, MAVLINK_TYPE_UINT32_T, 0, 0, offsetof(mavlink_fleet_uav_info_global_t, custom_mode) }, \
         { "latitude", NULL, MAVLINK_TYPE_UINT32_T, 0, 4, offsetof(mavlink_fleet_uav_info_global_t, latitude) }, \
         { "longitude", NULL, MAVLINK_TYPE_UINT32_T, 0, 8, offsetof(mavlink_fleet_uav_info_global_t, longitude) }, \
         { "voltage_battery", NULL, MAVLINK_TYPE_UINT16_T, 0, 12, offsetof(mavlink_fleet_uav_info_global_t, voltage_battery) }, \
         { "battery_remaining", NULL, MAVLINK_TYPE_UINT16_T, 0, 14, offsetof(mavlink_fleet_uav_info_global_t, battery_remaining) }, \
         { "drop_rate_comm", NULL, MAVLINK_TYPE_UINT16_T, 0, 16, offsetof(mavlink_fleet_uav_info_global_t, drop_rate_comm) }, \
         { "errors_drop", NULL, MAVLINK_TYPE_UINT16_T, 0, 18, offsetof(mavlink_fleet_uav_info_global_t, errors_drop) }, \
         { "altitude", NULL, MAVLINK_TYPE_UINT16_T, 0, 20, offsetof(mavlink_fleet_uav_info_global_t, altitude) }, \
         { "relative_alt", NULL, MAVLINK_TYPE_UINT16_T, 0, 22, offsetof(mavlink_fleet_uav_info_global_t, relative_alt) }, \
         { "heading", NULL, MAVLINK_TYPE_INT16_T, 0, 24, offsetof(mavlink_fleet_uav_info_global_t, heading) }, \
         { "base_mode", NULL, MAVLINK_TYPE_UINT8_T, 0, 26, offsetof(mavlink_fleet_uav_info_global_t, base_mode) }, \
         { "system_status", NULL, MAVLINK_TYPE_UINT8_T, 0, 27, offsetof(mavlink_fleet_uav_info_global_t, system_status) }, \
         } \
}
#else
#define MAVLINK_MESSAGE_INFO_FLEET_UAV_INFO_GLOBAL { \
    "FLEET_UAV_INFO_GLOBAL", \
    12, \
    {  { "custom_mode", NULL, MAVLINK_TYPE_UINT32_T, 0, 0, offsetof(mavlink_fleet_uav_info_global_t, custom_mode) }, \
         { "latitude", NULL, MAVLINK_TYPE_UINT32_T, 0, 4, offsetof(mavlink_fleet_uav_info_global_t, latitude) }, \
         { "longitude", NULL, MAVLINK_TYPE_UINT32_T, 0, 8, offsetof(mavlink_fleet_uav_info_global_t, longitude) }, \
         { "voltage_battery", NULL, MAVLINK_TYPE_UINT16_T, 0, 12, offsetof(mavlink_fleet_uav_info_global_t, voltage_battery) }, \
         { "battery_remaining", NULL, MAVLINK_TYPE_UINT16_T, 0, 14, offsetof(mavlink_fleet_uav_info_global_t, battery_remaining) }, \
         { "drop_rate_comm", NULL, MAVLINK_TYPE_UINT16_T, 0, 16, offsetof(mavlink_fleet_uav_info_global_t, drop_rate_comm) }, \
         { "errors_drop", NULL, MAVLINK_TYPE_UINT16_T, 0, 18, offsetof(mavlink_fleet_uav_info_global_t, errors_drop) }, \
         { "altitude", NULL, MAVLINK_TYPE_UINT16_T, 0, 20, offsetof(mavlink_fleet_uav_info_global_t, altitude) }, \
         { "relative_alt", NULL, MAVLINK_TYPE_UINT16_T, 0, 22, offsetof(mavlink_fleet_uav_info_global_t, relative_alt) }, \
         { "heading", NULL, MAVLINK_TYPE_INT16_T, 0, 24, offsetof(mavlink_fleet_uav_info_global_t, heading) }, \
         { "base_mode", NULL, MAVLINK_TYPE_UINT8_T, 0, 26, offsetof(mavlink_fleet_uav_info_global_t, base_mode) }, \
         { "system_status", NULL, MAVLINK_TYPE_UINT8_T, 0, 27, offsetof(mavlink_fleet_uav_info_global_t, system_status) }, \
         } \
}
#endif

/**
 * @brief Pack a fleet_uav_info_global message
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 *
 * @param base_mode  MAVlink base mode
 * @param custom_mode  HDS AIRMES custom mode (consult ENUM_HANDOVER_CUSTOM_MODES)
 * @param system_status  UAV system status
 * @param voltage_battery  Battery voltage (in mV)
 * @param battery_remaining  Battery remaining percentage
 * @param drop_rate_comm  Communication drop rate
 * @param errors_drop  Communication errors
 * @param latitude  Latitude GPS
 * @param longitude  Longitude GPS
 * @param altitude  Altitude
 * @param relative_alt  Relative altitude
 * @param heading  Heading
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_fleet_uav_info_global_pack(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg,
                               uint8_t base_mode, uint32_t custom_mode, uint8_t system_status, uint16_t voltage_battery, uint16_t battery_remaining, uint16_t drop_rate_comm, uint16_t errors_drop, uint32_t latitude, uint32_t longitude, uint16_t altitude, uint16_t relative_alt, int16_t heading)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_FLEET_UAV_INFO_GLOBAL_LEN];
    _mav_put_uint32_t(buf, 0, custom_mode);
    _mav_put_uint32_t(buf, 4, latitude);
    _mav_put_uint32_t(buf, 8, longitude);
    _mav_put_uint16_t(buf, 12, voltage_battery);
    _mav_put_uint16_t(buf, 14, battery_remaining);
    _mav_put_uint16_t(buf, 16, drop_rate_comm);
    _mav_put_uint16_t(buf, 18, errors_drop);
    _mav_put_uint16_t(buf, 20, altitude);
    _mav_put_uint16_t(buf, 22, relative_alt);
    _mav_put_int16_t(buf, 24, heading);
    _mav_put_uint8_t(buf, 26, base_mode);
    _mav_put_uint8_t(buf, 27, system_status);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_FLEET_UAV_INFO_GLOBAL_LEN);
#else
    mavlink_fleet_uav_info_global_t packet;
    packet.custom_mode = custom_mode;
    packet.latitude = latitude;
    packet.longitude = longitude;
    packet.voltage_battery = voltage_battery;
    packet.battery_remaining = battery_remaining;
    packet.drop_rate_comm = drop_rate_comm;
    packet.errors_drop = errors_drop;
    packet.altitude = altitude;
    packet.relative_alt = relative_alt;
    packet.heading = heading;
    packet.base_mode = base_mode;
    packet.system_status = system_status;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_FLEET_UAV_INFO_GLOBAL_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_FLEET_UAV_INFO_GLOBAL;
    return mavlink_finalize_message(msg, system_id, component_id, MAVLINK_MSG_ID_FLEET_UAV_INFO_GLOBAL_MIN_LEN, MAVLINK_MSG_ID_FLEET_UAV_INFO_GLOBAL_LEN, MAVLINK_MSG_ID_FLEET_UAV_INFO_GLOBAL_CRC);
}

/**
 * @brief Pack a fleet_uav_info_global message on a channel
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param base_mode  MAVlink base mode
 * @param custom_mode  HDS AIRMES custom mode (consult ENUM_HANDOVER_CUSTOM_MODES)
 * @param system_status  UAV system status
 * @param voltage_battery  Battery voltage (in mV)
 * @param battery_remaining  Battery remaining percentage
 * @param drop_rate_comm  Communication drop rate
 * @param errors_drop  Communication errors
 * @param latitude  Latitude GPS
 * @param longitude  Longitude GPS
 * @param altitude  Altitude
 * @param relative_alt  Relative altitude
 * @param heading  Heading
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_fleet_uav_info_global_pack_chan(uint8_t system_id, uint8_t component_id, uint8_t chan,
                               mavlink_message_t* msg,
                                   uint8_t base_mode,uint32_t custom_mode,uint8_t system_status,uint16_t voltage_battery,uint16_t battery_remaining,uint16_t drop_rate_comm,uint16_t errors_drop,uint32_t latitude,uint32_t longitude,uint16_t altitude,uint16_t relative_alt,int16_t heading)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_FLEET_UAV_INFO_GLOBAL_LEN];
    _mav_put_uint32_t(buf, 0, custom_mode);
    _mav_put_uint32_t(buf, 4, latitude);
    _mav_put_uint32_t(buf, 8, longitude);
    _mav_put_uint16_t(buf, 12, voltage_battery);
    _mav_put_uint16_t(buf, 14, battery_remaining);
    _mav_put_uint16_t(buf, 16, drop_rate_comm);
    _mav_put_uint16_t(buf, 18, errors_drop);
    _mav_put_uint16_t(buf, 20, altitude);
    _mav_put_uint16_t(buf, 22, relative_alt);
    _mav_put_int16_t(buf, 24, heading);
    _mav_put_uint8_t(buf, 26, base_mode);
    _mav_put_uint8_t(buf, 27, system_status);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_FLEET_UAV_INFO_GLOBAL_LEN);
#else
    mavlink_fleet_uav_info_global_t packet;
    packet.custom_mode = custom_mode;
    packet.latitude = latitude;
    packet.longitude = longitude;
    packet.voltage_battery = voltage_battery;
    packet.battery_remaining = battery_remaining;
    packet.drop_rate_comm = drop_rate_comm;
    packet.errors_drop = errors_drop;
    packet.altitude = altitude;
    packet.relative_alt = relative_alt;
    packet.heading = heading;
    packet.base_mode = base_mode;
    packet.system_status = system_status;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_FLEET_UAV_INFO_GLOBAL_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_FLEET_UAV_INFO_GLOBAL;
    return mavlink_finalize_message_chan(msg, system_id, component_id, chan, MAVLINK_MSG_ID_FLEET_UAV_INFO_GLOBAL_MIN_LEN, MAVLINK_MSG_ID_FLEET_UAV_INFO_GLOBAL_LEN, MAVLINK_MSG_ID_FLEET_UAV_INFO_GLOBAL_CRC);
}

/**
 * @brief Encode a fleet_uav_info_global struct
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 * @param fleet_uav_info_global C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_fleet_uav_info_global_encode(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg, const mavlink_fleet_uav_info_global_t* fleet_uav_info_global)
{
    return mavlink_msg_fleet_uav_info_global_pack(system_id, component_id, msg, fleet_uav_info_global->base_mode, fleet_uav_info_global->custom_mode, fleet_uav_info_global->system_status, fleet_uav_info_global->voltage_battery, fleet_uav_info_global->battery_remaining, fleet_uav_info_global->drop_rate_comm, fleet_uav_info_global->errors_drop, fleet_uav_info_global->latitude, fleet_uav_info_global->longitude, fleet_uav_info_global->altitude, fleet_uav_info_global->relative_alt, fleet_uav_info_global->heading);
}

/**
 * @brief Encode a fleet_uav_info_global struct on a channel
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param fleet_uav_info_global C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_fleet_uav_info_global_encode_chan(uint8_t system_id, uint8_t component_id, uint8_t chan, mavlink_message_t* msg, const mavlink_fleet_uav_info_global_t* fleet_uav_info_global)
{
    return mavlink_msg_fleet_uav_info_global_pack_chan(system_id, component_id, chan, msg, fleet_uav_info_global->base_mode, fleet_uav_info_global->custom_mode, fleet_uav_info_global->system_status, fleet_uav_info_global->voltage_battery, fleet_uav_info_global->battery_remaining, fleet_uav_info_global->drop_rate_comm, fleet_uav_info_global->errors_drop, fleet_uav_info_global->latitude, fleet_uav_info_global->longitude, fleet_uav_info_global->altitude, fleet_uav_info_global->relative_alt, fleet_uav_info_global->heading);
}

/**
 * @brief Send a fleet_uav_info_global message
 * @param chan MAVLink channel to send the message
 *
 * @param base_mode  MAVlink base mode
 * @param custom_mode  HDS AIRMES custom mode (consult ENUM_HANDOVER_CUSTOM_MODES)
 * @param system_status  UAV system status
 * @param voltage_battery  Battery voltage (in mV)
 * @param battery_remaining  Battery remaining percentage
 * @param drop_rate_comm  Communication drop rate
 * @param errors_drop  Communication errors
 * @param latitude  Latitude GPS
 * @param longitude  Longitude GPS
 * @param altitude  Altitude
 * @param relative_alt  Relative altitude
 * @param heading  Heading
 */
#ifdef MAVLINK_USE_CONVENIENCE_FUNCTIONS

static inline void mavlink_msg_fleet_uav_info_global_send(mavlink_channel_t chan, uint8_t base_mode, uint32_t custom_mode, uint8_t system_status, uint16_t voltage_battery, uint16_t battery_remaining, uint16_t drop_rate_comm, uint16_t errors_drop, uint32_t latitude, uint32_t longitude, uint16_t altitude, uint16_t relative_alt, int16_t heading)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_FLEET_UAV_INFO_GLOBAL_LEN];
    _mav_put_uint32_t(buf, 0, custom_mode);
    _mav_put_uint32_t(buf, 4, latitude);
    _mav_put_uint32_t(buf, 8, longitude);
    _mav_put_uint16_t(buf, 12, voltage_battery);
    _mav_put_uint16_t(buf, 14, battery_remaining);
    _mav_put_uint16_t(buf, 16, drop_rate_comm);
    _mav_put_uint16_t(buf, 18, errors_drop);
    _mav_put_uint16_t(buf, 20, altitude);
    _mav_put_uint16_t(buf, 22, relative_alt);
    _mav_put_int16_t(buf, 24, heading);
    _mav_put_uint8_t(buf, 26, base_mode);
    _mav_put_uint8_t(buf, 27, system_status);

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_FLEET_UAV_INFO_GLOBAL, buf, MAVLINK_MSG_ID_FLEET_UAV_INFO_GLOBAL_MIN_LEN, MAVLINK_MSG_ID_FLEET_UAV_INFO_GLOBAL_LEN, MAVLINK_MSG_ID_FLEET_UAV_INFO_GLOBAL_CRC);
#else
    mavlink_fleet_uav_info_global_t packet;
    packet.custom_mode = custom_mode;
    packet.latitude = latitude;
    packet.longitude = longitude;
    packet.voltage_battery = voltage_battery;
    packet.battery_remaining = battery_remaining;
    packet.drop_rate_comm = drop_rate_comm;
    packet.errors_drop = errors_drop;
    packet.altitude = altitude;
    packet.relative_alt = relative_alt;
    packet.heading = heading;
    packet.base_mode = base_mode;
    packet.system_status = system_status;

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_FLEET_UAV_INFO_GLOBAL, (const char *)&packet, MAVLINK_MSG_ID_FLEET_UAV_INFO_GLOBAL_MIN_LEN, MAVLINK_MSG_ID_FLEET_UAV_INFO_GLOBAL_LEN, MAVLINK_MSG_ID_FLEET_UAV_INFO_GLOBAL_CRC);
#endif
}

/**
 * @brief Send a fleet_uav_info_global message
 * @param chan MAVLink channel to send the message
 * @param struct The MAVLink struct to serialize
 */
static inline void mavlink_msg_fleet_uav_info_global_send_struct(mavlink_channel_t chan, const mavlink_fleet_uav_info_global_t* fleet_uav_info_global)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    mavlink_msg_fleet_uav_info_global_send(chan, fleet_uav_info_global->base_mode, fleet_uav_info_global->custom_mode, fleet_uav_info_global->system_status, fleet_uav_info_global->voltage_battery, fleet_uav_info_global->battery_remaining, fleet_uav_info_global->drop_rate_comm, fleet_uav_info_global->errors_drop, fleet_uav_info_global->latitude, fleet_uav_info_global->longitude, fleet_uav_info_global->altitude, fleet_uav_info_global->relative_alt, fleet_uav_info_global->heading);
#else
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_FLEET_UAV_INFO_GLOBAL, (const char *)fleet_uav_info_global, MAVLINK_MSG_ID_FLEET_UAV_INFO_GLOBAL_MIN_LEN, MAVLINK_MSG_ID_FLEET_UAV_INFO_GLOBAL_LEN, MAVLINK_MSG_ID_FLEET_UAV_INFO_GLOBAL_CRC);
#endif
}

#if MAVLINK_MSG_ID_FLEET_UAV_INFO_GLOBAL_LEN <= MAVLINK_MAX_PAYLOAD_LEN
/*
  This varient of _send() can be used to save stack space by re-using
  memory from the receive buffer.  The caller provides a
  mavlink_message_t which is the size of a full mavlink message. This
  is usually the receive buffer for the channel, and allows a reply to an
  incoming message with minimum stack space usage.
 */
static inline void mavlink_msg_fleet_uav_info_global_send_buf(mavlink_message_t *msgbuf, mavlink_channel_t chan,  uint8_t base_mode, uint32_t custom_mode, uint8_t system_status, uint16_t voltage_battery, uint16_t battery_remaining, uint16_t drop_rate_comm, uint16_t errors_drop, uint32_t latitude, uint32_t longitude, uint16_t altitude, uint16_t relative_alt, int16_t heading)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char *buf = (char *)msgbuf;
    _mav_put_uint32_t(buf, 0, custom_mode);
    _mav_put_uint32_t(buf, 4, latitude);
    _mav_put_uint32_t(buf, 8, longitude);
    _mav_put_uint16_t(buf, 12, voltage_battery);
    _mav_put_uint16_t(buf, 14, battery_remaining);
    _mav_put_uint16_t(buf, 16, drop_rate_comm);
    _mav_put_uint16_t(buf, 18, errors_drop);
    _mav_put_uint16_t(buf, 20, altitude);
    _mav_put_uint16_t(buf, 22, relative_alt);
    _mav_put_int16_t(buf, 24, heading);
    _mav_put_uint8_t(buf, 26, base_mode);
    _mav_put_uint8_t(buf, 27, system_status);

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_FLEET_UAV_INFO_GLOBAL, buf, MAVLINK_MSG_ID_FLEET_UAV_INFO_GLOBAL_MIN_LEN, MAVLINK_MSG_ID_FLEET_UAV_INFO_GLOBAL_LEN, MAVLINK_MSG_ID_FLEET_UAV_INFO_GLOBAL_CRC);
#else
    mavlink_fleet_uav_info_global_t *packet = (mavlink_fleet_uav_info_global_t *)msgbuf;
    packet->custom_mode = custom_mode;
    packet->latitude = latitude;
    packet->longitude = longitude;
    packet->voltage_battery = voltage_battery;
    packet->battery_remaining = battery_remaining;
    packet->drop_rate_comm = drop_rate_comm;
    packet->errors_drop = errors_drop;
    packet->altitude = altitude;
    packet->relative_alt = relative_alt;
    packet->heading = heading;
    packet->base_mode = base_mode;
    packet->system_status = system_status;

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_FLEET_UAV_INFO_GLOBAL, (const char *)packet, MAVLINK_MSG_ID_FLEET_UAV_INFO_GLOBAL_MIN_LEN, MAVLINK_MSG_ID_FLEET_UAV_INFO_GLOBAL_LEN, MAVLINK_MSG_ID_FLEET_UAV_INFO_GLOBAL_CRC);
#endif
}
#endif

#endif

// MESSAGE FLEET_UAV_INFO_GLOBAL UNPACKING


/**
 * @brief Get field base_mode from fleet_uav_info_global message
 *
 * @return  MAVlink base mode
 */
static inline uint8_t mavlink_msg_fleet_uav_info_global_get_base_mode(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint8_t(msg,  26);
}

/**
 * @brief Get field custom_mode from fleet_uav_info_global message
 *
 * @return  HDS AIRMES custom mode (consult ENUM_HANDOVER_CUSTOM_MODES)
 */
static inline uint32_t mavlink_msg_fleet_uav_info_global_get_custom_mode(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint32_t(msg,  0);
}

/**
 * @brief Get field system_status from fleet_uav_info_global message
 *
 * @return  UAV system status
 */
static inline uint8_t mavlink_msg_fleet_uav_info_global_get_system_status(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint8_t(msg,  27);
}

/**
 * @brief Get field voltage_battery from fleet_uav_info_global message
 *
 * @return  Battery voltage (in mV)
 */
static inline uint16_t mavlink_msg_fleet_uav_info_global_get_voltage_battery(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint16_t(msg,  12);
}

/**
 * @brief Get field battery_remaining from fleet_uav_info_global message
 *
 * @return  Battery remaining percentage
 */
static inline uint16_t mavlink_msg_fleet_uav_info_global_get_battery_remaining(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint16_t(msg,  14);
}

/**
 * @brief Get field drop_rate_comm from fleet_uav_info_global message
 *
 * @return  Communication drop rate
 */
static inline uint16_t mavlink_msg_fleet_uav_info_global_get_drop_rate_comm(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint16_t(msg,  16);
}

/**
 * @brief Get field errors_drop from fleet_uav_info_global message
 *
 * @return  Communication errors
 */
static inline uint16_t mavlink_msg_fleet_uav_info_global_get_errors_drop(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint16_t(msg,  18);
}

/**
 * @brief Get field latitude from fleet_uav_info_global message
 *
 * @return  Latitude GPS
 */
static inline uint32_t mavlink_msg_fleet_uav_info_global_get_latitude(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint32_t(msg,  4);
}

/**
 * @brief Get field longitude from fleet_uav_info_global message
 *
 * @return  Longitude GPS
 */
static inline uint32_t mavlink_msg_fleet_uav_info_global_get_longitude(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint32_t(msg,  8);
}

/**
 * @brief Get field altitude from fleet_uav_info_global message
 *
 * @return  Altitude
 */
static inline uint16_t mavlink_msg_fleet_uav_info_global_get_altitude(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint16_t(msg,  20);
}

/**
 * @brief Get field relative_alt from fleet_uav_info_global message
 *
 * @return  Relative altitude
 */
static inline uint16_t mavlink_msg_fleet_uav_info_global_get_relative_alt(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint16_t(msg,  22);
}

/**
 * @brief Get field heading from fleet_uav_info_global message
 *
 * @return  Heading
 */
static inline int16_t mavlink_msg_fleet_uav_info_global_get_heading(const mavlink_message_t* msg)
{
    return _MAV_RETURN_int16_t(msg,  24);
}

/**
 * @brief Decode a fleet_uav_info_global message into a struct
 *
 * @param msg The message to decode
 * @param fleet_uav_info_global C-struct to decode the message contents into
 */
static inline void mavlink_msg_fleet_uav_info_global_decode(const mavlink_message_t* msg, mavlink_fleet_uav_info_global_t* fleet_uav_info_global)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    fleet_uav_info_global->custom_mode = mavlink_msg_fleet_uav_info_global_get_custom_mode(msg);
    fleet_uav_info_global->latitude = mavlink_msg_fleet_uav_info_global_get_latitude(msg);
    fleet_uav_info_global->longitude = mavlink_msg_fleet_uav_info_global_get_longitude(msg);
    fleet_uav_info_global->voltage_battery = mavlink_msg_fleet_uav_info_global_get_voltage_battery(msg);
    fleet_uav_info_global->battery_remaining = mavlink_msg_fleet_uav_info_global_get_battery_remaining(msg);
    fleet_uav_info_global->drop_rate_comm = mavlink_msg_fleet_uav_info_global_get_drop_rate_comm(msg);
    fleet_uav_info_global->errors_drop = mavlink_msg_fleet_uav_info_global_get_errors_drop(msg);
    fleet_uav_info_global->altitude = mavlink_msg_fleet_uav_info_global_get_altitude(msg);
    fleet_uav_info_global->relative_alt = mavlink_msg_fleet_uav_info_global_get_relative_alt(msg);
    fleet_uav_info_global->heading = mavlink_msg_fleet_uav_info_global_get_heading(msg);
    fleet_uav_info_global->base_mode = mavlink_msg_fleet_uav_info_global_get_base_mode(msg);
    fleet_uav_info_global->system_status = mavlink_msg_fleet_uav_info_global_get_system_status(msg);
#else
        uint8_t len = msg->len < MAVLINK_MSG_ID_FLEET_UAV_INFO_GLOBAL_LEN? msg->len : MAVLINK_MSG_ID_FLEET_UAV_INFO_GLOBAL_LEN;
        memset(fleet_uav_info_global, 0, MAVLINK_MSG_ID_FLEET_UAV_INFO_GLOBAL_LEN);
    memcpy(fleet_uav_info_global, _MAV_PAYLOAD(msg), len);
#endif
}
