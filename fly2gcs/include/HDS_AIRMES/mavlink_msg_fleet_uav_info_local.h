#pragma once
// MESSAGE FLEET_UAV_INFO_LOCAL PACKING

#define MAVLINK_MSG_ID_FLEET_UAV_INFO_LOCAL 150

MAVPACKED(
typedef struct __mavlink_fleet_uav_info_local_t {
 uint32_t custom_mode; /*<  HDS AIRMES custom mode (consult ENUM_HANDOVER_CUSTOM_MODES)*/
 float x; /*<  Coordinate X*/
 float y; /*<  Coordinate Y*/
 float z; /*<  Coordinate Z*/
 float heading; /*<  Heading*/
 uint16_t voltage_battery; /*<  Battery voltage (in mV)*/
 uint16_t battery_remaining; /*<  Battery remaining percentage*/
 uint16_t drop_rate_comm; /*<  Communication drop rate*/
 uint16_t errors_drop; /*<  Communication errors*/
 uint8_t base_mode; /*<  MAVlink base mode*/
 uint8_t system_status; /*<  UAV system status*/
}) mavlink_fleet_uav_info_local_t;

#define MAVLINK_MSG_ID_FLEET_UAV_INFO_LOCAL_LEN 30
#define MAVLINK_MSG_ID_FLEET_UAV_INFO_LOCAL_MIN_LEN 30
#define MAVLINK_MSG_ID_150_LEN 30
#define MAVLINK_MSG_ID_150_MIN_LEN 30

#define MAVLINK_MSG_ID_FLEET_UAV_INFO_LOCAL_CRC 194
#define MAVLINK_MSG_ID_150_CRC 194



#if MAVLINK_COMMAND_24BIT
#define MAVLINK_MESSAGE_INFO_FLEET_UAV_INFO_LOCAL { \
    150, \
    "FLEET_UAV_INFO_LOCAL", \
    11, \
    {  { "custom_mode", NULL, MAVLINK_TYPE_UINT32_T, 0, 0, offsetof(mavlink_fleet_uav_info_local_t, custom_mode) }, \
         { "x", NULL, MAVLINK_TYPE_FLOAT, 0, 4, offsetof(mavlink_fleet_uav_info_local_t, x) }, \
         { "y", NULL, MAVLINK_TYPE_FLOAT, 0, 8, offsetof(mavlink_fleet_uav_info_local_t, y) }, \
         { "z", NULL, MAVLINK_TYPE_FLOAT, 0, 12, offsetof(mavlink_fleet_uav_info_local_t, z) }, \
         { "heading", NULL, MAVLINK_TYPE_FLOAT, 0, 16, offsetof(mavlink_fleet_uav_info_local_t, heading) }, \
         { "voltage_battery", NULL, MAVLINK_TYPE_UINT16_T, 0, 20, offsetof(mavlink_fleet_uav_info_local_t, voltage_battery) }, \
         { "battery_remaining", NULL, MAVLINK_TYPE_UINT16_T, 0, 22, offsetof(mavlink_fleet_uav_info_local_t, battery_remaining) }, \
         { "drop_rate_comm", NULL, MAVLINK_TYPE_UINT16_T, 0, 24, offsetof(mavlink_fleet_uav_info_local_t, drop_rate_comm) }, \
         { "errors_drop", NULL, MAVLINK_TYPE_UINT16_T, 0, 26, offsetof(mavlink_fleet_uav_info_local_t, errors_drop) }, \
         { "base_mode", NULL, MAVLINK_TYPE_UINT8_T, 0, 28, offsetof(mavlink_fleet_uav_info_local_t, base_mode) }, \
         { "system_status", NULL, MAVLINK_TYPE_UINT8_T, 0, 29, offsetof(mavlink_fleet_uav_info_local_t, system_status) }, \
         } \
}
#else
#define MAVLINK_MESSAGE_INFO_FLEET_UAV_INFO_LOCAL { \
    "FLEET_UAV_INFO_LOCAL", \
    11, \
    {  { "custom_mode", NULL, MAVLINK_TYPE_UINT32_T, 0, 0, offsetof(mavlink_fleet_uav_info_local_t, custom_mode) }, \
         { "x", NULL, MAVLINK_TYPE_FLOAT, 0, 4, offsetof(mavlink_fleet_uav_info_local_t, x) }, \
         { "y", NULL, MAVLINK_TYPE_FLOAT, 0, 8, offsetof(mavlink_fleet_uav_info_local_t, y) }, \
         { "z", NULL, MAVLINK_TYPE_FLOAT, 0, 12, offsetof(mavlink_fleet_uav_info_local_t, z) }, \
         { "heading", NULL, MAVLINK_TYPE_FLOAT, 0, 16, offsetof(mavlink_fleet_uav_info_local_t, heading) }, \
         { "voltage_battery", NULL, MAVLINK_TYPE_UINT16_T, 0, 20, offsetof(mavlink_fleet_uav_info_local_t, voltage_battery) }, \
         { "battery_remaining", NULL, MAVLINK_TYPE_UINT16_T, 0, 22, offsetof(mavlink_fleet_uav_info_local_t, battery_remaining) }, \
         { "drop_rate_comm", NULL, MAVLINK_TYPE_UINT16_T, 0, 24, offsetof(mavlink_fleet_uav_info_local_t, drop_rate_comm) }, \
         { "errors_drop", NULL, MAVLINK_TYPE_UINT16_T, 0, 26, offsetof(mavlink_fleet_uav_info_local_t, errors_drop) }, \
         { "base_mode", NULL, MAVLINK_TYPE_UINT8_T, 0, 28, offsetof(mavlink_fleet_uav_info_local_t, base_mode) }, \
         { "system_status", NULL, MAVLINK_TYPE_UINT8_T, 0, 29, offsetof(mavlink_fleet_uav_info_local_t, system_status) }, \
         } \
}
#endif

/**
 * @brief Pack a fleet_uav_info_local message
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 *
 * @param base_mode  MAVlink base mode
 * @param custom_mode  HDS AIRMES custom mode (consult ENUM_HANDOVER_CUSTOM_MODES)
 * @param system_status  UAV system status
 * @param voltage_battery  Battery voltage (in mV)
 * @param battery_remaining  Battery remaining percentage
 * @param drop_rate_comm  Communication drop rate
 * @param errors_drop  Communication errors
 * @param x  Coordinate X
 * @param y  Coordinate Y
 * @param z  Coordinate Z
 * @param heading  Heading
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_fleet_uav_info_local_pack(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg,
                               uint8_t base_mode, uint32_t custom_mode, uint8_t system_status, uint16_t voltage_battery, uint16_t battery_remaining, uint16_t drop_rate_comm, uint16_t errors_drop, float x, float y, float z, float heading)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_FLEET_UAV_INFO_LOCAL_LEN];
    _mav_put_uint32_t(buf, 0, custom_mode);
    _mav_put_float(buf, 4, x);
    _mav_put_float(buf, 8, y);
    _mav_put_float(buf, 12, z);
    _mav_put_float(buf, 16, heading);
    _mav_put_uint16_t(buf, 20, voltage_battery);
    _mav_put_uint16_t(buf, 22, battery_remaining);
    _mav_put_uint16_t(buf, 24, drop_rate_comm);
    _mav_put_uint16_t(buf, 26, errors_drop);
    _mav_put_uint8_t(buf, 28, base_mode);
    _mav_put_uint8_t(buf, 29, system_status);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_FLEET_UAV_INFO_LOCAL_LEN);
#else
    mavlink_fleet_uav_info_local_t packet;
    packet.custom_mode = custom_mode;
    packet.x = x;
    packet.y = y;
    packet.z = z;
    packet.heading = heading;
    packet.voltage_battery = voltage_battery;
    packet.battery_remaining = battery_remaining;
    packet.drop_rate_comm = drop_rate_comm;
    packet.errors_drop = errors_drop;
    packet.base_mode = base_mode;
    packet.system_status = system_status;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_FLEET_UAV_INFO_LOCAL_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_FLEET_UAV_INFO_LOCAL;
    return mavlink_finalize_message(msg, system_id, component_id, MAVLINK_MSG_ID_FLEET_UAV_INFO_LOCAL_MIN_LEN, MAVLINK_MSG_ID_FLEET_UAV_INFO_LOCAL_LEN, MAVLINK_MSG_ID_FLEET_UAV_INFO_LOCAL_CRC);
}

/**
 * @brief Pack a fleet_uav_info_local message on a channel
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param base_mode  MAVlink base mode
 * @param custom_mode  HDS AIRMES custom mode (consult ENUM_HANDOVER_CUSTOM_MODES)
 * @param system_status  UAV system status
 * @param voltage_battery  Battery voltage (in mV)
 * @param battery_remaining  Battery remaining percentage
 * @param drop_rate_comm  Communication drop rate
 * @param errors_drop  Communication errors
 * @param x  Coordinate X
 * @param y  Coordinate Y
 * @param z  Coordinate Z
 * @param heading  Heading
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_fleet_uav_info_local_pack_chan(uint8_t system_id, uint8_t component_id, uint8_t chan,
                               mavlink_message_t* msg,
                                   uint8_t base_mode,uint32_t custom_mode,uint8_t system_status,uint16_t voltage_battery,uint16_t battery_remaining,uint16_t drop_rate_comm,uint16_t errors_drop,float x,float y,float z,float heading)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_FLEET_UAV_INFO_LOCAL_LEN];
    _mav_put_uint32_t(buf, 0, custom_mode);
    _mav_put_float(buf, 4, x);
    _mav_put_float(buf, 8, y);
    _mav_put_float(buf, 12, z);
    _mav_put_float(buf, 16, heading);
    _mav_put_uint16_t(buf, 20, voltage_battery);
    _mav_put_uint16_t(buf, 22, battery_remaining);
    _mav_put_uint16_t(buf, 24, drop_rate_comm);
    _mav_put_uint16_t(buf, 26, errors_drop);
    _mav_put_uint8_t(buf, 28, base_mode);
    _mav_put_uint8_t(buf, 29, system_status);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_FLEET_UAV_INFO_LOCAL_LEN);
#else
    mavlink_fleet_uav_info_local_t packet;
    packet.custom_mode = custom_mode;
    packet.x = x;
    packet.y = y;
    packet.z = z;
    packet.heading = heading;
    packet.voltage_battery = voltage_battery;
    packet.battery_remaining = battery_remaining;
    packet.drop_rate_comm = drop_rate_comm;
    packet.errors_drop = errors_drop;
    packet.base_mode = base_mode;
    packet.system_status = system_status;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_FLEET_UAV_INFO_LOCAL_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_FLEET_UAV_INFO_LOCAL;
    return mavlink_finalize_message_chan(msg, system_id, component_id, chan, MAVLINK_MSG_ID_FLEET_UAV_INFO_LOCAL_MIN_LEN, MAVLINK_MSG_ID_FLEET_UAV_INFO_LOCAL_LEN, MAVLINK_MSG_ID_FLEET_UAV_INFO_LOCAL_CRC);
}

/**
 * @brief Encode a fleet_uav_info_local struct
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 * @param fleet_uav_info_local C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_fleet_uav_info_local_encode(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg, const mavlink_fleet_uav_info_local_t* fleet_uav_info_local)
{
    return mavlink_msg_fleet_uav_info_local_pack(system_id, component_id, msg, fleet_uav_info_local->base_mode, fleet_uav_info_local->custom_mode, fleet_uav_info_local->system_status, fleet_uav_info_local->voltage_battery, fleet_uav_info_local->battery_remaining, fleet_uav_info_local->drop_rate_comm, fleet_uav_info_local->errors_drop, fleet_uav_info_local->x, fleet_uav_info_local->y, fleet_uav_info_local->z, fleet_uav_info_local->heading);
}

/**
 * @brief Encode a fleet_uav_info_local struct on a channel
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param fleet_uav_info_local C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_fleet_uav_info_local_encode_chan(uint8_t system_id, uint8_t component_id, uint8_t chan, mavlink_message_t* msg, const mavlink_fleet_uav_info_local_t* fleet_uav_info_local)
{
    return mavlink_msg_fleet_uav_info_local_pack_chan(system_id, component_id, chan, msg, fleet_uav_info_local->base_mode, fleet_uav_info_local->custom_mode, fleet_uav_info_local->system_status, fleet_uav_info_local->voltage_battery, fleet_uav_info_local->battery_remaining, fleet_uav_info_local->drop_rate_comm, fleet_uav_info_local->errors_drop, fleet_uav_info_local->x, fleet_uav_info_local->y, fleet_uav_info_local->z, fleet_uav_info_local->heading);
}

/**
 * @brief Send a fleet_uav_info_local message
 * @param chan MAVLink channel to send the message
 *
 * @param base_mode  MAVlink base mode
 * @param custom_mode  HDS AIRMES custom mode (consult ENUM_HANDOVER_CUSTOM_MODES)
 * @param system_status  UAV system status
 * @param voltage_battery  Battery voltage (in mV)
 * @param battery_remaining  Battery remaining percentage
 * @param drop_rate_comm  Communication drop rate
 * @param errors_drop  Communication errors
 * @param x  Coordinate X
 * @param y  Coordinate Y
 * @param z  Coordinate Z
 * @param heading  Heading
 */
#ifdef MAVLINK_USE_CONVENIENCE_FUNCTIONS

static inline void mavlink_msg_fleet_uav_info_local_send(mavlink_channel_t chan, uint8_t base_mode, uint32_t custom_mode, uint8_t system_status, uint16_t voltage_battery, uint16_t battery_remaining, uint16_t drop_rate_comm, uint16_t errors_drop, float x, float y, float z, float heading)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_FLEET_UAV_INFO_LOCAL_LEN];
    _mav_put_uint32_t(buf, 0, custom_mode);
    _mav_put_float(buf, 4, x);
    _mav_put_float(buf, 8, y);
    _mav_put_float(buf, 12, z);
    _mav_put_float(buf, 16, heading);
    _mav_put_uint16_t(buf, 20, voltage_battery);
    _mav_put_uint16_t(buf, 22, battery_remaining);
    _mav_put_uint16_t(buf, 24, drop_rate_comm);
    _mav_put_uint16_t(buf, 26, errors_drop);
    _mav_put_uint8_t(buf, 28, base_mode);
    _mav_put_uint8_t(buf, 29, system_status);

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_FLEET_UAV_INFO_LOCAL, buf, MAVLINK_MSG_ID_FLEET_UAV_INFO_LOCAL_MIN_LEN, MAVLINK_MSG_ID_FLEET_UAV_INFO_LOCAL_LEN, MAVLINK_MSG_ID_FLEET_UAV_INFO_LOCAL_CRC);
#else
    mavlink_fleet_uav_info_local_t packet;
    packet.custom_mode = custom_mode;
    packet.x = x;
    packet.y = y;
    packet.z = z;
    packet.heading = heading;
    packet.voltage_battery = voltage_battery;
    packet.battery_remaining = battery_remaining;
    packet.drop_rate_comm = drop_rate_comm;
    packet.errors_drop = errors_drop;
    packet.base_mode = base_mode;
    packet.system_status = system_status;

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_FLEET_UAV_INFO_LOCAL, (const char *)&packet, MAVLINK_MSG_ID_FLEET_UAV_INFO_LOCAL_MIN_LEN, MAVLINK_MSG_ID_FLEET_UAV_INFO_LOCAL_LEN, MAVLINK_MSG_ID_FLEET_UAV_INFO_LOCAL_CRC);
#endif
}

/**
 * @brief Send a fleet_uav_info_local message
 * @param chan MAVLink channel to send the message
 * @param struct The MAVLink struct to serialize
 */
static inline void mavlink_msg_fleet_uav_info_local_send_struct(mavlink_channel_t chan, const mavlink_fleet_uav_info_local_t* fleet_uav_info_local)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    mavlink_msg_fleet_uav_info_local_send(chan, fleet_uav_info_local->base_mode, fleet_uav_info_local->custom_mode, fleet_uav_info_local->system_status, fleet_uav_info_local->voltage_battery, fleet_uav_info_local->battery_remaining, fleet_uav_info_local->drop_rate_comm, fleet_uav_info_local->errors_drop, fleet_uav_info_local->x, fleet_uav_info_local->y, fleet_uav_info_local->z, fleet_uav_info_local->heading);
#else
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_FLEET_UAV_INFO_LOCAL, (const char *)fleet_uav_info_local, MAVLINK_MSG_ID_FLEET_UAV_INFO_LOCAL_MIN_LEN, MAVLINK_MSG_ID_FLEET_UAV_INFO_LOCAL_LEN, MAVLINK_MSG_ID_FLEET_UAV_INFO_LOCAL_CRC);
#endif
}

#if MAVLINK_MSG_ID_FLEET_UAV_INFO_LOCAL_LEN <= MAVLINK_MAX_PAYLOAD_LEN
/*
  This varient of _send() can be used to save stack space by re-using
  memory from the receive buffer.  The caller provides a
  mavlink_message_t which is the size of a full mavlink message. This
  is usually the receive buffer for the channel, and allows a reply to an
  incoming message with minimum stack space usage.
 */
static inline void mavlink_msg_fleet_uav_info_local_send_buf(mavlink_message_t *msgbuf, mavlink_channel_t chan,  uint8_t base_mode, uint32_t custom_mode, uint8_t system_status, uint16_t voltage_battery, uint16_t battery_remaining, uint16_t drop_rate_comm, uint16_t errors_drop, float x, float y, float z, float heading)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char *buf = (char *)msgbuf;
    _mav_put_uint32_t(buf, 0, custom_mode);
    _mav_put_float(buf, 4, x);
    _mav_put_float(buf, 8, y);
    _mav_put_float(buf, 12, z);
    _mav_put_float(buf, 16, heading);
    _mav_put_uint16_t(buf, 20, voltage_battery);
    _mav_put_uint16_t(buf, 22, battery_remaining);
    _mav_put_uint16_t(buf, 24, drop_rate_comm);
    _mav_put_uint16_t(buf, 26, errors_drop);
    _mav_put_uint8_t(buf, 28, base_mode);
    _mav_put_uint8_t(buf, 29, system_status);

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_FLEET_UAV_INFO_LOCAL, buf, MAVLINK_MSG_ID_FLEET_UAV_INFO_LOCAL_MIN_LEN, MAVLINK_MSG_ID_FLEET_UAV_INFO_LOCAL_LEN, MAVLINK_MSG_ID_FLEET_UAV_INFO_LOCAL_CRC);
#else
    mavlink_fleet_uav_info_local_t *packet = (mavlink_fleet_uav_info_local_t *)msgbuf;
    packet->custom_mode = custom_mode;
    packet->x = x;
    packet->y = y;
    packet->z = z;
    packet->heading = heading;
    packet->voltage_battery = voltage_battery;
    packet->battery_remaining = battery_remaining;
    packet->drop_rate_comm = drop_rate_comm;
    packet->errors_drop = errors_drop;
    packet->base_mode = base_mode;
    packet->system_status = system_status;

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_FLEET_UAV_INFO_LOCAL, (const char *)packet, MAVLINK_MSG_ID_FLEET_UAV_INFO_LOCAL_MIN_LEN, MAVLINK_MSG_ID_FLEET_UAV_INFO_LOCAL_LEN, MAVLINK_MSG_ID_FLEET_UAV_INFO_LOCAL_CRC);
#endif
}
#endif

#endif

// MESSAGE FLEET_UAV_INFO_LOCAL UNPACKING


/**
 * @brief Get field base_mode from fleet_uav_info_local message
 *
 * @return  MAVlink base mode
 */
static inline uint8_t mavlink_msg_fleet_uav_info_local_get_base_mode(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint8_t(msg,  28);
}

/**
 * @brief Get field custom_mode from fleet_uav_info_local message
 *
 * @return  HDS AIRMES custom mode (consult ENUM_HANDOVER_CUSTOM_MODES)
 */
static inline uint32_t mavlink_msg_fleet_uav_info_local_get_custom_mode(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint32_t(msg,  0);
}

/**
 * @brief Get field system_status from fleet_uav_info_local message
 *
 * @return  UAV system status
 */
static inline uint8_t mavlink_msg_fleet_uav_info_local_get_system_status(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint8_t(msg,  29);
}

/**
 * @brief Get field voltage_battery from fleet_uav_info_local message
 *
 * @return  Battery voltage (in mV)
 */
static inline uint16_t mavlink_msg_fleet_uav_info_local_get_voltage_battery(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint16_t(msg,  20);
}

/**
 * @brief Get field battery_remaining from fleet_uav_info_local message
 *
 * @return  Battery remaining percentage
 */
static inline uint16_t mavlink_msg_fleet_uav_info_local_get_battery_remaining(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint16_t(msg,  22);
}

/**
 * @brief Get field drop_rate_comm from fleet_uav_info_local message
 *
 * @return  Communication drop rate
 */
static inline uint16_t mavlink_msg_fleet_uav_info_local_get_drop_rate_comm(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint16_t(msg,  24);
}

/**
 * @brief Get field errors_drop from fleet_uav_info_local message
 *
 * @return  Communication errors
 */
static inline uint16_t mavlink_msg_fleet_uav_info_local_get_errors_drop(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint16_t(msg,  26);
}

/**
 * @brief Get field x from fleet_uav_info_local message
 *
 * @return  Coordinate X
 */
static inline float mavlink_msg_fleet_uav_info_local_get_x(const mavlink_message_t* msg)
{
    return _MAV_RETURN_float(msg,  4);
}

/**
 * @brief Get field y from fleet_uav_info_local message
 *
 * @return  Coordinate Y
 */
static inline float mavlink_msg_fleet_uav_info_local_get_y(const mavlink_message_t* msg)
{
    return _MAV_RETURN_float(msg,  8);
}

/**
 * @brief Get field z from fleet_uav_info_local message
 *
 * @return  Coordinate Z
 */
static inline float mavlink_msg_fleet_uav_info_local_get_z(const mavlink_message_t* msg)
{
    return _MAV_RETURN_float(msg,  12);
}

/**
 * @brief Get field heading from fleet_uav_info_local message
 *
 * @return  Heading
 */
static inline float mavlink_msg_fleet_uav_info_local_get_heading(const mavlink_message_t* msg)
{
    return _MAV_RETURN_float(msg,  16);
}

/**
 * @brief Decode a fleet_uav_info_local message into a struct
 *
 * @param msg The message to decode
 * @param fleet_uav_info_local C-struct to decode the message contents into
 */
static inline void mavlink_msg_fleet_uav_info_local_decode(const mavlink_message_t* msg, mavlink_fleet_uav_info_local_t* fleet_uav_info_local)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    fleet_uav_info_local->custom_mode = mavlink_msg_fleet_uav_info_local_get_custom_mode(msg);
    fleet_uav_info_local->x = mavlink_msg_fleet_uav_info_local_get_x(msg);
    fleet_uav_info_local->y = mavlink_msg_fleet_uav_info_local_get_y(msg);
    fleet_uav_info_local->z = mavlink_msg_fleet_uav_info_local_get_z(msg);
    fleet_uav_info_local->heading = mavlink_msg_fleet_uav_info_local_get_heading(msg);
    fleet_uav_info_local->voltage_battery = mavlink_msg_fleet_uav_info_local_get_voltage_battery(msg);
    fleet_uav_info_local->battery_remaining = mavlink_msg_fleet_uav_info_local_get_battery_remaining(msg);
    fleet_uav_info_local->drop_rate_comm = mavlink_msg_fleet_uav_info_local_get_drop_rate_comm(msg);
    fleet_uav_info_local->errors_drop = mavlink_msg_fleet_uav_info_local_get_errors_drop(msg);
    fleet_uav_info_local->base_mode = mavlink_msg_fleet_uav_info_local_get_base_mode(msg);
    fleet_uav_info_local->system_status = mavlink_msg_fleet_uav_info_local_get_system_status(msg);
#else
        uint8_t len = msg->len < MAVLINK_MSG_ID_FLEET_UAV_INFO_LOCAL_LEN? msg->len : MAVLINK_MSG_ID_FLEET_UAV_INFO_LOCAL_LEN;
        memset(fleet_uav_info_local, 0, MAVLINK_MSG_ID_FLEET_UAV_INFO_LOCAL_LEN);
    memcpy(fleet_uav_info_local, _MAV_PAYLOAD(msg), len);
#endif
}
