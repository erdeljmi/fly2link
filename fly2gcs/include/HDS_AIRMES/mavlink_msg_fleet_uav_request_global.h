#pragma once
// MESSAGE FLEET_UAV_REQUEST_GLOBAL PACKING

#define MAVLINK_MSG_ID_FLEET_UAV_REQUEST_GLOBAL 153

MAVPACKED(
typedef struct __mavlink_fleet_uav_request_global_t {
 uint32_t latitude; /*<  Latitude GPS*/
 uint32_t longitude; /*<  Longitude GPS*/
 uint16_t altitude; /*<  Altitude*/
 uint8_t request_type; /*<  Type of the request (consult ENUM_FLEET_REQUEST_TYPE)*/
 uint8_t request_ID; /*<  Request's unique ID*/
 uint8_t ID_to_replace; /*<  System ID of the system/component that sent the request*/
 uint8_t request_urgency; /*<  Parameter used as a coefficient for choosing the most critical request to respond to*/
}) mavlink_fleet_uav_request_global_t;

#define MAVLINK_MSG_ID_FLEET_UAV_REQUEST_GLOBAL_LEN 14
#define MAVLINK_MSG_ID_FLEET_UAV_REQUEST_GLOBAL_MIN_LEN 14
#define MAVLINK_MSG_ID_153_LEN 14
#define MAVLINK_MSG_ID_153_MIN_LEN 14

#define MAVLINK_MSG_ID_FLEET_UAV_REQUEST_GLOBAL_CRC 191
#define MAVLINK_MSG_ID_153_CRC 191



#if MAVLINK_COMMAND_24BIT
#define MAVLINK_MESSAGE_INFO_FLEET_UAV_REQUEST_GLOBAL { \
    153, \
    "FLEET_UAV_REQUEST_GLOBAL", \
    7, \
    {  { "latitude", NULL, MAVLINK_TYPE_UINT32_T, 0, 0, offsetof(mavlink_fleet_uav_request_global_t, latitude) }, \
         { "longitude", NULL, MAVLINK_TYPE_UINT32_T, 0, 4, offsetof(mavlink_fleet_uav_request_global_t, longitude) }, \
         { "altitude", NULL, MAVLINK_TYPE_UINT16_T, 0, 8, offsetof(mavlink_fleet_uav_request_global_t, altitude) }, \
         { "request_type", NULL, MAVLINK_TYPE_UINT8_T, 0, 10, offsetof(mavlink_fleet_uav_request_global_t, request_type) }, \
         { "request_ID", NULL, MAVLINK_TYPE_UINT8_T, 0, 11, offsetof(mavlink_fleet_uav_request_global_t, request_ID) }, \
         { "ID_to_replace", NULL, MAVLINK_TYPE_UINT8_T, 0, 12, offsetof(mavlink_fleet_uav_request_global_t, ID_to_replace) }, \
         { "request_urgency", NULL, MAVLINK_TYPE_UINT8_T, 0, 13, offsetof(mavlink_fleet_uav_request_global_t, request_urgency) }, \
         } \
}
#else
#define MAVLINK_MESSAGE_INFO_FLEET_UAV_REQUEST_GLOBAL { \
    "FLEET_UAV_REQUEST_GLOBAL", \
    7, \
    {  { "latitude", NULL, MAVLINK_TYPE_UINT32_T, 0, 0, offsetof(mavlink_fleet_uav_request_global_t, latitude) }, \
         { "longitude", NULL, MAVLINK_TYPE_UINT32_T, 0, 4, offsetof(mavlink_fleet_uav_request_global_t, longitude) }, \
         { "altitude", NULL, MAVLINK_TYPE_UINT16_T, 0, 8, offsetof(mavlink_fleet_uav_request_global_t, altitude) }, \
         { "request_type", NULL, MAVLINK_TYPE_UINT8_T, 0, 10, offsetof(mavlink_fleet_uav_request_global_t, request_type) }, \
         { "request_ID", NULL, MAVLINK_TYPE_UINT8_T, 0, 11, offsetof(mavlink_fleet_uav_request_global_t, request_ID) }, \
         { "ID_to_replace", NULL, MAVLINK_TYPE_UINT8_T, 0, 12, offsetof(mavlink_fleet_uav_request_global_t, ID_to_replace) }, \
         { "request_urgency", NULL, MAVLINK_TYPE_UINT8_T, 0, 13, offsetof(mavlink_fleet_uav_request_global_t, request_urgency) }, \
         } \
}
#endif

/**
 * @brief Pack a fleet_uav_request_global message
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 *
 * @param request_type  Type of the request (consult ENUM_FLEET_REQUEST_TYPE)
 * @param request_ID  Request's unique ID
 * @param ID_to_replace  System ID of the system/component that sent the request
 * @param latitude  Latitude GPS
 * @param longitude  Longitude GPS
 * @param altitude  Altitude
 * @param request_urgency  Parameter used as a coefficient for choosing the most critical request to respond to
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_fleet_uav_request_global_pack(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg,
                               uint8_t request_type, uint8_t request_ID, uint8_t ID_to_replace, uint32_t latitude, uint32_t longitude, uint16_t altitude, uint8_t request_urgency)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_FLEET_UAV_REQUEST_GLOBAL_LEN];
    _mav_put_uint32_t(buf, 0, latitude);
    _mav_put_uint32_t(buf, 4, longitude);
    _mav_put_uint16_t(buf, 8, altitude);
    _mav_put_uint8_t(buf, 10, request_type);
    _mav_put_uint8_t(buf, 11, request_ID);
    _mav_put_uint8_t(buf, 12, ID_to_replace);
    _mav_put_uint8_t(buf, 13, request_urgency);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_FLEET_UAV_REQUEST_GLOBAL_LEN);
#else
    mavlink_fleet_uav_request_global_t packet;
    packet.latitude = latitude;
    packet.longitude = longitude;
    packet.altitude = altitude;
    packet.request_type = request_type;
    packet.request_ID = request_ID;
    packet.ID_to_replace = ID_to_replace;
    packet.request_urgency = request_urgency;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_FLEET_UAV_REQUEST_GLOBAL_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_FLEET_UAV_REQUEST_GLOBAL;
    return mavlink_finalize_message(msg, system_id, component_id, MAVLINK_MSG_ID_FLEET_UAV_REQUEST_GLOBAL_MIN_LEN, MAVLINK_MSG_ID_FLEET_UAV_REQUEST_GLOBAL_LEN, MAVLINK_MSG_ID_FLEET_UAV_REQUEST_GLOBAL_CRC);
}

/**
 * @brief Pack a fleet_uav_request_global message on a channel
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param request_type  Type of the request (consult ENUM_FLEET_REQUEST_TYPE)
 * @param request_ID  Request's unique ID
 * @param ID_to_replace  System ID of the system/component that sent the request
 * @param latitude  Latitude GPS
 * @param longitude  Longitude GPS
 * @param altitude  Altitude
 * @param request_urgency  Parameter used as a coefficient for choosing the most critical request to respond to
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_fleet_uav_request_global_pack_chan(uint8_t system_id, uint8_t component_id, uint8_t chan,
                               mavlink_message_t* msg,
                                   uint8_t request_type,uint8_t request_ID,uint8_t ID_to_replace,uint32_t latitude,uint32_t longitude,uint16_t altitude,uint8_t request_urgency)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_FLEET_UAV_REQUEST_GLOBAL_LEN];
    _mav_put_uint32_t(buf, 0, latitude);
    _mav_put_uint32_t(buf, 4, longitude);
    _mav_put_uint16_t(buf, 8, altitude);
    _mav_put_uint8_t(buf, 10, request_type);
    _mav_put_uint8_t(buf, 11, request_ID);
    _mav_put_uint8_t(buf, 12, ID_to_replace);
    _mav_put_uint8_t(buf, 13, request_urgency);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_FLEET_UAV_REQUEST_GLOBAL_LEN);
#else
    mavlink_fleet_uav_request_global_t packet;
    packet.latitude = latitude;
    packet.longitude = longitude;
    packet.altitude = altitude;
    packet.request_type = request_type;
    packet.request_ID = request_ID;
    packet.ID_to_replace = ID_to_replace;
    packet.request_urgency = request_urgency;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_FLEET_UAV_REQUEST_GLOBAL_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_FLEET_UAV_REQUEST_GLOBAL;
    return mavlink_finalize_message_chan(msg, system_id, component_id, chan, MAVLINK_MSG_ID_FLEET_UAV_REQUEST_GLOBAL_MIN_LEN, MAVLINK_MSG_ID_FLEET_UAV_REQUEST_GLOBAL_LEN, MAVLINK_MSG_ID_FLEET_UAV_REQUEST_GLOBAL_CRC);
}

/**
 * @brief Encode a fleet_uav_request_global struct
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 * @param fleet_uav_request_global C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_fleet_uav_request_global_encode(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg, const mavlink_fleet_uav_request_global_t* fleet_uav_request_global)
{
    return mavlink_msg_fleet_uav_request_global_pack(system_id, component_id, msg, fleet_uav_request_global->request_type, fleet_uav_request_global->request_ID, fleet_uav_request_global->ID_to_replace, fleet_uav_request_global->latitude, fleet_uav_request_global->longitude, fleet_uav_request_global->altitude, fleet_uav_request_global->request_urgency);
}

/**
 * @brief Encode a fleet_uav_request_global struct on a channel
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param fleet_uav_request_global C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_fleet_uav_request_global_encode_chan(uint8_t system_id, uint8_t component_id, uint8_t chan, mavlink_message_t* msg, const mavlink_fleet_uav_request_global_t* fleet_uav_request_global)
{
    return mavlink_msg_fleet_uav_request_global_pack_chan(system_id, component_id, chan, msg, fleet_uav_request_global->request_type, fleet_uav_request_global->request_ID, fleet_uav_request_global->ID_to_replace, fleet_uav_request_global->latitude, fleet_uav_request_global->longitude, fleet_uav_request_global->altitude, fleet_uav_request_global->request_urgency);
}

/**
 * @brief Send a fleet_uav_request_global message
 * @param chan MAVLink channel to send the message
 *
 * @param request_type  Type of the request (consult ENUM_FLEET_REQUEST_TYPE)
 * @param request_ID  Request's unique ID
 * @param ID_to_replace  System ID of the system/component that sent the request
 * @param latitude  Latitude GPS
 * @param longitude  Longitude GPS
 * @param altitude  Altitude
 * @param request_urgency  Parameter used as a coefficient for choosing the most critical request to respond to
 */
#ifdef MAVLINK_USE_CONVENIENCE_FUNCTIONS

static inline void mavlink_msg_fleet_uav_request_global_send(mavlink_channel_t chan, uint8_t request_type, uint8_t request_ID, uint8_t ID_to_replace, uint32_t latitude, uint32_t longitude, uint16_t altitude, uint8_t request_urgency)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_FLEET_UAV_REQUEST_GLOBAL_LEN];
    _mav_put_uint32_t(buf, 0, latitude);
    _mav_put_uint32_t(buf, 4, longitude);
    _mav_put_uint16_t(buf, 8, altitude);
    _mav_put_uint8_t(buf, 10, request_type);
    _mav_put_uint8_t(buf, 11, request_ID);
    _mav_put_uint8_t(buf, 12, ID_to_replace);
    _mav_put_uint8_t(buf, 13, request_urgency);

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_FLEET_UAV_REQUEST_GLOBAL, buf, MAVLINK_MSG_ID_FLEET_UAV_REQUEST_GLOBAL_MIN_LEN, MAVLINK_MSG_ID_FLEET_UAV_REQUEST_GLOBAL_LEN, MAVLINK_MSG_ID_FLEET_UAV_REQUEST_GLOBAL_CRC);
#else
    mavlink_fleet_uav_request_global_t packet;
    packet.latitude = latitude;
    packet.longitude = longitude;
    packet.altitude = altitude;
    packet.request_type = request_type;
    packet.request_ID = request_ID;
    packet.ID_to_replace = ID_to_replace;
    packet.request_urgency = request_urgency;

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_FLEET_UAV_REQUEST_GLOBAL, (const char *)&packet, MAVLINK_MSG_ID_FLEET_UAV_REQUEST_GLOBAL_MIN_LEN, MAVLINK_MSG_ID_FLEET_UAV_REQUEST_GLOBAL_LEN, MAVLINK_MSG_ID_FLEET_UAV_REQUEST_GLOBAL_CRC);
#endif
}

/**
 * @brief Send a fleet_uav_request_global message
 * @param chan MAVLink channel to send the message
 * @param struct The MAVLink struct to serialize
 */
static inline void mavlink_msg_fleet_uav_request_global_send_struct(mavlink_channel_t chan, const mavlink_fleet_uav_request_global_t* fleet_uav_request_global)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    mavlink_msg_fleet_uav_request_global_send(chan, fleet_uav_request_global->request_type, fleet_uav_request_global->request_ID, fleet_uav_request_global->ID_to_replace, fleet_uav_request_global->latitude, fleet_uav_request_global->longitude, fleet_uav_request_global->altitude, fleet_uav_request_global->request_urgency);
#else
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_FLEET_UAV_REQUEST_GLOBAL, (const char *)fleet_uav_request_global, MAVLINK_MSG_ID_FLEET_UAV_REQUEST_GLOBAL_MIN_LEN, MAVLINK_MSG_ID_FLEET_UAV_REQUEST_GLOBAL_LEN, MAVLINK_MSG_ID_FLEET_UAV_REQUEST_GLOBAL_CRC);
#endif
}

#if MAVLINK_MSG_ID_FLEET_UAV_REQUEST_GLOBAL_LEN <= MAVLINK_MAX_PAYLOAD_LEN
/*
  This varient of _send() can be used to save stack space by re-using
  memory from the receive buffer.  The caller provides a
  mavlink_message_t which is the size of a full mavlink message. This
  is usually the receive buffer for the channel, and allows a reply to an
  incoming message with minimum stack space usage.
 */
static inline void mavlink_msg_fleet_uav_request_global_send_buf(mavlink_message_t *msgbuf, mavlink_channel_t chan,  uint8_t request_type, uint8_t request_ID, uint8_t ID_to_replace, uint32_t latitude, uint32_t longitude, uint16_t altitude, uint8_t request_urgency)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char *buf = (char *)msgbuf;
    _mav_put_uint32_t(buf, 0, latitude);
    _mav_put_uint32_t(buf, 4, longitude);
    _mav_put_uint16_t(buf, 8, altitude);
    _mav_put_uint8_t(buf, 10, request_type);
    _mav_put_uint8_t(buf, 11, request_ID);
    _mav_put_uint8_t(buf, 12, ID_to_replace);
    _mav_put_uint8_t(buf, 13, request_urgency);

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_FLEET_UAV_REQUEST_GLOBAL, buf, MAVLINK_MSG_ID_FLEET_UAV_REQUEST_GLOBAL_MIN_LEN, MAVLINK_MSG_ID_FLEET_UAV_REQUEST_GLOBAL_LEN, MAVLINK_MSG_ID_FLEET_UAV_REQUEST_GLOBAL_CRC);
#else
    mavlink_fleet_uav_request_global_t *packet = (mavlink_fleet_uav_request_global_t *)msgbuf;
    packet->latitude = latitude;
    packet->longitude = longitude;
    packet->altitude = altitude;
    packet->request_type = request_type;
    packet->request_ID = request_ID;
    packet->ID_to_replace = ID_to_replace;
    packet->request_urgency = request_urgency;

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_FLEET_UAV_REQUEST_GLOBAL, (const char *)packet, MAVLINK_MSG_ID_FLEET_UAV_REQUEST_GLOBAL_MIN_LEN, MAVLINK_MSG_ID_FLEET_UAV_REQUEST_GLOBAL_LEN, MAVLINK_MSG_ID_FLEET_UAV_REQUEST_GLOBAL_CRC);
#endif
}
#endif

#endif

// MESSAGE FLEET_UAV_REQUEST_GLOBAL UNPACKING


/**
 * @brief Get field request_type from fleet_uav_request_global message
 *
 * @return  Type of the request (consult ENUM_FLEET_REQUEST_TYPE)
 */
static inline uint8_t mavlink_msg_fleet_uav_request_global_get_request_type(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint8_t(msg,  10);
}

/**
 * @brief Get field request_ID from fleet_uav_request_global message
 *
 * @return  Request's unique ID
 */
static inline uint8_t mavlink_msg_fleet_uav_request_global_get_request_ID(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint8_t(msg,  11);
}

/**
 * @brief Get field ID_to_replace from fleet_uav_request_global message
 *
 * @return  System ID of the system/component that sent the request
 */
static inline uint8_t mavlink_msg_fleet_uav_request_global_get_ID_to_replace(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint8_t(msg,  12);
}

/**
 * @brief Get field latitude from fleet_uav_request_global message
 *
 * @return  Latitude GPS
 */
static inline uint32_t mavlink_msg_fleet_uav_request_global_get_latitude(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint32_t(msg,  0);
}

/**
 * @brief Get field longitude from fleet_uav_request_global message
 *
 * @return  Longitude GPS
 */
static inline uint32_t mavlink_msg_fleet_uav_request_global_get_longitude(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint32_t(msg,  4);
}

/**
 * @brief Get field altitude from fleet_uav_request_global message
 *
 * @return  Altitude
 */
static inline uint16_t mavlink_msg_fleet_uav_request_global_get_altitude(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint16_t(msg,  8);
}

/**
 * @brief Get field request_urgency from fleet_uav_request_global message
 *
 * @return  Parameter used as a coefficient for choosing the most critical request to respond to
 */
static inline uint8_t mavlink_msg_fleet_uav_request_global_get_request_urgency(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint8_t(msg,  13);
}

/**
 * @brief Decode a fleet_uav_request_global message into a struct
 *
 * @param msg The message to decode
 * @param fleet_uav_request_global C-struct to decode the message contents into
 */
static inline void mavlink_msg_fleet_uav_request_global_decode(const mavlink_message_t* msg, mavlink_fleet_uav_request_global_t* fleet_uav_request_global)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    fleet_uav_request_global->latitude = mavlink_msg_fleet_uav_request_global_get_latitude(msg);
    fleet_uav_request_global->longitude = mavlink_msg_fleet_uav_request_global_get_longitude(msg);
    fleet_uav_request_global->altitude = mavlink_msg_fleet_uav_request_global_get_altitude(msg);
    fleet_uav_request_global->request_type = mavlink_msg_fleet_uav_request_global_get_request_type(msg);
    fleet_uav_request_global->request_ID = mavlink_msg_fleet_uav_request_global_get_request_ID(msg);
    fleet_uav_request_global->ID_to_replace = mavlink_msg_fleet_uav_request_global_get_ID_to_replace(msg);
    fleet_uav_request_global->request_urgency = mavlink_msg_fleet_uav_request_global_get_request_urgency(msg);
#else
        uint8_t len = msg->len < MAVLINK_MSG_ID_FLEET_UAV_REQUEST_GLOBAL_LEN? msg->len : MAVLINK_MSG_ID_FLEET_UAV_REQUEST_GLOBAL_LEN;
        memset(fleet_uav_request_global, 0, MAVLINK_MSG_ID_FLEET_UAV_REQUEST_GLOBAL_LEN);
    memcpy(fleet_uav_request_global, _MAV_PAYLOAD(msg), len);
#endif
}
