#ifndef FLATBUFF_GENERATED
#define FLATBUFF_GENERATED

#include "fleetgcs-telemetry_generated.h"
#include "fleet_clear_target_generated.h" 
#include "fleet_set_formation_generated.h" 
#include "fleet_set_target_generated.h" 
#include "fleet_set_target_local_generated.h" 
#include "fleet_start_mission_generated.h"
#include "fleet_stop_mission_generated.h"

#endif