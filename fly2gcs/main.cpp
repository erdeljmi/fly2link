#include "Fly2Recv.h"
#include <QApplication>
#include <iostream>
#include <tuple>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    QApplication::setApplicationName("Fly2GCS");
    QApplication::setApplicationVersion("2.1");

    // using values from the configuration file
    const std::string config_file = "fly2gcs_config.xml";
    using boost::property_tree::ptree;
    ptree pt;

    // using hard-coded values in case if parameters are not provided
    int cmdPort = 5555;
    int infoPort = 9999;
    int sysid = 1;
    bool debug_info = false;

    read_xml(config_file, pt);

    for(const auto& i : pt.get_child("fly2gcs.config"))
    {
        std::string name;
        std::string param_name;
        std::string param_value;
        ptree sub_pt;
        std::tie(name, sub_pt) = i;

        if (name != "param")
            continue;
        param_name = sub_pt.get<std::string>("<xmlattr>.name");
        param_value = sub_pt.get<std::string>("<xmlattr>.value");
        //std::cout << name << " " << sub_pt.get<std::string>("<xmlattr>.name") << " " << sub_pt.get<std::string>("<xmlattr>.value") << std::endl;

        if(!param_name.compare("uav_sys_id")) {
            sysid = stoi(param_value);
            std::cout << "Configuration parameter retrieved: " << param_name << " = " << param_value << std::endl;
        }
        if(!param_name.compare("cid_command_port")) {
            cmdPort = stoi(param_value);
            std::cout << "Configuration parameter retrieved: " << param_name << " = " << param_value << std::endl;
        }
        if(!param_name.compare("cid_info_port")) {
            infoPort = stoi(param_value);
            std::cout << "Configuration parameter retrieved: " << param_name << " = " << param_value << std::endl;
        }
        if(!param_name.compare("debug_info")) {
            debug_info = (bool)(stoi(param_value));
            std::cout << "Configuration parameter retrieved: " << param_name << " = " << param_value << std::endl;
        }
    }

    std::cout << "Command port: " << cmdPort << std::endl;
    std::cout << "Info port: " << infoPort << std::endl;
    std::cout << "System ID: " << sysid << std::endl;
    std::cout << "Verbose: " << debug_info << std::endl;

    Fly2Recv w(sysid, cmdPort, infoPort, debug_info, config_file);
    w.show();
    return app.exec();
}
