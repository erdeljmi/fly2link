package main

// DataClient : code executed by each node/drone.
// communicates information to the robot server.

import (
	"log"
	//	"fmt"
	b64 "encoding/base64"
	"io/ioutil"
	"os"
	"os/exec"
	"strconv"
	"time"

	flatbuffers "github.com/google/flatbuffers/go"
	zmq "github.com/pebbe/zmq4"
	//message "gitlab.utc.fr/bobo-dataflatbuffers/TXData"
	message "dataflatbuffers/TXData"

	viper "github.com/spf13/viper"
)

func main() {
	// read configuration from file
	viper.SetConfigName("config")
	viper.AddConfigPath("./")

	err := viper.ReadInConfig()

	if err != nil {
		log.Fatalf("Config file not found. Create a 'config' config file")
	}

	droneId := uint32(viper.GetInt("drone.id"))
	camDevice := viper.GetString("drone.cam-path")

	imagePath := viper.GetString("screenshot.image-path")

	retrieve := viper.GetString("stream.retrieve")
	publish := viper.GetString("stream.publish")

	publishString := publish + "/" + strconv.Itoa(int(droneId))
	retrieveString := retrieve + "?drone=" + strconv.Itoa(int(droneId))

	log.Println("publish " + publishString)
	log.Println("retrieve " + retrieveString)

	robotServer := viper.GetString("robot-server.server")

	ResetSnapshot(imagePath + "/snapshot.jpeg")

	streamInfo := publish + "/" + strconv.Itoa(int(droneId))
	//ffmpeg -f v4l2 -framerate 25 -video_size 640x480 -i /dev/video0 -f mpegts -codec:v mpeg1video -s 640x480 -b:v 1000k -bf 0 http://localhost:8081/supersecret
	cmd := exec.Command("ffmpeg", "-f", "v4l2", "-framerate", "25", "-video_size", "640x480", "-i", camDevice, "-f", "mpegts", "-codec:v", "mpeg1video", "-s", "640x480", "-b:v", "1000k", "-bf", streamInfo, "-f", "image2", "-update", "1", "-vf", "fps=fps=0.5", imagePath+"/snapshot.jpeg")
	go cmd.Run()
	log.Printf("Command finished with error: %v", err)

	log.Println("launched stream on " + streamInfo)

	// create the context
	//context, _ := zmq.NewContext()

	// a drone node is a PUSH client
	// connect to the sink
	sender, _ := zmq.NewSocket(zmq.PUSH)
	defer sender.Close()
	sender.Bind("tcp://" + robotServer)
	i := 0
	j := 0

	// endless streaming
	for {

		log.Println("iteration")

		i++
		j++
		if j == 5 {
			log.Println("prepare sending streaming")

			sender.Send("Streaming", zmq.SNDMORE)
			data := retrieveStreamingData(droneId, publishString, retrieveString)
			sender.SendBytes(data, 0)
			log.Println("sent stream info")

			j = 0
		}
		if i == 11 {
			log.Println("prepare sending screenshot")

			sender.Send("Screenshot", zmq.SNDMORE)
			data := retrieveScreenshotData(droneId, imagePath)
			sender.SendBytes(data, 0)

			log.Println("sent screenshot")
			i = 0
		}

		time.Sleep(time.Second)
	}
}

func retrieveStreamingData(droneId uint32, publish string, retrieve string) []byte {

	// initialize our flatbuffer builder with 1024 bytes of initial size
	builder := flatbuffers.NewBuilder(1024)

	retrieveString := builder.CreateString(retrieve)
	publishString := builder.CreateString(publish)

	// build the telemetry
	message.StreamingStart(builder)
	message.StreamingAddDroneId(builder, droneId)

	message.StreamingAddUrlRetrieve(builder, retrieveString)
	message.StreamingAddUrlPublish(builder, publishString)

	messageToSend := message.StreamingEnd(builder)
	builder.Finish(messageToSend)
	data := builder.FinishedBytes()

	return data
}

func retrieveScreenshotData(droneId uint32, dir string) []byte {

	file := dir + "/snapshot.jpeg"

	// initialize our flatbuffer builder with 1024 bytes of initial size
	builder := flatbuffers.NewBuilder(1024)

	log.Println(dir + "/" + file)
	screenshot, _ := ioutil.ReadFile(dir + "/" + file)
	baseScreenshot := b64.StdEncoding.EncodeToString([]byte(screenshot))
	log.Println(baseScreenshot)

	baseScreenshotString := builder.CreateString(baseScreenshot)

	// build the telemetry
	message.ScreenshotStart(builder)
	message.ScreenshotAddDrone(builder, droneId)

	message.ScreenshotAddImage(builder, baseScreenshotString)

	messageToSend := message.ScreenshotEnd(builder)
	builder.Finish(messageToSend)
	data := builder.FinishedBytes()

	return data
}

func ResetSnapshot(name string) bool {
	if _, err := os.Stat(name); err != nil {
		if os.IsNotExist(err) {
			return false
		}
	}

	os.Remove(name)

	return true
}
