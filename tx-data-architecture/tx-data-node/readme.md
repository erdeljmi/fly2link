# data-node
uses viper, zeromq, ffmpeg, flatbuffers

wrapper around ffmpeg, also sends data to the drone-router

# global data flow
```
(drone-node) -> (robot-server)

ffmpeg -- (mpegTS)--> konam-relay 
drone-node -- (telemetry/flabuffers) --> drone-router
drone-node -- (stream info/string) --> drone-router
drone-node -- (screenshot/base64 string) --> drone-router
```

drone configuration (drone-id, video inptu) and network configuration is done in the config file

just execute one drone-node program per drone
