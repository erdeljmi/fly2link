package main

// DataRouter : code executed by robot server.
// will gather every data sent by the local nodes, and send it to the central webserver

import (
	"log"
	"time"

	//flatbuffers "github.com/google/flatbuffers/go"
	zmq "github.com/pebbe/zmq4"
	//	message "gitlab.utc.fr/bobo-dataflatbuffers/TXData"
	viper "github.com/spf13/viper"
	//message "gitlab.utc.fr/ukborey/tx_streaming/data-streaming/bobo-dataflatbuffers/TXData"
)

func main() {
	viper.SetConfigName("config")
	viper.AddConfigPath("./")

	err := viper.ReadInConfig()

	if err != nil {
		log.Fatalf("Config file not found. Create a config file and relaunch")
	}

	//context, _ := zmq.Context()

	serverAddress := viper.GetString("server.address")
	webserverAddress := viper.GetString("webserver.address")

	log.Println("Server address: ", serverAddress)
	log.Println("Webserver address: ", webserverAddress)

	//thread for the way api > router > fleetgcs
	go routineReceiveCmd()

	// we want to pull everything that is sent by the drones
	receiver, _ := zmq.NewSocket(zmq.PULL)
	defer receiver.Close()
	receiver.Connect("tcp://" + serverAddress)

	// we will push every data sent by the drones to the webserver
	sender, _ := zmq.NewSocket(zmq.PUB)
	defer sender.Close()
	sender.Bind("tcp://" + webserverAddress)

	log.Println("begin loop ")
	//sender.Send("Streaming", zmq.SNDMORE)
	//sender.Send("Test", 0)

	for {

		log.Println("inside message loop")
		// get the envoloppe
		envl, _ := receiver.Recv(0)
		// get the data
		data, _ := receiver.RecvBytes(0)
		log.Println("got message")
		log.Println(envl)
		log.Println(data)

		//resend everything
		sender.Send(envl, zmq.SNDMORE)
		sender.SendBytes(data, 0)
	}
}

func routineReceiveCmd() {
	listeningServerAddress := viper.GetString("listeningserver.address")
	requestServerAddress := viper.GetString("requestserver.address")
	uavServerId := viper.GetString("uavserver.id")

	// we will push every data sent by the drones to the webserver
	subscriber, _ := zmq.NewSocket(zmq.SUB)
	defer subscriber.Close()
	subscriber.Connect("tcp://" + listeningServerAddress)
	subscriber.SetSubscribe(uavServerId) //subscribe on its name

	// send to the fleetgcs server
	senderCmd, _ := zmq.NewSocket(zmq.PUSH)
	defer senderCmd.Close()
	senderCmd.Connect("tcp://" + requestServerAddress)

	// 0MQ is so fast, we need to wait a while...
	// not sure about this
	time.Sleep(time.Second)

	log.Println("begin loop ")

	for {
		// get the envoloppe
		envl, _ := subscriber.Recv(0)
		// get the data
		data, _ := subscriber.RecvBytes(0)
		//resend everything
		senderCmd.Send(envl, zmq.SNDMORE)
		senderCmd.SendBytes(data, 0)
	}

}

// test function to read from a flatbuffer

/*func readData(data []byte) {
	receivedMessageContent := message.GetRootAsMessage(data, 0)
	unionTable := new(flatbuffers.Table)
	if receivedMessageContent.Message(unionTable) {
		unionType := receivedMessageContent.MessageType()
		if unionType == message.MessageContentTelemetry {
			telemetryRead := new(message.Telemetry)
			telemetryRead.Init(unionTable.Bytes, unionTable.Pos)
			receivedMessageString := telemetryRead.Message()
			log.Println(string(receivedMessageString))
		}
	}
}*/
