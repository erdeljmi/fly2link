# data-router
collect every data sent by attached drones. also serves as a waypoint for every video drone stream.
check `konam-relay` for media streaming

this program is just a router for serving drones data to the webserver. thus, it doesn't do any processing on data, contenting itself of sending what it receives.
it is a gateway for the drones to the webserver.

## basic data flow

```
(drone-node) -> (data-router) -> (web-client)
ffmpeg -- mpeg-ts --> konam-relay -- mpeg-ts --> /jsmpeg

(drone-node) -> (data-router) -> (data-webserver)
drone-node -- (telemetry/flatbuffers) --> data-router -- (telemetry/flatbuffers) --> data-webserver
dorne-node -- (stream info/string) --> data-router -- (stream info/string) --> data-webserver
drone-node -- (screenshot/string) --> data-router -- (screenshot/string) --> data-webserver
```
