package main

import (
	"log"
	"os/exec"
)

//launcher for the API REST
func NewAPIProcesser() *GenericProcesser {
	return &GenericProcesser{"API", nil}
}

func (sp *GenericProcesser) ProcessAPI() {
	log.Println("APIProcesser launched!")

	cmd := exec.Command("node", "server.js")
	cmd.Dir = "./tx-data-server-api" //check your path
	err := cmd.Run()

	if err != nil {
		log.Println(err)
	}
}
