package main

import (
	"log"
	zmq "github.com/pebbe/zmq4"
	viper "github.com/spf13/viper"	
	
)

type GenericProcesser struct {
	SubscribeLine   string
	ProcesserSocket *zmq.Socket
}

var robotBind string
var internalBind string
var internalAddress string

// var apiAddress string

func main() {
	log.Println("DataServer: launching...")
	viper.SetConfigName("config")
	viper.AddConfigPath("./")
	err := viper.ReadInConfig()
	if err != nil {
		log.Fatalf("config file not found")
	}
	robotBind = viper.GetString("network.robot-bind")
	internalBind = viper.GetString("network.internal-bind")
	internalAddress = viper.GetString("network.internal-address")

	log.Println("Robot bind: ", robotBind)
	log.Println("Internal bind: ", internalBind)
	log.Println("Internal address: ", internalAddress)

	context, _ := zmq.NewContext()

	// pull everything from the robot servers
	receiver, _ := context.NewSocket(zmq.PULL)
	defer receiver.Close()
	receiver.Bind("tcp://" + robotBind)

	// publisher endpoint for the sub programs
	publisher, _ := context.NewSocket(zmq.PUB)
	defer publisher.Close()
	publisher.Bind("tcp://" + internalBind)

	telemetrySocket, _ := zmq.NewSocket(zmq.SUB)


	//streamingProcesser := NewStreamingProcesser()
	apiProcesser := NewAPIProcesser()
	//screenshotProcesser := NewScreenshotProcesser()
	telemetryProcesser := NewTelemetryProcesser(telemetrySocket)

	//go screenshotProcesser.ProcessScreenshots()
	go telemetryProcesser.ProcessTelemetries()
	go apiProcesser.ProcessAPI()
	//go streamingProcesser.ProcessStreaming()
	
	log.Println("DataServer: ready!")

	for {
		envl, _ := receiver.Recv(0)
		data, _ := receiver.RecvBytes(0)
		publisher.Send(envl, zmq.SNDMORE)
		publisher.SendBytes(data, 0)
				log.Println("sent to workers")
	}
}
