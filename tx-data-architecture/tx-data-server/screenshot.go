package main

import (
	"log"
	"os/exec"
	
)

func NewScreenshotProcesser() *GenericProcesser {
	return &GenericProcesser{"Screenshot", nil}
}



func (sp *GenericProcesser) ProcessScreenshots() {
	log.Println("ScreenshotProcesser launched!")
	
	
	
	cmd := exec.Command("node", "subber-screenshot.js")
	cmd.Dir = "./tx-data-server-screenshot" //check your path
	err:= cmd.Run()
	
  	if err != nil {
  		log.Println(err)
  	}
 
	
}
