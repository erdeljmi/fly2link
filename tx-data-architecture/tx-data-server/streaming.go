package main

import (
	"log"
	"os/exec"
	
)

//launcher for the Streaming REST
func NewStreamingProcesser() *GenericProcesser {
	return &GenericProcesser{"Streaming", nil}
}



func (sp *GenericProcesser) ProcessStreaming() {
	log.Println("StreamingProcesser launched!")
	
	cmd := exec.Command("node", "websocket-relay.js")
	cmd.Dir = "./tx-data-server-streaming" //check your path
	err:= cmd.Run()
	
  	if err != nil {
  		log.Println(err)
  	}	
}
