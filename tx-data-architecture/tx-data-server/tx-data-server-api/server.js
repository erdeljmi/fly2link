
//express for api rest
var express = require('express');  
var app = express(); 

//flatbuffers
var flatbuffers = require('flatbuffers').flatbuffers; 
  
//app.use(express.json()); 

// removed everything regardin elasticsearch

// // client elasticsearch
// var elasticsearch = require('elasticsearch');
// var client = new elasticsearch.Client({
// 	host: 'http://192.168.56.1:9200',
// 	log: 'trace',
// 	httpAuth:'elastic:Z8I7rcZhE8@njE2d-&F2'
// });

// //check elasticsearch
// client.ping({
//   // ping usually has a 3000ms timeout
// 	requestTimeout: 3000
// }, function (error) {
//   if (error) {
//     console.trace(error+' elasticsearch cluster is down!');
//   } else {
//     console.log('All is well');
//   }
// });

//zmq
var zmq = require('zmq');
var sock = zmq.socket('pub');

sock.bindSync('tcp://127.0.0.1:1339');
console.log('ZMQ publisher bound to port 1339');

//template for http response
require('./response');


//structures for flatbuffer
var FLEET_STOP_MISSION = require('./fleet_stop_mission_generated').FLEET_STOP_MISSION;
var FLEET_START_MISSION = require('./fleet_start_mission_generated').FLEET_START_MISSION;
var FLEET_SET_TARGET = require('./fleet_set_target_generated').FLEET_SET_TARGET;
var FLEET_SET_TARGET_LOCAL = require('./fleet_set_target_local_generated').FLEET_SET_TARGET_LOCAL;
var FLEET_CLEAR_TARGET = require('./fleet_clear_target_generated').FLEET_CLEAR_TARGET;
var FLEET_SET_FORMATION = require('./fleet_set_formation_generated').FLEET_SET_FORMATION;

//service for fleet start mission 
app.post('/fleet_start_mission/:uav_server_id/:mission_id', function (req, res) {

	uav_server_id = req.params.uav_server_id;  
	mission_id = req.params.mission_id;

	console.log("ask for starting mission "+mission_id+" on "+uav_server_id);

	if(uav_server_id != undefined && mission_id != undefined)
	{
		builder = new flatbuffers.Builder(1024);
		
		FLEET_START_MISSION.startFLEET_START_MISSION(builder);
		FLEET_START_MISSION.addMissionID(builder, mission_id);
		mission = FLEET_START_MISSION.endFLEET_START_MISSION(builder);
		builder.finish(mission);

		buff = builder.asUint8Array();
		console.log("send as flatbuff "+buff+"\n");

		sock.send([uav_server_id+";fleet_start_mission", buff]);
		res.respond("Success", 200);
	}
	else
	{

		res.respond(new Error('Error protocol'), 500);
	}

});

//service for fleet clear target 
app.post('/fleet_set_formation/:uav_server_id/:formation_type/:leader_id/:barycenter_lat/:barycenter_lon/:barycenter_alt/:barycenter_relative_alt/:width_bound/:length_bound/:height_bound', function (req, res) {

	uav_server_id = req.params.uav_server_id;  
	formation_type = req.params.formation_type;
	leader_id = req.params.leader_id;
	barycenter_alt = req.params.barycenter_alt;
	barycenter_lat = req.params.barycenter_lat;
	barycenter_lon = req.params.barycenter_lon;
	barycenter_relative_alt = req.params.barycenter_relative_alt;
	length_bound = req.params.length_bound;
	height_bound = req.params.height_bound;
	width_bound_bound = req.params.width_bound;
	
	console.log("ask for clearing mission "+target_id+" on "+uav_server_id);

	if(uav_server_id != undefined
		&& formation_type!=undefined
		&& leader_id!=undefined
		&& barycenter_alt!=undefined
		&& barycenter_lat!=undefined
		&& barycenter_lon!=undefined
		&& barycenter_relative_alt!=undefined
		&& width_bound!=undefined
		&& length_bound!=undefined
		&& height_bound!=undefined)
	{

		builder = new flatbuffers.Builder(1024);
		

		FLEET_SET_FORMATION.startFLEET_SET_FORMATION(builder);
		FLEET_SET_FORMATION.addFormationType(builder, formation_type);
		FLEET_SET_FORMATION.addLeaderID(builder, leader_id);
		FLEET_SET_FORMATION.addBarycenterLat(builder, formation_type);
		FLEET_SET_FORMATION.addBarycenterLon(builder, formation_type);
		FLEET_SET_FORMATION.addBarycenterAlt(builder, formation_type);
		FLEET_SET_FORMATION.addBarycenterRelativeAlt(builder, formation_type);
		FLEET_SET_FORMATION.addWidthBound(builder, formation_type);
		FLEET_SET_FORMATION.addLengthBound(builder, formation_type);
		FLEET_SET_FORMATION.addHeightBound(builder, formation_type);
		cmd = FLEET_SET_FORMATION.endFLEET_SET_FORMATION(builder);
		builder.finish(cmd);

		buff = builder.asUint8Array();
		console.log("send as flatbuff "+buff+"\n");

		sock.send([uav_server_id+";fleet_set_formation", buff]);
		res.respond("Success", 200);
	}
	else
	{

		res.respond(new Error('Error protocol'), 500);
	}

});


//service for fleet clear target 
app.post('/fleet_clear_target/:uav_server_id/:target_id', function (req, res) {

	uav_server_id = req.params.uav_server_id;  
	target_id = req.params.target_id;

	console.log("ask for clearing mission "+target_id+" on "+uav_server_id);

	if(uav_server_id != undefined && target_id != undefined)
	{

		builder = new flatbuffers.Builder(1024);
		

		FLEET_CLEAR_TARGET.startFLEET_CLEAR_TARGET(builder);
		FLEET_CLEAR_TARGET.addTargetID(builder, target_id);
		cmd = FLEET_CLEAR_TARGET.endFLEET_CLEAR_TARGET(builder);
		builder.finish(cmd);

		buff = builder.asUint8Array();
		console.log("send as flatbuff "+buff+"\n");

		sock.send([uav_server_id+";fleet_clear_target", buff]);
		res.respond("Success", 200);
	}
	else
	{

		res.respond(new Error('Error protocol'), 500);
	}

});




//service for fleet stop mission
app.post('/fleet_stop_mission/:uav_server_id/:mission_id', function (req, res) {


	uav_server_id = req.params.uav_server_id;  
	mission_id = req.params.mission_id;

	console.log("ask for starting mission "+mission_id+" on "+uav_server_id);

	if(uav_server_id != undefined && mission_id != undefined)
	{
		builder = new flatbuffers.Builder(1024);
		
		FLEET_STOP_MISSION.startFLEET_STOP_MISSION(builder);
		FLEET_STOP_MISSION.addMissionID(builder, mission_id);
		mission = FLEET_STOP_MISSION.endFLEET_STOP_MISSION(builder);
		builder.finish(mission);

		buff = builder.asUint8Array();
		console.log("send as flatbuff "+buff+"\n");

		sock.send([uav_server_id+";fleet_stop_mission", buff]);
		res.respond("Success", 200);
	}
	else
	{
		res.respond(new Error('Error protocol'), 500);
	}
});


//service for fleet set target
app.post('/fleet_set_target/:uav_server_id/:target_id/:target_type/:lat/:long/:alt/:relative_alt/:heading/:add_replace/:drone_count/:perimeter', function (req, res) {

	uav_server_id = req.params.uav_server_id;  
	target_id = req.params.target_id;
	target_type = req.params.target_type;
	lat = req.params.lat;
	long = req.params.long;
	alt = req.params.alt;
	relative_alt = req.params.relative_alt;
	heading = req.params.heading;
	add_replace =req.params.add_replace;
	drone_count = req.params.drone_count;
	perimeter = req.params.perimeter;


	if(uav_server_id && target_id && target_type && lat && lon && alt && relative_alt && heading && add_replace && drone_count && perimeter)
	{

		builder = new flatbuffers.Builder(1024);
		

		FLEET_SET_TARGET.startFLEET_SET_TARGET(builder);
		FLEET_SET_TARGET.addTargetID(builder, target_id);
		FLEET_SET_TARGET.addTargetType(builder, target_type);
		FLEET_SET_TARGET.addLat(builder, lat);
		FLEET_SET_TARGET.addLon(builder, lon);
		FLEET_SET_TARGET.addRelativeAlt(builder, relative_alt);
		FLEET_SET_TARGET.addHeading(builder, heading);
		FLEET_SET_TARGET.addDroneCount(builder, drone_count);
		FLEET_SET_TARGET.addPerimeter(builder, perimeter);
		set_target = FLEET_SET_TARGET.endFLEET_SET_TARGET(builder);
		builder.finish(set_target);

		buff = builder.asUint8Array();
		console.log("send as flatbuff "+buff+"\n");

		sock.send([uav_server_id+";fleet_set_target", buff]);
		res.respond("Success", 200);
	}
	else
	{
		res.respond(new Error('Error protocol'), 500);
	}

});

//service for fleet start mission 
app.get('/drones_telemetry/:uav_server_id/:drone_id', function (req, res) {

	uav_server_id = req.params.uav_server_id;  
	drone_id = req.params.drone_id;

	// removed because of elastic search 
	// client.search({
	//   index: 'drones_telemetry',
	//   type: 'telemetry',
	//   body: {
	// 	  sort : [
	// 		{ last_update : { order : "desc"}}],
	// 		size : 1,
	// 	query: {
	// 	  bool: {
	// 	  must: [
	// 		{ match: { system_id: drone_id}},
	// 		{ match: { comp_id: 0  }}
	// 	  ]}
	// 	}	
	//   }
	// }).then(function (resp) {
	// 	var hits = resp.hits.hits;
	// 	res.respond(hits[0]._source, 200);
	// }, function (err) {
	// 	res.respond(new Error('Error query'), 500);
	// });	

});


app.get('/drones_streaming/:drone_id', function (req, res) {

	drone_id = req.params.drone_id;
 
	// client.search({
	//   index: 'drones_streaming',
	//   type: 'streaming',
	//   body: {
	// 	size : 1,
	// 	query: {
	// 	  bool: {
	// 	  must: [
	// 		{ match: {system_id: drone_id}}
	// 	  ]}
	// 	}	
	//   }
	// }).then(function (resp) {
	// 	var hits = resp.hits.hits;
	// 	res.respond(hits[0]._source, 200);
	// }, function (err) {
	// 	res.respond(new Error('Error query'), 500);
	// });	

});	 
  
  
    
app.get('/drones_snapshot/:drone_id', function (req, res) {

	drone_id = req.params.drone_id;
 
	// client.search({
	//   index: 'drones_snapshot',
	//   type: 'snapshot',
	//   body: {
	// 	size : 1,
	// 	query: {
	// 	  bool: {
	// 	  must: [
	// 		{ match: {system_id: drone_id}}
	// 	  ]}
	// 	}	
	//   }
	// }).then(function (resp) {
	// 	var hits = resp.hits.hits;		
		
	// 	if(hits[0]!=undefined && hits[0]._source.snapshot!= undefined){
				
	// 		var b64string = hits[0]._source.snapshot
	// 		res.writeHead(200, {'Content-Type': 'image/jpeg'});
	// 		res.end(b64string, 'base64');
		
	// 	}
	// 	else
	// 	{
	// 		res.respond(new Error('Error img'), 500);	
	// 	}
	// }, function (err) {
	// 	res.respond(new Error('Error query'), 500);
	// });	

})

app.get('/drones_snapshot/:drone_id', function (req, res) {

	drone_id = req.params.drone_id;
 
	// client.search({
	//   index: 'drones_snapshot',
	//   type: 'snapshot',
	//   body: {
	// 	size : 1,
	// 	query: {
	// 	  bool: {
	// 	  must: [
	// 		{ match: {system_id: drone_id}}
	// 	  ]}
	// 	}	
	//   }
	// }).then(function (resp) {
	// 	var hits = resp.hits.hits;		
		
	// 	if(hits[0]!=undefined && hits[0]._source.snapshot!= undefined){
				
	// 		var b64string = hits[0]._source.snapshot
	// 		res.writeHead(200, {'Content-Type': 'image/jpeg'});
	// 		res.end(b64string, 'base64');
		
	// 	}
	// 	else
	// 	{
	// 		res.respond(new Error('Error img'), 500);	
	// 	}
	// }, function (err) {
	// 	res.respond(new Error('Error query'), 500);
	// });	

}) 
    
app.listen(8080, 'localhost');  
console.log("API TX is running on port 8080");  

