// worker.js 
var zmq = require('zeromq')
  , sock = zmq.socket('pull');
 
sock.connect('tcp://127.0.0.1:1337');
console.log('Worker connected to port 1337');
 
sock.on('message', function(msg, t){
  console.log('work: %s', msg.toString(), t.toString());
});
