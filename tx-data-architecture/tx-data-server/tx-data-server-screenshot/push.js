// producer.js
var zmq = require('zmq')
  , sock = zmq.socket('push');

sock.bindSync('tcp://*:1337');
console.log('Producer bound to port 1337');

setInterval(function(){
  console.log('sending work');
  sock.send('some work');
}, 500);
