// subber.js 
var fs = require('fs');
var cv = require('opencv');

var flatbuffers = require('./flatbuffers').flatbuffers; 
var Screenshot = require('./screenshot_generated').Screenshot;
var Streaming = require('./streaming_generated').Streaming;

var zmq = require('zmq'),
	sock = zmq.socket('sub');

var url = process.argv[2] || 'tcp://127.0.0.1:1338';


// client elasticsearch
/*var elasticsearch = require('elasticsearch');
var client = new elasticsearch.Client({
  host: 'http://192.168.56.1:9200',
  log: 'trace'
});

//check elasticsearch
client.ping({
  // ping usually has a 3000ms timeout

}, function (error) {
  if (error) {
    console.trace(error+' elasticsearch cluster is down!');
  } else {
    console.log('All is well');
  }
});*/


sock.connect(url);
sock.subscribe("Screenshot"); //subscribe screenshot flux
//sock.subscribe("Streaming"); //subscribe screenshot flux
console.log('Subscriber connected to '+url+' on tag Streaming & Screenshot');

sock.on('message', function(topic, message) {
	if(topic == "Screenshot"){
		console.log("got screenshot message");
		analyseScreenshot(message);
	}
	else if(topic == "Streaming")
	{
		console.log("got streaming message");
		newStreamingInfo(message);
	}
	else
	{
		console.log("msg ignore");	
	}
});

function newStreamingInfo(data)
{
	buf = new flatbuffers.ByteBuffer(data);
	message = Streaming.getRootAsStreaming(buf);
	
	system_id = message.droneId();
	url_publish = message.urlPublish();
	url_retrieve = message.urlRetrieve();
	
	console.log(url_publish+ " "+url_retrieve+" "+system_id);
	console.log("save into db");
	/*client.index({
	  index: 'drones_streaming',
	  type: 'streaming',
	  id: system_id,
	  body: {
		url_publish: url_publish,
		url_retrieve: url_retrieve,
		last_update : 0,
		system_id : system_id
	  }
	}, function (error, response) {
		if(!error){
			console.log("new streaming info add "+response);
		}
		else
		{
			console.log("error in reading streaming info " + error);
		}
	});*/
	
	
}

function analyseScreenshot(data) {
	
	buf = new flatbuffers.ByteBuffer(data);
	message = Screenshot.getRootAsScreenshot(buf);

	drone = message.drone();
	base64Data = message.image();


	//create img file
	fs.writeFile("screenshot.jpeg", base64Data, 'base64', function(err) {
		if(err) {console.log("create temp file error");}
		
		cv.readImage("screenshot.jpeg", function(err, im) {
		if (err) {
			console.log("read image error");
		} // throw err;
		if (im.width() < 1 || im.height() < 1) {
			console.log("image format error");
		}

		im.detectObject(cv.FACE_CASCADE, {}, function(err, faces) {

			if (err) {
				console.log("opencv read image error");
			} //throw err;
			else {
				check = false;

				for (var i = 0; i < faces.length; i++) {

					var face = faces[i];
					check = true;
					im.ellipse(face.x + face.width / 2, face.y + face.height / 2, face.width / 2, face.height / 2);
				}

				if (check) {
					im.save('./stream'+drone+'.jpg');
					console.log("face detected on stream " + drone);

					//updates db
					/*client.update({
						  index: 'drones_streaming',
						  type: 'streaming',
						  id : drone,
						  body: {
								doc: {
								  notification: true
								}
						  }
					});*/
				}
				else
				{
					//updates db
					/*client.update({
						  index: 'drones_streaming',
						  type: 'streaming',
						  id : drone,
						  body: {
								doc: {
								  notification: false
								}
						  }
					});*/
				}

				//remove temp file
				fs.unlinkSync("screenshot.jpeg");

			}
		});

	});
		
	});
}

/*

*/

