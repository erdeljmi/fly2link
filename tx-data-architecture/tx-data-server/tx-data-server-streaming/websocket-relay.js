var fs = require('fs'),
	http = require('http'),
	WebSocket = require('ws');
	
var url = require("url");
var querystring = require('querystring');

if (process.argv.length < 2) {
	console.log(
		'Usage: \n' +
		'node websocket-relay.js [<stream-port> <websocket-port>]'
	);
	process.exit();
}

var STREAM_PORT = process.argv[2] || 8081,
	WEBSOCKET_PORT = process.argv[3] || 8082;

	
var sockets = {};
	
// Websocket Server

//http://localhost:8282/stream?drone=<id>
var socketServer = new WebSocket.Server({path: '/stream', port: WEBSOCKET_PORT, perMessageDeflate: false});
socketServer.connectionCount = 0;
socketServer.on('connection', function(socket, upgradeReq) {
	socketServer.connectionCount++;
	console.log(
		'New WebSocket Connection: ', 
		(upgradeReq || socket.upgradeReq).socket.remoteAddress,
		(upgradeReq || socket.upgradeReq).headers['user-agent'],
		'('+socketServer.connectionCount+' total)'
	);
	socket.on('close', function(code, message){
		socketServer.connectionCount--;
		console.log(
			'Disconnected WebSocket ('+socketServer.connectionCount+' total)'
		);
	});
	
	socket.on("message", function(str) {
		//init message 
		
		//determine the listened flux
		  var obj = JSON.parse(str);
		  if("flux" in obj) {
			if(!sockets[obj.flux])
			{
				sockets[obj.flux] = [];  
			}
			
			console.log("add listener to "+obj.flux);
			sockets[obj.flux].push(socket);
		  } 
	});	
});


//multicast listeners
socketServer.broadcast = function(data, flux) {
	if(sockets[flux]){
		sockets[flux].forEach(function each(client) {
			if (client.readyState === WebSocket.OPEN) {
				client.send(data);
			}
		});
	}
};


// HTTP Server to accept incomming MPEG-TS Stream from ffmpeg
//http://localhost:8181/publish/<id>
var publishServer = http.createServer( function(request, response) {
	var params = request.url.substr(1).split('/');
	console.log("get");
	if(params[0] == "publish"){
		var flux = params[1];
		

		if(flux){		
			console.log("new drone "+flux);
			init_publish_rqst(request, response, flux);
			
		}
		else
		{
			console.log("error "+flux);
		}
	}
	
}).listen(STREAM_PORT);


function init_publish_rqst(request, response, flux)
{
	response.connection.setTimeout(0);
	console.log(
		'dk Stream Connected: ' + 
		request.socket.remoteAddress + ':' +
		request.socket.remotePort
	);
	request.on('data', function(data){
		//console.log(data);
		socketServer.broadcast(data, flux);
		
		
	});
	request.on('end',function(){
		console.log('close');

	});
}


console.log('Listening for incomming MPEG-TS Stream on http://127.0.0.1:'+STREAM_PORT+'/<secret>');
console.log('Awaiting WebSocket connections on ws://127.0.0.1:'+WEBSOCKET_PORT+'/');
