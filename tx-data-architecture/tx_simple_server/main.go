package main

import (
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

func GetDroneSnapshot(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	for _, param := range params {
		log.Println("param: ", param)
	}
}

func GetDroneStreaming(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	for _, param := range params {
		log.Println("param: ", param)
	}
}

func PostFleetStartMission(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	for _, param := range params {
		log.Println("param: ", param)
	}
}

func PostFleetSetFormation(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	for _, param := range params {
		log.Println("param: ", param)
	}
}

func PostFleetClearTarget(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	for _, param := range params {
		log.Println("param: ", param)
	}
}

func PostFleetStopMission(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	for _, param := range params {
		log.Println("param: ", param)
	}
}

func PostFleetSetTarget(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	for _, param := range params {
		log.Println("param: ", param)
	}
}

func GetAvailableRoutes(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text")
	w.WriteHeader(200)
	w.Write([]byte("/drones_snapshot/{drone_id}, /drones_streaming/{drone_id},/fleet_start_mission/{uav_server_id}/{mission_id}"))
}

func main() {
	router := mux.NewRouter()

	router.HandleFunc("/", GetAvailableRoutes).Methods("GET")

	// Add routes
	router.HandleFunc("/drones_snapshot/{drone_id}", GetDroneSnapshot).Methods("GET")

	router.HandleFunc("/drones_streaming/{drone_id}", GetDroneStreaming).Methods("GET")

	router.HandleFunc("/fleet_start_mission/{uav_server_id}/{mission_id}", PostFleetStartMission).Methods("POST")

	router.HandleFunc("/fleet_set_formation/{uav_server_id}/{formation_type}/{leader_id}/{barycenter_lat}/{barycenter_lon}/{barycenter_alt}/{barycenter_relative_alt}/{width_bound}/{length_bound}/{height_bound}", PostFleetSetFormation).Methods("POST")

	router.HandleFunc("/fleet_clear_target/{uav_server_id}/{target_id}", PostFleetClearTarget).Methods("POST")

	router.HandleFunc("/fleet_stop_mission/{uav_server_id}/{mission_id}", PostFleetStopMission).Methods("POST")

	router.HandleFunc("/fleet_set_target/{uav_server_id}/{target_id}/{target_type}/{lat}/{lon}/{alt}/{relative_alt}/{heading}/{add_replace}/{drone_count}/{perimeter}", PostFleetSetTarget).Methods("POST")

	log.Println("launching server on localhost:8080")
	log.Println("Available routes are:")

	log.Println("/drones_snapshot/{drone_id}")
	log.Println("/drones_streaming/{drone_id}")
	log.Println("/fleet_start_mission/{uav_server_id}/{mission_id}")
	log.Println("/fleet_set_formation/{uav_server_id}/{formation_type}/{leader_id}/{barycenter_lat}/{barycenter_lon}/{barycenter_alt}/{barycenter_relative_alt}/{width_bound}/{length_bound}/{height_bound}")
	log.Println("/fleet_clear_target/{uav_server_id}/{target_id}")
	log.Println("/fleet_stop_mission/{uav_server_id}/{mission_id}")
	log.Println("/fleet_set_target/{uav_server_id}/{target_id}/{target_type}/{lat}/{lon}/{alt}/{relative_alt}/{heading}/{add_replace}/{drone_count}/{perimeter}")

	log.Fatal(http.ListenAndServe(":8080", router))
}
